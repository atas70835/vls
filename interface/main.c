#include <gtk/gtk.h>


int CreateControls();
int CreateInterface();

void DeleteEvent(GtkWidget *pwWidget, GdkEvent* peEvent, gpointer pData)
{
  gtk_main_quit ();
}

void quit ()
{
  gtk_exit (0);
}


int main(int argc, char* argv[])
{
  GtkWidget* pwWindow;
//  GtkWidget* pwButton;
  
  /* Default setup of the GTK lib */
  gtk_init(&argc, &argv);
  
  /* Create a window widget */
  pwWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  /* Sets the title of the window */
  gtk_window_set_title(GTK_WINDOW(pwWindow), "VideoLAN Server Manager");

  /* Quit ????????????????????????????? */
  gtk_signal_connect(GTK_OBJECT(pwWindow), "delete_event",
   GTK_SIGNAL_FUNC (DeleteEvent), NULL);
  gtk_signal_connect(GTK_OBJECT(pwWindow), "destroy_event",
   GTK_SIGNAL_FUNC (DeleteEvent), NULL);
  gtk_signal_connect (GTK_OBJECT (pwWindow), "destroy",
                                            GTK_SIGNAL_FUNC (quit), NULL);

  /* Display it */
  gtk_widget_show(pwWindow);

  /* GTK main processing loop */
  gtk_main();
  
  return 0;
}
