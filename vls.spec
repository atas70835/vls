%define name		vls
%define version		0.5.5-cvs
%define release		1

Summary:	VideoLAN is a free streaming software solution.
Name:		%{name}
Version:	%{version}
Release:	%{release}
Copyright:	GPL
URL:		http://www.videolan.org/
Group:		Application/Multimedia
Source:		http://www.videolan.org/pub/videolan/vls/%{version}/%{name}-%{version}.tar.gz
BuildRoot:	%_tmppath/%name-%version-%release-root
Requires:	libdvbpsi1
Buildrequires:	libdvbpsi1-devel

%description
VideoLAN is a free streaming software solution developed by
students from the Ecole Centrale Paris and developers from all over the
World.
The VideoLAN Server can stream MPEG content from several sources
(MPEG 1 or 2 files, DVDs, satellite cards, digital terrestial television
cards, real time MPEG encoding cards) over an IP network in unicast or
multicast.
You may install vls-dvd to allow DVD streaming and/or vls-dvb to be able
to stream digital unencrypted satellite channels and digital unencrypted
terrestial television channels from a DVB device.

%package -n %{name}-dvd
Summary:	DVD input for VideoLAN Server
Group:		Application/Multimedia
Requires:	libdvdread2
Requires:	%{name} = %{version}
Buildrequires:	libdvdread2-devel

%description -n %{name}-dvd
The %{name}-dvd package includes the DVD input plugin for vls, the VideoLAN
Server.
With this plugin vls is able to read MPEG data from a DVD and send the
stream to the network, together with all subtitles and audio tracks.

%package -n %{name}-dvb
Summary:	DVB input for VideoLAN Server
Group:		Application/Multimedia
Requires:	%{name} = %{version}
#Buildrequires:	dvb-devel

%description -n %{name}-dvb
The %{name}-dvb package includes the DVB input plugin for the VideoLAN
Server.
It allows reception of digital unencrypted satellite channels and
digital unencrypted terrestial television channels from a DVB device
using the DVB drivers from linuxtv.org.

%prep
%setup -q

%build
./configure --prefix=%_prefix --disable-debug --enable-dvb
make 

%install
%makeinstall

%clean
rm -rf %buildroot

%files -n %{name}
%defattr(-,root,root,-)
%doc AUTHORS README COPYING ChangeLog
%config %{_sysconfdir}/videolan/vls/vls.cfg

%{_bindir}/vls

%files -n %{name}-dvd
%defattr(-,root,root)
%{_libdir}/videolan/vls/dvdreader.so

%files -n %{name}-dvb
%defattr(-,root,root)
%{_libdir}/videolan/vls/dvbinput.so

%changelog
* Sun Dec 8 2002 Alexis de Lattre <alexis@videolan.org>
- Updated vls.spec for the vls 0.4.5 release.

* Mon Oct 14 2002 Alexis de Lattre <alexis@videolan.org>
- Updated vls.spec for the vls 0.4 release.

* Tue Apr 9 2002 Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
- split into two separate packages.

* Thu Apr 4 2002 Jean-Paul Saman <saman@natlab.research.philips.com>
- first version of package for redhat systems.

