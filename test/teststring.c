// Test des fonctions manipulation des strings
// A compiler par 'make -f Makefile.string'

#include <stdio.h>
#include "../core/config.h"
#include "../core/common.h"
#include "../core/string.h"

int main()
{
  C_String* pString1 = new C_String("string1");
  pString1->Display();

  C_String* pString2 = new C_String(25, "string2 %s %d", "oki", 1);
  pString2->Display();

  C_String String3("string3");
  String3.Display();

  C_String String4("string4");
  String4.Display();

  String4 = *pString1;
  String4.Display();

  String4 = "string4";
  String4.Display();

  C_String String20("20");
  String20.Display();
  String20 = String20 + "ta mere";
  String20.Display();

  String20 = String20 + "ta mere";
  C_String String21("21");
  String21 = String3 + String4;

  printf("<B>%c %c %c<E>\n", (*pString1)[0], (*pString2)[9], String3[5]);
  printf("<B>%s<E> -> %d chars\n", (const char*)pString2, pString2->Length());
  printf("<B>%s<E> -> %d chars\n", (const char*)String4, String4.Length());

  String4 = String3 + "ca marche(1)";
  String4.Display();

  String4 = String4 + "ca marche (2)" + "ca rulez";
  String4.Display();

  String4 = String3 + *pString2;
  String4.Display();  

  String4 += "HEHE";
  String4.Display();
  String4 += String3;
  String4.Display();
  String4 += String4;
  String4.Display();

  C_String String50((String3 + String4 + *pString1));
  String50.Display();

  delete pString1;
  delete pString2;

  return 0;
}
