/*****************************************************************************/
/*                                                                           */
/* Unix Socket Client                                                        */
/* Program to test the UnixSocket interface                                  */
/*                                                                           */
/* Tristan Leteurtre <tristan.leteurtre@anevia.com>                          */
/* Oct 2003                                                                  */
/*                                                                           */
/*****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>  /* exit malloc */
#include <unistd.h> /* close */


int main(int argc, char ** argv)
{ 
  int sock;
  struct sockaddr_un server;

  if (argc < 2)
  { 
     printf("usage: %s <pathname>\n", argv[0]);
     exit(1);
  }

  sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sock < 0)
  { 
    perror("opening stream socket");
    exit(1);
  }
  
  server.sun_family = AF_UNIX;
  strncpy(server.sun_path, argv[1], 108);
 
  if (connect(sock, (struct sockaddr *) &server,
                    sizeof(struct sockaddr_un)) < 0)
  {
    close(sock);
    perror("connecting stream socket");
    exit(1);
  }

  char * s;
  s = (char*)malloc (256 * sizeof(char));
  
  while (12)
  {
    fgets(s,256, stdin);       
    if (write(sock, s, strlen(s)) < 0)
      perror("writing on stream socket");
  }
  close(sock);

  return 0;
} 

