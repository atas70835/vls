#!/usr/bin/expect -f
#
#  Author(s)  : Jean-Paul Saman
#
#  Description: Expect script that handles the interactive
#               telnet session. It opens a connection then
#               test the program command (and show program)
#               and closes the connection
#
#  Arg1       : Hostname of IP address
#  Arg2       : Port address
#  Arg3       : Username
#  Arg4       : Password
#
#  Return     : 0  The session succeeded
#               -1 The session failed
#

set Site [lindex $argv 0]
set Port [lindex $argv 1]
set User [lindex $argv 2]
set Password [lindex $argv 3]

set timeout 30
spawn telnet $Site $Port
expect {
    "Login:" { }
    "unknown" {
        send "quit\r"
        send_user "Error: login expected!\n"
        exit -1
    }
    "failure" {
       send "quit\r"
       send_user "Error: could not start a telnet session\n"
       exit -1
    }
    timeout {
       send "quit\r"
       send_user "Error: (timeout) login expected!\n"
       exit -1
    }
}
#  Sometime the telnet session hangs, probably due to
#  the fast inlogging (scripting), the following sleep
#  commands solve this hangup.
sleep 1
send "$User\r"
sleep 1
expect {
    "Password:" { }
    timeout {
        send "quit\r"
        send_user "Error: (timeout) Password expected!\n"
        exit -1
    }
}

send "$Password\r"
expect {
    "$User@vls>" { }
    "incorrect" {
        send_user "Error: remote user prompt expected!\n"
        exit -1
    }
    timeout {
       send "quit\r"
       send_user "Error: (timeout) remote user prompt expected!\n"
       exit -1
    }
}

# Faulty situations

send "program bogey local1 10457_1.ts Mpeg2-TS --add --delete\r"
expect {
    "Status: 0" {
       send_user "Error: Options --add and --delete should not exists together\n"
       exit -1
    }
    "Error: -1" { }
    timeout {
       send "quit\r"
       send_user "Error: (timeout) Adding program command expected to fail!\n"
       exit -1
    }
}


# logging out
send "logout\r"
expect "Closing connection\rConnection closed by foreign host.\r"

#  End script

