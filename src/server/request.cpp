/*******************************************************************************
* request.cpp: User requests encapsulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "broadcast.h"
#include "program.h"
#include "request.h"

#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"
#include "buffer.h"
#include "output.h"
#include "channel.h"
//#include "../../server/broadcast.h"
//#include "../../server/request.h"
#include "input.h"
//#include "../../server/tsstreamer.h"

#define STRING_SEPARATOR ' '


//******************************************************************************
// class C_Request
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Request::C_Request(const C_String& strCmd) : m_strCmd(strCmd), m_cArgs(5)
{
}


//------------------------------------------------------------------------------
// Set the value of an argument.
//------------------------------------------------------------------------------
void C_Request::SetArg(const C_String strArgName, const C_String& strArgValue)
{
  ASSERT(strArgName != "");

  C_String* pNewVal = new C_String(strArgValue);
  m_cArgs.Update(strArgName, pNewVal);
}


//------------------------------------------------------------------------------
// Get the value of an argument.
//------------------------------------------------------------------------------
C_String C_Request::GetArg(const C_String& strArgName) const
{
  C_String* pArg = m_cArgs.Get(strArgName);
  if(pArg)
    return *pArg;
  else
    return C_String();
}


//------------------------------------------------------------------------------
// Get the list of arguments.
//------------------------------------------------------------------------------
const C_HashTable<C_String, C_String>& C_Request::GetArgs() const
{
  return m_cArgs;
}



//******************************************************************************
// class C_Answer
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Answer::C_Answer(const C_String& strProvider) : m_strProvider(strProvider)
{
  m_iStatus = GEN_ERR;
}


//------------------------------------------------------------------------------
// Get the name of the provider.
//------------------------------------------------------------------------------
const C_String& C_Answer::GetProvider() const
{
  return m_strProvider;
}


//------------------------------------------------------------------------------
// Get th status.
//------------------------------------------------------------------------------
s32 C_Answer::GetStatus() const
{
  return m_iStatus;
}


//------------------------------------------------------------------------------
// Get all the messages.
//------------------------------------------------------------------------------
const C_List<C_String>& C_Answer::GetMessages() const
{
  return m_cMessages;
}


//------------------------------------------------------------------------------
// Get all the sub-answers.
//------------------------------------------------------------------------------
const C_List<C_Answer>& C_Answer::GetSubAnswers() const
{
  return m_cAnswerList;
}


//------------------------------------------------------------------------------
// Set the status
//------------------------------------------------------------------------------
void C_Answer::SetStatus(s32 iStatus)
{
  m_iStatus = iStatus;
}


//------------------------------------------------------------------------------
// Add a message.
//------------------------------------------------------------------------------
void C_Answer::AddMessage(const C_String& strMessage)
{
  m_cMessages.PushEnd(new C_String(strMessage));
}


//------------------------------------------------------------------------------
// Add a sub-answer
//------------------------------------------------------------------------------
void C_Answer::Add(const C_Answer& cAnswer)
{
  C_Answer* pAnswer = new C_Answer(cAnswer);
  ASSERT(pAnswer);
  m_cAnswerList.PushEnd(pAnswer);
}


//------------------------------------------------------------------------------
// Clone the answer.
//------------------------------------------------------------------------------
C_Answer* C_Answer::Clone()
{
  C_Answer* pAnswer = new C_Answer(*this);
  ASSERT(pAnswer);
  return pAnswer;
}



//******************************************************************************
// class C_Event
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Event::C_Event(const C_String& strProvider) : m_strProvider(strProvider)
{
//  m_iCode = NO_CODE;
}


//------------------------------------------------------------------------------
// Get the provider of the event.
//------------------------------------------------------------------------------
const C_String& C_Event::GetProvider() const
{
  return m_strProvider;
}


//------------------------------------------------------------------------------
// Get event's code
//------------------------------------------------------------------------------
s32 C_Event::GetCode() const
{
  return m_iCode;
}


//------------------------------------------------------------------------------
// Get event's broadcast
//------------------------------------------------------------------------------
const C_Broadcast* C_Event::GetBroadcast() const
{
  return m_pBroadcast;
}


//------------------------------------------------------------------------------
// Get the value of a property.
//------------------------------------------------------------------------------
/*C_String C_Event::GetProperty(const C_String& strPropName) const
{
  C_String* pProp = m_cProperties.Get(strPropName);
  if(pProp)
    return *pProp;
  else
    return C_String();
}*/


//------------------------------------------------------------------------------
// Get all the messages.
//------------------------------------------------------------------------------
const C_List<C_String>& C_Event::GetMessages() const
{
  return m_cMessages;
}


//------------------------------------------------------------------------------
// Set event's code.
//------------------------------------------------------------------------------
void C_Event::SetCode(s32 iCode)
{
  m_iCode = iCode;
}


//------------------------------------------------------------------------------
// Set event's broadcast
//------------------------------------------------------------------------------
void C_Event::SetBroadcast(const C_Broadcast* pBroadcast)
{
    m_pBroadcast = pBroadcast;
}


//------------------------------------------------------------------------------
// Set the value of a property.
//------------------------------------------------------------------------------
/*void C_Event::SetProperty(const C_String strPropName,
                          const C_String& strPropValue)
{
  ASSERT(strPropName != "");

  C_String* pNewVal = new C_String(strPropValue);
  m_cProperties.Update(strPropName, pNewVal);
}*/


//------------------------------------------------------------------------------
// Add a message.
//------------------------------------------------------------------------------
void C_Event::AddMessage(const C_String& strMessage)
{
  m_cMessages.PushEnd(new C_String(strMessage));
}


//------------------------------------------------------------------------------
// Clone.
//------------------------------------------------------------------------------
C_Event* C_Event::Clone()
{
  C_Event* pEvent = new C_Event(*this);
  ASSERT(pEvent);
  return pEvent;
}



//******************************************************************************
// class C_Message
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Message::C_Message(const C_Request& cRequest) : m_cRequest(cRequest),
                                                  m_cAnswer("none"),
                                                  m_cEvent("none")
{
  m_iType = REQUEST_TYPE;
}


//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Message::C_Message(const C_Answer& cAnswer) : m_cRequest("none"),
                                                m_cAnswer(cAnswer),
                                                m_cEvent("none")
{
  m_iType = ANSWER_TYPE;
}


//------------------------------------------------------------------------------
// Constructor.
//------------------------------------------------------------------------------
C_Message::C_Message(const C_Event& cEvent) : m_cRequest("none"),
                                              m_cAnswer("none"),
                                              m_cEvent(cEvent)
{
  m_iType = EVENT_TYPE;
}


//------------------------------------------------------------------------------
// Reverse streaming (temporary)
//------------------------------------------------------------------------------
C_Message::C_Message(const C_String& strMessage) : m_cRequest("none"),
                                                   m_cAnswer("none"),
                                                   m_cEvent("none")
{
  C_StringTokenizer cTokenizer(strMessage, STRING_SEPARATOR);

  unsigned int iCount = cTokenizer.CountTokens();

  if (iCount >= 2)
  {
    C_String strType = cTokenizer.NextToken();
    if (strType == REQUEST_TYPE_STR)
    {
      // "1 CMD {ARG VALUE}xN"
      if (iCount % 2 == 0)
      {
        m_iType = REQUEST_TYPE;
        C_String strCmd = cTokenizer.NextToken();
        m_cRequest.m_strCmd = strCmd;
        while (cTokenizer.HasMoreToken())
        {
          C_String strArgName = cTokenizer.NextToken();
          C_String strArgValue = cTokenizer.NextToken();
          m_cRequest.SetArg(strArgName, strArgValue);
        }
      }
      else
      {
        // Bad request
        m_iType = INVALID_TYPE;
      }
    }
    else if (strType == ANSWER_TYPE_STR)
    {
      // "2 ANSWER"
      // ANSWER == "PROVIDER STATUS N {MESSAGES}xN M {ANSWER}xM"
      if (iCount >= 5)
      {
        m_iType = ANSWER_TYPE;
        C_Answer *pAnswer = ExtractAnswer(cTokenizer);
        if (pAnswer)
        {
          m_cAnswer = *pAnswer;
          delete pAnswer;
        }
        else
        {
          // Bad answer
          m_iType = INVALID_TYPE;
        }
      }
      else
      {
        // Bad answer
        m_iType = INVALID_TYPE;
      }
    }
    else if (strType == EVENT_TYPE_STR)
    {
      // "3 PROVIDER CODE N {PROPERTY VALUE}xN M {MESSAGE}xM"
      if (iCount >= 5)
      {
        m_iType = EVENT_TYPE;
        // get the provider
        C_String strProvider = cTokenizer.NextToken();
        m_cEvent = C_Event(strProvider);
        // get the code
        C_String strCode = cTokenizer.NextToken();
        m_cEvent.SetCode(strCode.ToInt());
        // get the properties
        C_String strPropertyCount = cTokenizer.NextToken();
        unsigned int iPropertyCount = strPropertyCount.ToInt();
        if(cTokenizer.CountTokens() >= 2 * iPropertyCount + 1)
        {
          for(unsigned int i = 0; i < iPropertyCount; i++)
          {
            C_String strName = cTokenizer.NextToken();
            C_String strValue = cTokenizer.NextToken();
//            m_cEvent.SetProperty(strName, strValue);
            printf("should read the broadcast info here instead");
          }
        }
        else
        {
          // Bad event
          m_iType = INVALID_TYPE;
        }
        // get the messages
        C_String strMessageCount = cTokenizer.NextToken();
        unsigned int iMessageCount = strPropertyCount.ToInt();
        if(cTokenizer.CountTokens() == iMessageCount)
        {
          for(unsigned int i = 0; i < iMessageCount; i++)
          {
            C_String strMessage = cTokenizer.NextToken();
            m_cEvent.AddMessage(strMessage);
          }
        }
        else
        {
          // Bad event
          m_iType = INVALID_TYPE;
        }
      }
      else
      {
        // Bad event
        m_iType = INVALID_TYPE;
      }
    }
    else
    {
      // Bad Type
      m_iType = INVALID_TYPE;
    }
  }
  else
  {
    // Bad message
    m_iType = INVALID_TYPE;
  }

  ASSERT(m_iType);
}

//------------------------------------------------------------------------------
// Streaming (temporary)
//------------------------------------------------------------------------------
C_String C_Message::GetString()
{
  ASSERT(m_iType != INVALID_TYPE)

  C_String strMessage(m_iType);

  if (m_iType == REQUEST_TYPE)
  {
    // "1 CMD {ARG VALUE}xN"
    strMessage += C_String(STRING_SEPARATOR) + m_cRequest.m_strCmd;
    C_HashTableIterator<C_String, C_String> cIterator =
                                      m_cRequest.m_cArgs.CreateIterator();
    while (cIterator.HasNext())
    {
      C_HashTableNode<C_String, C_String> *pNode = cIterator.GetNext();
      strMessage += C_String(STRING_SEPARATOR) + pNode->GetKey() +
                    STRING_SEPARATOR + *pNode->GetValue();
    }
  }
  else if(m_iType == ANSWER_TYPE)
  {
    // "2 ERR_CODE ANSWER"
    // ANSWER == "PROVIDER STATUS N {MESSAGES}xN M {ANSWER}xM"
    char sCode[4];
    sprintf(sCode, "%03x", m_cAnswer.GetStatus());
    strMessage += C_String(STRING_SEPARATOR);
    strMessage += C_String(sCode);
    strMessage += C_String(STRING_SEPARATOR);
    strMessage += AnswerToString(m_cAnswer);
  }
  else //if (m_iType == EVENT_TYPE)
  {
    // "3 CODE PROVIDER N {PROPERTY VALUE}xN M {MESSAGE}xM"
    const C_Broadcast* pBroadcast =  m_cEvent.GetBroadcast();
    char sCode[4];     sprintf(sCode, "%03x", m_cEvent.m_iCode);

    strMessage += C_String(STRING_SEPARATOR) + sCode;
    strMessage += C_String(STRING_SEPARATOR) + pBroadcast->GetName();
    strMessage += C_String(STRING_SEPARATOR) +
                                           pBroadcast->GetChannel()->GetName();

    strMessage += C_String(STRING_SEPARATOR) +
                                           pBroadcast->GetInput()->GetName();
    strMessage += C_String(STRING_SEPARATOR) +
                                           pBroadcast->GetProgram()->GetName();

    // add the messages
/*    unsigned int iSize = m_cEvent.m_cMessages.Size();
    strMessage += C_String(STRING_SEPARATOR) + iSize;
    for(unsigned int i = 0; i < iSize; i++)
      strMessage += C_String(STRING_SEPARATOR) + m_cEvent.m_cMessages[i];*/
  }

  return strMessage;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer* C_Message::ExtractAnswer(C_StringTokenizer &cTokenizer)
{
  // ANSWER == "PROVIDER STATUS N {MESSAGES}xN M {ANSWER}xM"
  C_Answer *pAnswer = NULL;

  unsigned int iCount = cTokenizer.CountTokens();

  if (iCount >= 4)
  {
    C_String strProvider = cTokenizer.NextToken();
    pAnswer = new C_Answer(strProvider);
    C_String strStatus = cTokenizer.NextToken();
    pAnswer->SetStatus(strStatus.ToInt());
    C_String strMessageCount = cTokenizer.NextToken();
    unsigned int iMessageCount = strMessageCount.ToInt();
    for (unsigned int i = 0; i < iMessageCount; i++)
    {
      if (cTokenizer.HasMoreToken())
      {
        C_String strMsg = cTokenizer.NextToken();
        pAnswer->AddMessage(strMsg);
      }
      else
      {
        // Bad answer
        delete pAnswer;
        pAnswer = NULL;
        break;
      }
    }
    if (pAnswer)
    {
      C_String strAnswerCount = cTokenizer.NextToken();
      unsigned int iAnswerCount = strAnswerCount.ToInt();
      for (unsigned int j = 0; j < iAnswerCount; j++)
      {
        C_Answer *pSubAnswer = ExtractAnswer(cTokenizer);
        if (pSubAnswer)
        {
          pAnswer->Add(*pSubAnswer);
        }
        else
        {
          // Bad answer
          delete pAnswer;
          pAnswer = NULL;
          break;
        }
      }
    }
    else
    {
      // Bad answer
      delete pAnswer;
      pAnswer = NULL;
    }
  }

  return pAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Message::AnswerToString(C_Answer &cAnswer)
{
  C_String str(cAnswer.m_strProvider);

  //str += C_String(" ") + cAnswer.m_cMessages.Size();

  C_ListIterator<C_String> cIterator1 = cAnswer.m_cMessages.CreateIterator();
  while (cIterator1.HasNext())
  {
    str += " " + *cIterator1.GetNext();
  }

  //str += C_String(" ") + cAnswer.m_cAnswerList.Size();

  C_ListIterator<C_Answer> cIterator2 = cAnswer.m_cAnswerList.CreateIterator();
  while (cIterator2.HasNext())
  {
    str += " " + AnswerToString(*cIterator2.GetNext());
  }

  return str;
}

