/*******************************************************************************
* directory.h: Program description and registration classes definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: directory.h,v 1.3 2003/02/05 01:25:32 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _DIRECTORY_H_
#define _DIRECTORY_H_


//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

// Status of the program
#define PGRM_AVAILABLE          1
#define PGRM_BROADCASTED        2
#define PGRM_PAUSED             3
#define PGRM_ERROR              4



//------------------------------------------------------------------------------
// Declaration forward
//------------------------------------------------------------------------------
class C_PgrmDirectory;
class C_PgrmDirectoryBrowser;
class C_Input;


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_PgrmInfo
{
 public:
  C_PgrmInfo(const C_Program& cProgram);
  
  const C_Program* GetProgram() const;

  const C_Vector<C_Input>& GetProviders() const;
  bool IsProvider(C_Input* pInput) const;
  void AddProvider(C_Input* pInput);
  bool DeleteProvider(C_Input* pInput);

 private:
  C_Program m_cProgram;
  C_Vector<C_Input> m_cProviders;
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Thread safe directory of programs. Locking rules should be transparent, they
// are summarised in the .cpp file
//------------------------------------------------------------------------------
class C_PgrmDirectoryBrowser
{
  friend class C_PgrmDirectory;

 public:
  bool HasNext();
  C_PgrmInfo* GetNext();

 protected:
  C_PgrmDirectoryBrowser(const C_PgrmDirectory& cDirectory);

 private:
  C_RepositoryBrowser<C_String, C_PgrmInfo> m_cBrowser;
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Thread safe directory of programs. Locking rules should be transparent, they
// are summarised in the .cpp file
//------------------------------------------------------------------------------
class C_PgrmDirectory
{
  friend class C_PgrmDirectoryBrowser;

 public:
  C_PgrmDirectory();
  ~C_PgrmDirectory();

  // Lock the directory
  void Lock();
  void UnLock();
  
  // Should only be called when a lock on the directory has been taken
  int Add(const C_Program& cProgram, C_Input* pInput);
  int Remove(const C_Program& cProgram, C_Input* pInput);
  void Empty(C_Input* pInput);
  void Merge(C_Input* pInput,  const C_Vector<C_Program>& cNewPgrms);

  // Lock the program and get access to it. When a global lock on the
  // directory has been taken, the [] must be used instead&
  const C_Program* GetPgrm(const C_String& strName/*, C_Input* pInput = NULL*/);
  void ReleasePgrm(const C_String& strName/*, C_Input* pInput = NULL*/);
  
  // Programs should be accessed through this operator only when the caller
  // owns a lock on the directory
  C_PgrmDirectoryBrowser CreateBrowser();

private:
  // List of programs available by the source
  //C_List<C_Program> m_cPgrms;

  C_Repository<C_String, C_PgrmInfo> m_cPgrmList;

  // Directory locking
  //C_Condition m_cModifAllowed;
  //unsigned int m_iPendingModifs;

#ifdef DEBUG
  bool m_bIsLocked;
  C_Thread* m_pLockOwner;
#endif
};




//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Thread safe directory of programs. Locking rules should be transparent, they
// are summarised in the .cpp file
//------------------------------------------------------------------------------
/*class C_BroadcastDirectory : protected C_Repository<C_String, C_Broadcast>
{
 public:
  
};*/



#else
#error "Multiple inclusions of directory.h"
#endif

