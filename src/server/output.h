/*******************************************************************************
* output.h: output stream classes definitions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: output.h,v 1.5 2003/06/02 14:10:45 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _OUTPUT_H_
#define _OUTPUT_H_


#define TS_IN_ETHER 7                // TS packets that fit in an ethernet

//------------------------------------------------------------------------------
// E_Output exception
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class E_Output : public E_Exception
{
 public:
  E_Output(const C_String& strMsg, const E_Exception& e);
};


//------------------------------------------------------------------------------
// C_Output class: generic output streaming
//------------------------------------------------------------------------------
// Provide a generic target for the output of data. Subclasses must be defined
// and used to send the data to specific device classes. 
//------------------------------------------------------------------------------
class C_Output
{
 public:
  C_Output(unsigned int iBuffSize);
  virtual ~C_Output();

  //  virtual C_Output* Clone() const = 0;

  void Init(C_NetList* pTsProvider);
  void Close();

  void Send(C_TsPacket* pPacket,bool RtpEncapsulation);
  void Flush();

  // Return the capacity of the output buffer
  unsigned int GetBuffCapacity()
  { return m_cTsBuff.Capacity(); }

  // Return the number of byte lost
  unsigned int GetByteLoss()
  { return m_iByteLost; }

  inline s64 GetDate();

 protected:
  virtual void WriteToPort(bool RtpEncapsulation, u32 RtpSendTime) = 0;
  virtual void OnInit() = 0;
  virtual void OnClose() = 0;

  // Fifo of TS packets waiting to be sent
  C_Fifo<C_TsPacket> m_cTsBuff;

  // Buffer of preallocated TS packets
  C_NetList* m_pTsProvider;

  // Byte loss counter
  unsigned int m_iByteLost;

  C_RtpHeader * m_pRtpHeader;
  u16 m_iRtpCounter;

};


#else
#error "Multiple inclusions of output.h"
#endif

