/*******************************************************************************
* manager.cpp: Vls manager
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include <errno.h>

#include "config.h"

#include "../core/core.h"

#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"

#include "program.h"
#include "request.h"
#include "buffer.h"
#include "output.h"
#include "channel.h"
#include "broadcast.h"
#include "input.h"
#include "repository.h"
#include "directory.h"
#include "tsstreamer.h"
#include "admin.h"

#include "manager.h"
#ifndef  _WIN32
	#include "daemon.h"
#endif
#include "vls.h"

#include "repository.cpp"

typedef C_Answer (C_Manager::*zzzz)(C_Request&);

#define AddFunction(y,z) \
  foo = new zzzz; \
  *foo = z;       \
  m_cFunctions.Add(y, foo);

/*******************************************************************************
* C_Manager
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Manager::C_Manager(handle hLog, C_EventHub* pHub) : m_cEventQueued(0),
                                                      m_cBroadcastsList(211)
{
  ASSERT(hLog);

  m_hLog = hLog;
  m_pEventHub = pHub;

  m_bStop = false;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_Manager::~C_Manager()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_Manager::Init()
{
  int iRc = NO_ERR;

  try
  {
    InitChannels();
    InitInputs();
    InitPgrmTable();
    InitCommands();
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, "Unable to init manager: " + e.Dump());
    iRc = GEN_ERR;
  }

  C_Application* pApp = C_Application::GetApp();

   C_Vector<C_Setting> vLaunch = pApp->GetSettings("helpers");

  for(unsigned int i=0; i < vLaunch.Size(); i++)
  {
    C_DoublePipe * cPipe = new C_DoublePipe;

    m_cHashDoublePipe.Add(vLaunch[i].GetName(), cPipe);

    Log(m_hLog, LOG_NOTE, "Spawning process " + vLaunch[i].GetName()
                          + " (" + vLaunch[i].GetValue() + ")");


    switch(m_iChildPid=fork())
    {
      case 0:
        /* Child Process : Muxer or helper */
        SpawnHelper(cPipe, vLaunch[i].GetValue());
        break;
      case -1:
        Log(m_hLog, LOG_ERROR, "Fork Error : "
                                 + C_String(strerror(errno)) +  "\n");
        break;
      default:
        /* Father Process : VLS */
        cPipe->FatherClose();
        break;
    }
  }
  
  return iRc;
}


//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
int C_Manager::Run()
{
  int iRc = NO_ERR;

  while(!m_bStop)
  {
    m_cEventQueued.Wait();

    m_cEventFifoLock.Lock();

    if((m_cEventFifo.Size() >= 1) && (!m_bStop))
    {
      C_Event* pEvent = m_cEventFifo.Remove(0);
      ASSERT(pEvent);

      m_cEventFifoLock.UnLock();

      PrivHandleEvent(*pEvent);

      delete pEvent;
    }
    else
    {
      ASSERT(m_bStop);

      m_cEventFifoLock.UnLock();
    }
  }


  return iRc;
}


//------------------------------------------------------------------------------
// Stop the execution
//------------------------------------------------------------------------------
int C_Manager::Stop()
{
  Log(m_hLog, LOG_NOTE, "Stopping the manager");

  int iRc = NO_ERR;

  try
  {
    StopPrograms();
//    C_Thread::Stop();
     m_bStop = true;
     m_cEventQueued.Post();
     Log(m_hLog, LOG_NOTE, "Manager stopped");
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, "Unable to stop the manager:\n" + e.Dump());
    iRc = GEN_ERR;
  } 

  return iRc;
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
int C_Manager::Destroy()
{
  int iRc = NO_ERR;

  Log(m_hLog, LOG_NOTE, "Destroying the manager");

  try
  {
    DestroyPgrmTable();
    DestroyInputs();
    DestroyChannels();
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, "Unable to destroy manager: " + e.Dump());
    iRc = e.GetCode();
  }

  if(!iRc)
    Log(m_hLog, LOG_NOTE, "Manager destroyed");

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::InitPgrmTable()
{
  // Lock the input repository and the program list
  m_cInputList.Lock();
  m_cProgramList.Lock();

  // Get the input lists of programs and merge them with the manager one
  C_RepositoryBrowser<C_String, C_Input> cIterator =
                                                  m_cInputList.CreateBrowser();

  while(cIterator.HasNextItem())
  {
    C_Input* pInput = cIterator.GetNextItem();
    C_List<C_Program> cInputPgrms = pInput->GetAvailablePgrms();
    unsigned int iPgrmNumber = cInputPgrms.Size();
    for(unsigned int j = 0; j < iPgrmNumber; j++)
    {
      m_cProgramList.Add(cInputPgrms[j], pInput);
    }
  }

  // Unlock the input repository and the program list
  m_cInputList.UnLock();
  m_cProgramList.UnLock();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::DestroyPgrmTable()
{
  // Nothing to do yet
}


//------------------------------------------------------------------------------
// Commands Initialization
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::InitCommands()
{
  zzzz *foo;

  AddFunction("ls",                &C_Manager::Ls);
  AddFunction("config",            &C_Manager::Config);

  AddFunction("broadcast_ls",      &C_Manager::Broadcast_Ls);
  AddFunction("broadcast_add",     &C_Manager::Broadcast_Add);
  AddFunction("broadcast_del",     &C_Manager::Broadcast_Del);
  AddFunction("broadcast_start",   &C_Manager::Broadcast_Start)
  AddFunction("broadcast_resume",  &C_Manager::Broadcast_Resume);
  AddFunction("broadcast_suspend", &C_Manager::Broadcast_Suspend);
  AddFunction("broadcast_forward", &C_Manager::Broadcast_Forward);
  AddFunction("broadcast_rewind",  &C_Manager::Broadcast_Rewind);
  AddFunction("broadcast_stop",    &C_Manager::Broadcast_Stop);

  AddFunction("channel_ls",        &C_Manager::Channel_Ls);
  AddFunction("channel_add",       &C_Manager::Channel_Add);
  AddFunction("channel_del",       &C_Manager::Channel_Del);

  AddFunction("program_ls",        &C_Manager::Program_Ls);

#ifdef MANAGER_EXTRA
#  define EXTRA_INIT
#  include MANAGER_EXTRA
#  undef EXTRA_INIT
#endif

}




//------------------------------------------------------------------------------
// Inputs initialization
//------------------------------------------------------------------------------
void C_Manager::InitInputs()
{
  // Get the names of all the sources
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  C_Vector<C_Setting> vInputs = pApp->GetSettings("inputs");

  // Lock the input repository
  m_cInputList.Lock();

  // Create the corresponding inputs
  for(unsigned int i = 0; i < vInputs.Size(); i++)
  {
    // Current input
    C_Input* pInput = NULL;

    // Get input name and type
    C_Setting cCurrentInput = vInputs[i];
    C_String strInputName = cCurrentInput.GetName();
    C_String strInputType = cCurrentInput.GetValue();

    // Create the input for the given source
    C_InputModule* pModule = (C_InputModule*)
                             C_Application::GetModuleManager()
                                        ->GetModule("input", strInputType);
    if(pModule)
    {
      pInput = pModule->NewInput(strInputName);
      ASSERT(pInput);
      pInput->SetEventHandler(this);
      Log(m_hLog, LOG_NOTE, "Starting input '"+strInputName+"'");
      try
      {
        // Initialize the input
        pInput->Init();

        // And register it
        m_cInputList.Add(strInputName, pInput);

        Log(m_hLog, LOG_NOTE, "Input '" + strInputName +
            "' sucessfully initialised");
      }
      catch(E_Exception e)
      {
        Log(m_hLog, LOG_ERROR, "Unable to start input '" + strInputName +
            "': " + e.Dump());
        delete pInput;
      }
    }
    else
    {
      Log(m_hLog, LOG_ERROR, "Input type \"" + strInputType + "\" invalid");
    }
  }

  // Unlock the input repository
  m_cInputList.UnLock();
}


//------------------------------------------------------------------------------
// Inputs destruction
//------------------------------------------------------------------------------
void C_Manager::DestroyInputs()
{
  // Lock the input repository
  m_cInputList.Lock();

  C_RepositoryBrowser<C_String, C_Input> cIterator =
                                                m_cInputList.CreateBrowser();

  while(cIterator.HasNextItem())
  {
    C_Input* pInput = cIterator.GetNextItem();

    try
    {
      pInput->Destroy();
      Log(m_hLog, LOG_NOTE, "Input "+pInput->GetName()+" correctly stopped");
    }
    catch(E_Exception e)
    {
      // Just log the pb and go on with inputs destruction
      C_String strInputName = pInput->GetName();
      Log(m_hLog, LOG_ERROR,
          "Unable to stop input "+strInputName+": "+e.Dump());
    }
  }

  // Unlock the input repository
  m_cInputList.UnLock();
}


//------------------------------------------------------------------------------
// Channels initialization
//------------------------------------------------------------------------------
void C_Manager::InitChannels()
{
  // Lock the channel repository
  m_cChannelList.Lock();

  // Get the names of all the channels
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  C_Vector<C_Setting> vChannels = pApp->GetSettings("channels");
  
  // Create the corresponding channels
  for(unsigned int i = 0; i < vChannels.Size(); i++)
  {
    // Current channel
    C_Channel* pChannel = NULL;

    // Get channel name and type
    C_Setting cCurrentChannel = vChannels[i];
    C_String strChannelName = cCurrentChannel.GetName();
    C_String strChannelType = cCurrentChannel.GetValue();

    C_ChannelModule* pModule = (C_ChannelModule*)
                               C_Application::GetModuleManager()
                                        ->GetModule("channel", strChannelType);
    if(pModule)
    {
      C_String m_strAppend;
      C_ChannelConfig cConfig;
      cConfig.m_strName = strChannelName;
      cConfig.m_strType = strChannelType;
#define GETSETTING(x,y,z) \
      x = pApp->GetSetting(strChannelName+y,z)
      GETSETTING(cConfig.m_strSrcHost,".srchost", "");
      GETSETTING(cConfig.m_strSrcPort,".srcport", "");
      GETSETTING(cConfig.m_strDstHost,".dsthost", "");
      GETSETTING(cConfig.m_strDstPort,".dstport", "1234");
      GETSETTING(cConfig.m_strStreamType, ".streamtype", "unicast").ToLower();
      GETSETTING(cConfig.m_strInterface, ".interface", "");
      GETSETTING(cConfig.m_iTTL, ".ttl", "0").ToInt();
      GETSETTING(cConfig.m_strDomain, ".domain", "inet4").ToLower();
      GETSETTING(cConfig.m_strFilename, ".filename", "fileout.ts");
      GETSETTING(m_strAppend, ".append", "no").ToLower();
      cConfig.m_bAppend = (m_strAppend == "yes") ? true: false;

      pChannel = pModule->NewChannel(cConfig);
      ASSERT(pChannel);
      m_cChannelList.Add(strChannelName, pChannel);
      Log(m_hLog, LOG_NOTE, "Channel '"+strChannelName+"' created");
    }
    else
    {
      Log(m_hLog, LOG_ERROR, "Channel type \"" + strChannelType + "\" invalid");
    }
  }

  // Unlock the channel repository
  m_cChannelList.UnLock();
}


//------------------------------------------------------------------------------
// Channels destruction
//------------------------------------------------------------------------------
void C_Manager::DestroyChannels()
{
  // Just make sure that the channel is free
  // to do
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::StopPrograms()
{
  // Stop all the programs so that the inputs could be destroyed

  m_cBroadcastsList.Lock();

  C_Vector<C_Request> cRequests;

  C_RepositoryBrowser<C_String, C_Broadcast> cIterator1 =
                m_cBroadcastsList.CreateBrowser();

  while(cIterator1.HasNextItem())
  {
    C_Broadcast* pBroadcast = cIterator1.GetNextItem();
    if(pBroadcast->GetStatus()!=BROADCAST_WAITING)
    {
      C_Request* pRequest = new C_Request("broadcast_stop");
      pRequest->SetArg("broadcast", pBroadcast->GetName());
      cRequests.Add(pRequest);
    }
  }

  m_cBroadcastsList.UnLock();

  unsigned int iCount = cRequests.Size();
  for(unsigned int i = 0; i < iCount; i++)
  {
    C_Request& cRequest = cRequests[i];
    HandleRequest(cRequest);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::HandleRequest(C_Request& cRequest)
{
  zzzz *foo;

  m_cCmdLock.Lock();
  LogDbg(m_hLog, "Manager locked");

  C_Answer cAnswer("Manager");

  C_String strCmd = cRequest.GetCmd();

  foo = m_cFunctions.Get(strCmd);
  if(foo)
  {
      cAnswer = (this->**foo)(cRequest);
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Unknown command: "+strCmd);
  }

  m_cCmdLock.UnLock();
  LogDbg(m_hLog, "Manager unlocked");
  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::HandleEvent(const C_Event& cEvent)
{
  m_cCmdLock.Lock();
  LogDbg(m_hLog, "Manager locked");

  m_cEventFifoLock.Lock();
  m_cEventFifo.Add(new C_Event(cEvent));
  m_cEventFifoLock.UnLock();

  m_cEventQueued.Post();

  LogDbg(m_hLog, "Event posted");
  
  m_pEventHub->ForwardEvent(cEvent);
  LogDbg(m_hLog, "Event forwarded");

  m_cCmdLock.UnLock();
  LogDbg(m_hLog, "Manager unlocked");
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Manager::PrivHandleEvent(const C_Event& cEvent)
{
  switch(cEvent.GetCode())
  {
  case EOF_EVENT :
  case CONNEXION_LOST_EVENT :
    C_String strName = cEvent.GetBroadcast()->GetName();
    ASSERT(strName != "");

    // First we have to stop the broadcasts
    C_Request cRequest("broadcast_stop");
    cRequest.SetArg("broadcast", strName);
    C_Answer cAnswer = HandleRequest(cRequest);
    break;
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Ls(C_Request&)
{
  C_Answer cAnswer("Manager");

  cAnswer.AddMessage("Configuration state:");

  C_Answer cInputAns("inputs");
  cInputAns.SetStatus(NO_ERR);
  cInputAns.AddMessage(C_String(m_cInputList.GetSize())
                                                  + " inputs configured");
  cAnswer.Add(cInputAns);
  cAnswer.SetStatus(ANS_NO_ERR);

  C_Answer cChanAns("channels");
  cChanAns.SetStatus(NO_ERR);
  cChanAns.AddMessage(C_String(m_cChannelList.GetSize())
                                                  + " channels configured");
  cAnswer.Add(cChanAns);
  cAnswer.SetStatus(ANS_NO_ERR);

  C_Answer cBdAns("broadcasts");
  cBdAns.SetStatus(NO_ERR);
  cBdAns.AddMessage(C_String(m_cBroadcastsList.GetSize())
                                                 + " broadcasts configured");
  cAnswer.Add(cBdAns);
  cAnswer.SetStatus(ANS_NO_ERR);

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Config(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strFileName = cRequest.GetArg("filename");
  ASSERT(strFileName != "");

  C_String strLoad = cRequest.GetArg("load");
  C_String strSave = cRequest.GetArg("save");

  if ((strLoad == "1") && (strSave == "1"))
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Config does not allow use of --add and --delete at the same time.");
    return cAnswer;
  }
  if ( strLoad == "1" )
  {
      // stop all programs
      StopPrograms();

      // Destroy current configuration
      DestroyPgrmTable();
      DestroyInputs();
      DestroyChannels();

      // TODO should load the new configuration

      // Init new configuration
      InitChannels();
      InitInputs();
      InitPgrmTable();

      cAnswer.AddMessage("Configuration reloaded from default vls.cfg.");

      // debug
      Log(m_hLog, LOG_NOTE, "Configuration loaded.");
  }
  else if ( strSave == "1" )
  {
      // Lock the input repository and the program list
      m_cProgramList.Lock();
      m_cInputList.Lock();
      m_cChannelList.Lock();

      // Dump all settings
      C_Application* pApp = C_Application::GetApp();
      C_Vector<C_Setting> cConfig=pApp->GetAllSettings();
      cConfig.Sort();

      unsigned int k,i;
      unsigned int iTokenCount[cConfig.Size()];
      C_StringTokenizer** cTokens;
      cTokens = new C_StringTokenizer*[cConfig.Size()];

      for(k=0; k<cConfig.Size(); k++)
      {
          cTokens[k] = new C_StringTokenizer(cConfig[k].GetName(),'.');
          iTokenCount[k] = cTokens[k]->CountTokens();
      }
      FILE* fd=fopen(strFileName.GetString(), "w");
      WRITE_BEGINS(fd, iTokenCount[0], cTokens[0], 0);

      for(i=0; i<cConfig.Size(); i++)
      {
          if(i)
          {
                //Compare tokens
                cTokens[i]->Reset();
                cTokens[i-1]->Reset();
                for (k=0;
                     k<iTokenCount[i-1] &&
                     k<iTokenCount[i] &&
                       (cTokens[i]->NextToken()==
                       cTokens[i-1]->NextToken());
                     k++);
                // k is the number of similar tokens
                cTokens[i]->Reset(); cTokens[i-1]->Reset();

                WRITE_ENDS(fd, iTokenCount[i-1], cTokens[i-1], k);
                WRITE_BEGINS(fd, iTokenCount[i], cTokens[i], k);
          }
          WRITE_PROPERTY(fd, iTokenCount[i], cTokens[i]);
          cTokens[i]->Reset();
      }
      WRITE_ENDS(fd, iTokenCount[i-1], cTokens[i-1], 0);

      fclose(fd);
      Log(m_hLog, LOG_NOTE, "Configuration written");

      // Unlock the input repository and the program list
      m_cInputList.UnLock();
      m_cProgramList.UnLock();
      m_cChannelList.UnLock();
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Command expects --load or --save.");
    Log(m_hLog, LOG_ERROR, "Command Syntax Error.");
  }
  return cAnswer;
}

void C_Manager::SpawnHelper(C_DoublePipe * cPipe, C_String strHelperExec)
{
  cPipe->ChildDup2();

  // XXX parse StrHelperExec and isolate Args
  char * const environ[1]={NULL};
  char * const Arg[1]={NULL};
  
  execve(strHelperExec.GetString(), Arg, environ);
  
  Log(m_hLog, LOG_WARN, "Helper process " +
                         strHelperExec + "halted !\n");
  exit(0);
}

#include "manager_broadcast.cpp"
#include "manager_channel.cpp"
#include "manager_program.cpp"

#ifdef MANAGER_EXTRA
#define EXTRA_CODE
#include MANAGER_EXTRA
#undef EXTRA_CODE
#endif
