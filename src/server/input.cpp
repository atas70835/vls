/*******************************************************************************
* input.cpp: Input streams
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"

#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"

#include "program.h"
#include "buffer.h"
#include "broadcast.h"
#include "output.h"
#include "channel.h"
#include "request.h"

#include "input.h"


/*******************************************************************************
* E_Input
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Input::E_Input(const C_String& strInputName, const C_String& strMsg) :
            E_Exception(GEN_ERR, strInputName + ": " + strMsg)
{
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Input::E_Input(const C_String& strInputName, const C_String& strMsg,
                 const E_Exception& e) :
            E_Exception(GEN_ERR, strInputName + ": " + strMsg, e)
{
}


/*******************************************************************************
* C_Input
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Input::C_Input(C_Module* pModule,
                 const C_String& strName) : m_strName(strName)
{
  ASSERT(pModule);

  m_bIsFree = true;
  m_pModule = pModule;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Input::SetEventHandler(C_EventHandler* pEventHandler)
{
  ASSERT(pEventHandler);

  m_pEventHandler = pEventHandler;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Input::C_Input(const C_Input&)
{
  // Input must not be copied
  ASSERT(false);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Input::~C_Input()
{
  m_pModule->Unref();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Input::Init()
{
  LogDbg(NULL, "Initialising input "+m_strName);

  try
  {
    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);

    // First register the source as a log client
    m_hLog = pApp->StartLog(m_strName, pApp->GetLogFlags());
    ASSERT(m_hLog);

    // Now call the child's initialisation method
    OnInit();

    // Undo what has been done during initialisation to release the
    // ressource that may have been allocated at that time
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, e.Dump());
    Destroy();

    throw E_Input(m_strName, "Unable to initialise input: aborting", e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Input::Destroy()
{
  LogDbg(NULL, "Destroying input "+m_strName);

  try
  {
    // Do some child specific cleanings
    OnDestroy();

    // Unregister the input by the logger
    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);
    pApp->StopLog(m_hLog);
  }
  catch(E_Exception e)
  {
    throw E_Input(m_strName, "Unable to destroy input", e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::StartStreaming(C_Broadcast* pBroadcast)
{
  C_Answer cAnswer(GetName());

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  ASSERT(m_bIsFree);
  m_bIsFree = false;

  try
  {
    // Start the reception of the program
    LogDbg(m_hLog, "Starting reception of pgrm "+strPgrmName);
    OnStartStreaming(pBroadcast);

    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_RUNNING);

    // Build the answer
    cAnswer.SetStatus(ANS_NO_ERR);
    cAnswer.AddMessage("Program " + strPgrmName + " started");
  }
  catch(E_Exception e)
  {
    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_ERROR);

    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to start program "+strPgrmName);
    cAnswer.AddMessage(e.Dump());

    // Just write to screen too
    Log(m_hLog, LOG_ERROR, "Unable to start program "+strPgrmName);
    Log(m_hLog, LOG_ERROR, e.Dump());

    // Mark as free again
    ASSERT(!m_bIsFree);
    m_bIsFree = true;
  }

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::ResumeStreaming(C_Broadcast* pBroadcast)
{
  C_Answer cAnswer(GetName());

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  try
  {
    // Ask the child to resume the streaming
    if((pBroadcast->GetStatus() == BROADCAST_SUSPENDED) ||
       (pBroadcast->GetStatus() == BROADCAST_FORWARD)   ||
       (pBroadcast->GetStatus() == BROADCAST_REWIND) )
    {
      LogDbg(m_hLog, "Resuming reception of pgrm "+strPgrmName);
      OnResumeStreaming(pBroadcast);
      pBroadcast->SetStatus(BROADCAST_RUNNING);
      LogDbg(m_hLog, "Pgrm "+strPgrmName+" resumed");

      // Build the answer
      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Program " + strPgrmName + " resumed");
    }
    else
    {
      cAnswer.AddMessage("Program " + strPgrmName + " isn't suspended");
    }
  }
  catch(E_Exception e)
  {
    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_ERROR);

    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to resume program "+strPgrmName);
    cAnswer.AddMessage(e.Dump());
  }

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::SuspendStreaming(C_Broadcast* pBroadcast)
{
  C_Answer cAnswer(GetName());

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  try
  {
    // Ask the child to suspend the streaming
    if( (pBroadcast->GetStatus() == BROADCAST_RUNNING) ||
        (pBroadcast->GetStatus() == BROADCAST_FORWARD) ||
        (pBroadcast->GetStatus() == BROADCAST_REWIND) )
     {
      LogDbg(m_hLog, "Suspending reception of pgrm "+strPgrmName);
      OnSuspendStreaming(pBroadcast);
      pBroadcast->SetStatus(BROADCAST_SUSPENDED);
      LogDbg(m_hLog, "Pgrm "+strPgrmName+" suspended");

      // Build the answer
      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Program " + strPgrmName + " suspended");
    }
    else
    {
      cAnswer.AddMessage("Program " + strPgrmName + " wasn't running");
    }
  }
  catch(E_Exception e)
  {
    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_ERROR);

    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to suspend program "+strPgrmName);
    cAnswer.AddMessage(e.Dump());
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::ForwardStreaming(C_Broadcast* pBroadcast, int speed)
{
  C_Answer cAnswer(GetName());

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  try
  {
    // Ask the child to start trickplay forward of the streaming
    if ( (pBroadcast->GetStatus() == BROADCAST_RUNNING) ||
         (pBroadcast->GetStatus() == BROADCAST_REWIND) ||
         (pBroadcast->GetStatus() == BROADCAST_FORWARD))
    {
      LogDbg(m_hLog, "Trickplay forward reception of pgrm "+strPgrmName);
      OnForwardStreaming(pBroadcast,speed);
      pBroadcast->SetStatus(BROADCAST_FORWARD);
      LogDbg(m_hLog, "Pgrm "+strPgrmName+" trickplay forward started");

      // Build the answer
      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Program " + strPgrmName + " forwarded");
    }
    else
    {
      cAnswer.AddMessage("Program " + strPgrmName + " isn't forwarded");
    }
  }
  catch(E_Exception e)
  {
    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_ERROR);

    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to forward program "+strPgrmName);
    cAnswer.AddMessage(e.Dump());
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::RewindStreaming(C_Broadcast* pBroadcast,int speed)
{
  C_Answer cAnswer(GetName());

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  try
  {
    // Ask the child to start trickplay rewind of the streaming
    if ((pBroadcast->GetStatus() == BROADCAST_RUNNING) ||
        (pBroadcast->GetStatus() == BROADCAST_FORWARD) ||
        (pBroadcast->GetStatus() == BROADCAST_REWIND))
    {
      LogDbg(m_hLog, "Trickplay rewind reception of pgrm "+strPgrmName);
      OnRewindStreaming(pBroadcast,speed);
      pBroadcast->SetStatus(BROADCAST_REWIND);
      LogDbg(m_hLog, "Pgrm "+strPgrmName+" trickplay rewind started");

      // Build the answer
      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Program " + strPgrmName + " rewinded");
    }
    else
    {
      cAnswer.AddMessage("Program " + strPgrmName + " isn't rewinded");
    }
  }
  catch(E_Exception e)
  {
    // Update broadcast status
    pBroadcast->SetStatus(BROADCAST_ERROR);

    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to rewind program "+strPgrmName);
    cAnswer.AddMessage(e.Dump());
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::UpdateProgram(C_String strProgram, C_String strFileName,
                    C_String strType)
{
  C_Answer cAnswer(GetName());

  try
  {
    OnUpdateProgram(strProgram, strFileName, strType);
    LogDbg(m_hLog, "Program "+strProgram+" added.");

    // Build the answer
    cAnswer.SetStatus(NO_ERR);
    cAnswer.AddMessage("Program " + strProgram + " added.");
  }
  catch(E_Exception e)
  {
    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to add program "+strProgram);
    cAnswer.AddMessage(e.Dump());
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::DeleteProgram(C_String strProgram)
{
  C_Answer cAnswer(GetName());

  try
  {
    OnDeleteProgram(strProgram);
    LogDbg(m_hLog, "Configuration Program "+strProgram+" removed.");
    // Build the answer
    cAnswer.SetStatus(NO_ERR);
    cAnswer.AddMessage("Configuration Program " + strProgram + " removed.");
  }
  catch(E_Exception e)
  {
    // Build the answer
    cAnswer.SetStatus(e.GetCode());
    cAnswer.AddMessage("Unable to delete program "+strProgram);
    cAnswer.AddMessage(e.Dump());
  }
  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Input::StopStreaming(C_Broadcast* pBroadcast)
{
  C_Answer cAnswer(GetName());
  cAnswer.SetStatus(NO_ERR);

  const C_Program* pPgrm = pBroadcast->GetProgram();
  const C_String strPgrmName = pPgrm->GetName();

  try
  {
    // Ask the child to stop the streaming
    if((pBroadcast->GetStatus() == BROADCAST_RUNNING) ||
       (pBroadcast->GetStatus() == BROADCAST_FORWARD) ||
       (pBroadcast->GetStatus() == BROADCAST_REWIND)  ||
       (pBroadcast->GetStatus() == BROADCAST_SUSPENDED))    {
      LogDbg(m_hLog, "Stopping reception of pgrm "+strPgrmName);
      OnStopStreaming(pBroadcast);
      pBroadcast->SetStatus(BROADCAST_STOPPED);
      LogDbg(m_hLog, "Pgrm "+strPgrmName+" stopped");

      // Build the answer
      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Program " + strPgrmName + " stopped");
    }
    else
    {
      cAnswer.AddMessage("Program " + strPgrmName + " not broadcasted");
    }
  }
  catch(E_Exception e)
  {
    LogDbg(m_hLog, "Error when stopping program "+strPgrmName);
    cAnswer.AddMessage("Error when stopping program " + strPgrmName);
    cAnswer.SetStatus(e.GetCode());
  }
  // Mark as free again
  ASSERT(!m_bIsFree);
  m_bIsFree = true;

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_Input::GetAvailablePgrms()
{
  return OnGetAvailablePgrms();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const C_String& C_Input::GetName() const
{
  return m_strName;
}


bool C_Input::operator == (const C_Input& cInput) const
{
  // The test is made on the input address: since all remote input have a local
  // instance of the C_RemoteInput class, this will work in all cases
  return (this == &cInput);
}


