/*******************************************************************************
* program.cpp: program management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: program.cpp,v 1.3 2003/10/27 10:58:12 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/
#include "config.h"

#include "../core/core.h"
#include "program.h"


/*******************************************************************************
* C_Program class
********************************************************************************
* A program is always locked when it is not part of a directory or when it is
* accessed from its directory
* Lock checks are only made in DEBUG mode for efficiency reasons
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Program::C_Program(/*u16 iPgrmNumber,*/ const C_String& strName,
                     u16 iPmtPid, u16 iPcrPid) : m_strName(strName)
{
//  m_iPgrmNumber = iPgrmNumber;
  m_iPmtPid = iPmtPid;
  m_iPcrPid = iPcrPid;

  // Lock the program since it is not part of a directory yet. The lock will
  // be removed when the program will be added to a directory
  //Lock();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Program::~C_Program()
{
  // Ensure proper mutex destruction
  //UnLock();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Program* C_Program::Clone() const
{
  return new C_Program(*this);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::Lock()
{
  m_cLock.Lock();

  //printf("Locking pgrm %d\n", m_iPgrmNumber);

#ifdef DEBUG
  m_bIsLocked = true;
  m_pLockOwner = C_Thread::Self();
#endif
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::UnLock()
{
  ASSERT(m_bIsLocked);
  ASSERT(m_pLockOwner == C_Thread::Self());

  //printf("Unlocking pgrm %d\n", m_iPgrmNumber);

#ifdef DEBUG
  m_bIsLocked = false;
#endif

  m_cLock.UnLock();
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const C_String& C_Program::GetName() const
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  return m_strName;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_Program::GetDescription() const
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  //C_String strDescr("Program Number ");
  //strDescr += GetPgrmNumber();
  C_String strDescr( "" );

  if(m_iPcrPid != (u16)-1)
    strDescr += "PCR on PID " + m_iPcrPid + C_String(", ");

  if(m_iPmtPid != (u16)-1)
    strDescr += "PSI on PID " + m_iPmtPid;

  if(strDescr == "")
    strDescr = "None";

  return strDescr;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//u16 C_Program::GetPgrmNumber() const
//{
//  //ASSERT(m_bIsLocked);
//  //ASSERT(m_pLockOwner == C_Thread::Self());
//
//  return m_iPgrmNumber;
//}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
u16 C_Program::GetPcrPid() const
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  return m_iPcrPid;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
u16 C_Program::GetPmtPid() const
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  return m_iPmtPid;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*u32 C_Program::GetProviderId() const
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  return m_iProviderId;
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::UpdateName(const C_String& strName)
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  int iPos = m_strName.Find("(was ");
  if(iPos > 0)
  {
    C_String m_strOldName = m_strName.SubString(iPos, m_strName.Length());
    m_strName = strName + "(was " + m_strOldName + ")";
  }
  else
    m_strName = strName + "(was " + m_strName + ")";
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::SetPmtPid(u16 iPmtPid)
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  m_iPmtPid = iPmtPid;
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::SetPcrPid(u16 iPcrPid)
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  m_iPcrPid = iPcrPid;
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*void C_Program::SetProviderId(u32 iProviderId)
{
  //ASSERT(m_bIsLocked);
  //ASSERT(m_pLockOwner == C_Thread::Self());

  m_iProviderId = iProviderId;
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//bool C_Program::operator == (const C_Program& cProgram) const
//{
//  // The 2 programs should be locked by the same thread to avoid
//  // data inconsistencies during comparison
//  //ASSERT(m_bIsLocked);
//  //ASSERT(m_pLockOwner == C_Thread::Self());
//
//  //ASSERT(cProgram.m_bIsLocked);
//  //ASSERT(cProgram.m_pLockOwner == C_Thread::Self());
//
//  return m_iPgrmNumber == cProgram.m_iPgrmNumber;
//}

