/*******************************************************************************
* manager_channel.cpp: Channel methods for Vls manager
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*          Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/
C_Answer C_Manager::Channel_Ls(C_Request&)
{
  C_Answer cAnswer("Manager");
  cAnswer.AddMessage("channels");

  // Lock the channel repository
  m_cChannelList.Lock();

  // Get the names of all the channels
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  C_Vector<C_Setting> vChannels = pApp->GetSettings("channels");

  // Create the corresponding channels
  for(unsigned int i = 0; i < vChannels.Size(); i++)
  {
    // Get channel name and type
    C_Setting cCurrentChannel = vChannels[i];
    C_String strChannelName = cCurrentChannel.GetName();
    C_String strChannelType = cCurrentChannel.GetValue();

    // Get all the specific settings
    C_Vector<C_Setting> vSettings = pApp->GetSettings(strChannelName);
    C_String strSettings="";
    for(unsigned int i = 0; i < vSettings.Size(); i++)
    {
        if(i!=0) strSettings += " ";
        strSettings += vSettings[i].GetName()+" "+ vSettings[i].GetValue();

    }

    // Create answer
    C_Answer cChannelDescr(strChannelName);
    cChannelDescr.SetStatus(NO_ERR);
    if(strSettings!="")
    {
        cChannelDescr.AddMessage("channel "+strChannelName+
                                      " type "+strChannelType+" "+strSettings);
    }
    else
    {
        cChannelDescr.AddMessage("channel "+strChannelName+
                                                      " type "+strChannelType);
    }
    cAnswer.Add(cChannelDescr);
  }

  // Unlock the channel repository
  m_cChannelList.UnLock();
  cAnswer.SetStatus(ANS_NO_ERR);

  return cAnswer;
}





C_Answer C_Manager::Channel_Add (C_Request& cRequest)
{
  C_ChannelConfig cConfig;
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  cConfig.m_strName = cRequest.GetArg("name");
  cConfig.m_strType = cRequest.GetArg("type");
  cConfig.m_strDstHost = cRequest.GetArg("dest_host");
  cConfig.m_strDstPort = cRequest.GetArg("dest_port");
  cConfig.m_strStreamType = cRequest.GetArg("streamtype");

  // Optional arguments
  cConfig.m_strDomain = cRequest.GetArg("domain");
  if (cConfig.m_strDomain == "") cConfig.m_strDomain = "inet4";

  cConfig.m_iTTL = cRequest.GetArg("ttl").ToInt();
  if(cConfig.m_iTTL == 0) cConfig.m_iTTL = 1;

  cConfig.m_strInterface = cRequest.GetArg("interface");

  // check if channel is already known
  C_Channel* pChannel = m_cChannelList.Get(cConfig.m_strName);
  if(pChannel)
  {
      cAnswer.SetStatus(ANS_CHANNEL_BUSY);
      cAnswer.AddMessage("Channel already present");
      m_cChannelList.Release(cConfig.m_strName);
      return cAnswer;
  }

  // Lock the input repository and the program list
  m_cChannelList.Lock();

  // Get the names of all the channels
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  C_String strKey = C_String("channels.") + cConfig.m_strName;
  pApp->SetSettings( strKey, cConfig.m_strType );
  strKey = cConfig.m_strName+C_String(".dsthost");
  pApp->SetSettings( strKey, cConfig.m_strDstHost );
  strKey = cConfig.m_strName+C_String(".dstport");
  pApp->SetSettings( strKey, cConfig.m_strDstPort );
  strKey = cConfig.m_strName+C_String(".domain");
  pApp->SetSettings( strKey, cConfig.m_strDomain );
  strKey = cConfig.m_strName+C_String(".ttl");
  C_String a=C_String("");
  a += cConfig.m_iTTL;
  pApp->SetSettings( strKey, a );
  if(cConfig.m_strStreamType!="")
  {
      strKey = cConfig.m_strName+C_String(".streamtype");
      pApp->SetSettings( strKey, cConfig.m_strStreamType );
  }
  if(cConfig.m_strInterface!="")
  {
      strKey = cConfig.m_strName+C_String(".interface");
      pApp->SetSettings( strKey, cConfig.m_strInterface );
  }


  C_ChannelModule* pModule = (C_ChannelModule*)
                                 C_Application::GetModuleManager()
                                      ->GetModule("channel", cConfig.m_strType);
  if(pModule)
  {
      // Current channel
      C_Channel* pChannel = NULL;
      pChannel = pModule->NewChannel(cConfig);
      ASSERT(pChannel);
      m_cChannelList.Add(cConfig.m_strName, pChannel);
      Log(m_hLog, LOG_NOTE, "Channel '"+cConfig.m_strName+"' created");
  }
  else
  {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Channel type is invalid.");
      Log(m_hLog, LOG_ERROR, "Channel type \"" + cConfig.m_strType
                                                            + "\" invalid");
  }

  // Unlock the chanel list
  m_cChannelList.UnLock();

  return cAnswer;
}




C_Answer C_Manager::Channel_Del(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strChannelName = cRequest.GetArg("name");
  ASSERT(strChannelName != "");

/*  C_String strChannelType = cRequest.GetArg("type");
  ASSERT(strChannelType != "");

  C_String strDstHost = cRequest.GetArg("dest_host");
  ASSERT(strDstHost != "");

  C_String strDstPort = cRequest.GetArg("dest_port");
  ASSERT(strDstPort != "");

  C_String strStreamType = cRequest.GetArg("streamtype");
  ASSERT(strStreamType != "");

  // Optional arguments
  C_String strDomain = cRequest.GetArg("domain");
  if (strDomain == "")
     strDomain = "inet4";

  C_String strTTL = cRequest.GetArg("ttl");
  C_String strInterface = cRequest.GetArg("interface");*/
    // check if channel is already known
    C_Channel* pChannel = m_cChannelList.Get(strChannelName);
    if(!pChannel)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Channel is unknown");
      return cAnswer;
    }
    else if(!pChannel->IsFree())
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Channel is busy");
      pChannel = NULL;
      m_cChannelList.Release(strChannelName);
      return cAnswer;
    }
    m_cChannelList.Release(strChannelName);

    // Lock the input repository and the program list
    m_cChannelList.Lock();

    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);

    // So now, we can remove channel from all the lists
    int iRc = m_cChannelList.Remove(strChannelName);
    if (iRc)
    {
      Log(m_hLog, LOG_NOTE, "Channel '" + strChannelName +
                                                 "' can not be deleted !");
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Channel can not be deleted");
    }
    else
    {
      C_String strKey = C_String("channels.")+strChannelName;
      if (pApp->GetSetting(strKey, "") != "")
         pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".dsthost") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".dstport") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".type") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".domain") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".interface") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strChannelName + C_String(".ttl");
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey );
      Log(m_hLog, LOG_NOTE, "Channel '" + strChannelName + "' removed !");
    }
    m_cChannelList.UnLock();
  return cAnswer;
}
