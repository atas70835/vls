/*******************************************************************************
* channel.cpp: Channel encapsulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: channel.cpp,v 1.9 2003/11/18 17:09:40 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"
#include "buffer.h"
#include "output.h"
#include "channel.h"



//******************************************************************************
// class C_Channel
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Channel::C_Channel(C_Module* pModule, const C_ChannelConfig& cConfig) :
                                       m_strName(cConfig.m_strName)
{
  ASSERT(pModule);

  m_bIsFree = true;
  m_pModule = pModule;
  iHelperProcessId = -1;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Channel::~C_Channel()
{
  ASSERT(m_pOutput);
  if (m_pOutput) delete m_pOutput;

  m_pModule->Unref();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Output* C_Channel::GetOutput()
{
  // Pb of race condition -> put a lock in the future ?
  ASSERT(m_bIsFree);
  m_bIsFree = false;

  ASSERT(m_pOutput);
  return m_pOutput;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Channel::ReleaseOutput()
{
  ASSERT(m_pOutput);
  ASSERT(!m_bIsFree);
  m_bIsFree = true;
}


