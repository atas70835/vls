/*******************************************************************************
* manager_broadcast.cpp: Brodacast mtehods for Vls manager
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*          Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


C_Broadcast* C_Manager::CreateBroadcast(C_String& strName,
                                        C_String& strChannel,
                                        C_String& strPgrm,
                                        C_String& strInput,
                                        C_Answer* cAnswer)
{
    C_Broadcast* pBroadcast = NULL;
    // Find the channel
    ASSERT(strChannel != "");
    C_Channel* pChannel = m_cChannelList.Get(strChannel);
    if(!pChannel)
    {
      cAnswer->SetStatus(ANS_CHANNEL_UNKNOWN);
      cAnswer->AddMessage("Unknown channel");
    }
    else if(!pChannel->IsFree())
    {
      cAnswer->SetStatus(ANS_CHANNEL_BUSY);
      cAnswer->AddMessage("Channel is busy");
      pChannel = NULL;
    }

    // Find the pgrm
    ASSERT(strPgrm != "");
    const C_Program* pPgrm = m_cProgramList.GetPgrm(strPgrm);
    if(!pPgrm)
    {
      cAnswer->SetStatus(ANS_PROGRAM_UNKNOWN);
      cAnswer->AddMessage("Unknown pgrm "+strPgrm);
    }

    // Find the input
    C_Input* pInput = m_cInputList.Get(strInput);
    if(!pInput)
    {
      cAnswer->SetStatus(ANS_INPUT_UNKNOWN);
      cAnswer->AddMessage("input doesn't exist");
    }

    // We have all the elements, we can create the broadcast
    if(pPgrm && pInput && pChannel)
    {
      pBroadcast = new C_Broadcast(pPgrm, pInput, pChannel, strName);
      m_cBroadcastsList.Lock();
      m_cBroadcastsList.Add(strName, pBroadcast);
      m_cBroadcastsList.UnLock();
    }
    if ( pChannel ) m_cChannelList.Release(strChannel);


    return pBroadcast;
}





C_Answer C_Manager::Broadcast_Ls(C_Request&)
{
  C_Answer cAnswer("Manager");

    cAnswer.AddMessage("broadcasts");

    // Lock Broadcast repository
    m_cBroadcastsList.Lock();

    C_RepositoryBrowser<C_String, C_Broadcast> cIterator1 =
                  m_cBroadcastsList.CreateBrowser();

    while(cIterator1.HasNextItem())
    {
      C_Broadcast* pBroadcast = cIterator1.GetNextItem();
      C_Answer cBroadcast(pBroadcast->GetChannel()->GetName());
      cBroadcast.SetStatus(NO_ERR);
      cBroadcast.AddMessage("broadcast "+pBroadcast->GetName());
      cBroadcast.AddMessage("channel "+pBroadcast->GetChannel()->GetName());
      cBroadcast.AddMessage("program "+pBroadcast->GetProgram()->GetName());
      cBroadcast.AddMessage("input "+pBroadcast->GetInput()->GetName());
      cBroadcast.AddMessage("status "+C_String(pBroadcast->GetStatus()));
      cBroadcast.AddMessage("rtp "+pBroadcast->GetOption("rtp"));
      cBroadcast.AddMessage("end "+pBroadcast->GetOption("end"));
      cBroadcast.AddMessage("loop "+pBroadcast->GetOption("loop"));
      cAnswer.Add(cBroadcast);
    }

    // UnLock Broadcast repository
    m_cBroadcastsList.UnLock();
    cAnswer.SetStatus(ANS_NO_ERR);

    return cAnswer;
}

C_Answer C_Manager::Broadcast_Add(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  C_Broadcast* pBroadcast;
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strName = cRequest.GetArg("name");
  C_String strChannel = cRequest.GetArg("channel");
  C_String strPgrm = cRequest.GetArg("program");
  C_String strInput = cRequest.GetArg("input");
  pBroadcast = CreateBroadcast(strName, strChannel, strPgrm, strInput,
                                                                      &cAnswer);
  if(pBroadcast)
  {
    m_cBroadcastsList.Lock();
    pBroadcast->SetOptions(cRequest.GetArgs());
    m_cBroadcastsList.UnLock();
  }

  // TODO add the broadcast in the configuration file
  return cAnswer;
}


C_Answer C_Manager::Broadcast_Del(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strName = cRequest.GetArg("broadcast");

  // check if broadcast is already known
  C_Broadcast* pBroadcast = m_cBroadcastsList.Get(strName);


  if(!pBroadcast)
  {
      cAnswer.SetStatus(ANS_BROADCAST_UNKNOWN);
      cAnswer.AddMessage("Broadcast is unknown");
      return cAnswer;
  }
  else if(pBroadcast->GetStatus() != BROADCAST_STOPPED &&
          pBroadcast->GetStatus() != BROADCAST_WAITING )
  {
      if(cRequest.GetArg("force")=="1")
      {
        C_Input* pInput = pBroadcast->GetInput();
        C_Answer cInputAnswer = pInput->StopStreaming(pBroadcast);
        if(cInputAnswer.GetStatus())
        {
            cAnswer.SetStatus(cInputAnswer.GetStatus());
        }
        cAnswer.Add(cInputAnswer);

        //Release repository items (Channel, Program, Input)
        m_cProgramList.ReleasePgrm(pBroadcast->GetProgram()->GetName());
        m_cInputList.Release(pInput->GetName());
      }
      else
      {
        cAnswer.SetStatus(ANS_BROADCAST_BUSY);
        cAnswer.AddMessage("Broadcast is busy");
        pBroadcast = NULL;
        m_cBroadcastsList.Release(strName);
        return cAnswer;
      }
  }


  m_cBroadcastsList.Release(strName);

  // Lock the broadcast list
  m_cBroadcastsList.Lock();

  // So now, we can remove broadcast from all the lists
  int iRc = m_cBroadcastsList.Remove(strName);
  if (iRc)
  {
      Log(m_hLog, LOG_NOTE, "Broadcast '" + strName +
                                                 "' can not be deleted !");
      cAnswer.SetStatus(ANS_INTERNAL_ERROR);
      cAnswer.AddMessage("Broadcast can not be deleted");
  }
  else
  {
      /* TODO save broadcasts in the config file and remove it there.
      C_String strKey = C_String("broadcasts.")+strName;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      strKey = strName + C_String(".dsthost") ;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey); */
      Log(m_hLog, LOG_NOTE, "Broadcast '" + strName + "' removed !");
  }
  m_cBroadcastsList.UnLock();
  return cAnswer;
}



C_Answer C_Manager::Broadcast_Start(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");

  C_String strName = cRequest.GetArg("broadcast");
  ASSERT(strName != "");

  // Find the broadcast
  m_cBroadcastsList.Lock();
  C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strName);

  if(pBroadcast)
  {
//    pBroadcast->SetOptions(cRequest.GetArgs());
    C_Answer cInputAnswer = pBroadcast->GetInput()->StartStreaming(pBroadcast);
    cAnswer.Add(cInputAnswer);
    cAnswer.SetStatus(cInputAnswer.GetStatus());
  }
  else
  {
      cAnswer.SetStatus(ANS_BROADCAST_UNKNOWN);
      cAnswer.AddMessage("Unknown broadcast");
  }

  m_cBroadcastsList.UnLock();

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast_Resume(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strChannel = cRequest.GetArg("broadcast");
  ASSERT(strChannel != "");

  m_cBroadcastsList.Lock();
  C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strChannel);
  if (pBroadcast)
  {
    C_Input* pInput = pBroadcast->GetInput();

    C_Answer cInputAnswer = pInput->ResumeStreaming(pBroadcast);
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Nothing broadcasted to channel " + strChannel);
  }
  m_cBroadcastsList.UnLock();

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast_Suspend(C_Request& cRequest)
{
   C_Answer cAnswer("Manager");
   cAnswer.SetStatus(ANS_NO_ERR);

   C_String strChannel = cRequest.GetArg("broadcast");
   ASSERT(strChannel != "");

   m_cBroadcastsList.Lock();
   C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strChannel);
   if (pBroadcast)
   {
     C_Input* pInput = pBroadcast->GetInput();

     C_Answer cInputAnswer = pInput->SuspendStreaming(pBroadcast);
     if(cInputAnswer.GetStatus())
     {
       cAnswer.SetStatus(cInputAnswer.GetStatus());
     }
     cAnswer.Add(cInputAnswer);

     // Release repository items (Program, Input)
     /*m_cProgramList.ReleasePgrm(pBroadcast->GetProgram()->GetName());
     m_cInputList.Release(pInput->GetName());*/
   }
   else
   {
     cAnswer.SetStatus(GEN_ERR);
     cAnswer.AddMessage("Nothing broadcasted to channel " + strChannel);
   }
   m_cBroadcastsList.UnLock();

   return cAnswer;
}

 //------------------------------------------------------------------------------
 //
 //------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast_Forward(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strChannel = cRequest.GetArg("broadcast");
  ASSERT(strChannel != "");

  C_String strSpeed = cRequest.GetArg("speed");
  ASSERT(strSpeed != "");

  m_cBroadcastsList.Lock();
  C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strChannel);
  if (pBroadcast)
  {
    C_Input* pInput = pBroadcast->GetInput();
    C_Answer cInputAnswer = pInput->ForwardStreaming(pBroadcast,strSpeed.ToInt());
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Pgrm was not broadcasted");
  }
  m_cBroadcastsList.UnLock();

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast_Rewind(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strChannel = cRequest.GetArg("broadcast");
  ASSERT(strChannel != "");

  C_String strSpeed = cRequest.GetArg("speed");
  ASSERT(strSpeed != "");
  m_cBroadcastsList.Lock();
  C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strChannel);
  if (pBroadcast)
  {
    C_Input* pInput = pBroadcast->GetInput();      
    C_Answer cInputAnswer = pInput->RewindStreaming(pBroadcast,strSpeed.ToInt());
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Pgrm was not broadcasted");
  }
  m_cBroadcastsList.UnLock();
  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast_Stop(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strChannel = cRequest.GetArg("broadcast");
  ASSERT(strChannel != "");

  m_cBroadcastsList.Lock();
  C_Broadcast* pBroadcast = m_cBroadcastsList.Find(strChannel);
  if (pBroadcast)
  {
    C_Input* pInput = pBroadcast->GetInput();
    C_Answer cInputAnswer = pInput->StopStreaming(pBroadcast);

    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

    // Release repository items (Channel, Program, Input)
    m_cProgramList.ReleasePgrm(pBroadcast->GetProgram()->GetName());
    m_cInputList.Release(pInput->GetName());

    LogDbg(m_hLog, "Remove the broadcast");
#ifdef DEBUG
    int iRc = m_cBroadcastsList.Remove(strChannel);
#else
    m_cBroadcastsList.Remove(strChannel);
#endif
    LogDbg(m_hLog, C_String("Broadcast removed with status ") + iRc);
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Nothing broadcasted to channel " + strChannel);
    fprintf(stderr, "nitrox --- nothing broadcasted to this bbroadcast\n");
  }
  m_cBroadcastsList.UnLock();

  return cAnswer;
}

