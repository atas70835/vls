

// TODO Creer plutot une class EventListener globale partagee avec le Timer
class C_SchedulerEvent
{
  
};


// The C_Date represents an instant in time with a precision of a millisecond
class C_Date
{
 public:
  // Initialise the date to the time at which the instance was created
  C_Date();
  // Milliseconds since 1970
  C_Date(unsigned long iMilliSecond);

  // Modify the value of the date so that it represents the current time
  SetCurrentTime();

  // 
};


class C_Scheduler
{
 public:
  void Init();
  void Destroy();
  
  // Ca roule, iPeriod = 0 -> pas de repetition
  void Schedule(C_Date cStartDate, unsigned int iPeriod, C_SchedulerEvent);
  // Pb si le meme event a ete schedule plusieurs fois
  void Cancel(C_SchedulerEvent);
  
 private:
  C_Timer m_cTimer;
};

