/*******************************************************************************
* repository.h: Repository class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: repository.h,v 1.6 2003/08/14 08:50:14 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _REPOSITORY_H_
#define _REPOSITORY_H_



//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
template <class Key, class Data> class C_Repository;


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> class C_RepositoryItem
{
 public:
  C_RepositoryItem(T* pItem)
  {
    ASSERT(pItem);
    m_pItem = pItem;
    m_iRefCounter = 0;
  }

  ~C_RepositoryItem()
  {
    ASSERT(m_iRefCounter == 0);
    delete m_pItem;
  }

  C_RepositoryItem(const C_RepositoryItem<T>& )
  {
    ASSERT(false);
  }


  T* GetItem()
  { return m_pItem; }

  unsigned int GetRefCount() const
  { return m_iRefCounter; }
  void IncreaseRefCount()
  { m_iRefCounter++; }
  void DecreaseRefCount()
  { m_iRefCounter--; }

 private:
  T* m_pItem;
  unsigned int m_iRefCounter;
};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Data> class C_RepositoryBrowser
{
 public:
  inline C_RepositoryBrowser(const C_Repository<Key, Data>& cRepository);

  // Items browsing
  bool HasNextItem()
  { return m_cDataIterator.HasNext(); }
  inline Data* GetNextItem();

  // Keys browsing
  bool HasNextkey()
  { return m_cKeyIterator.HasNext(); }
  Key GetNextKey()
  { return m_cKeyIterator.GetNext()->GetKey(); }

 private:
  C_HashTableIterator<Key, C_RepositoryItem<Data> > m_cKeyIterator;
  C_HashTableIterator<Key, C_RepositoryItem<Data> > m_cDataIterator;
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Thread safe repository. Locking rules should be transparent, they
// are summarised in the .cpp file
//------------------------------------------------------------------------------
template <class Key, class Data> class C_Repository
{
  friend class C_RepositoryBrowser<Key, Data>;

 public:
#ifdef DEBUG
  C_Repository()
  {
    m_bIsLocked = false;
    m_pLockOwner = NULL;
  };
#else
  C_Repository() {};
#endif
  C_Repository(unsigned int uisize);

  // Repository locking
  void Lock();
  void UnLock();

  // Global repository update
  // Should only be called when a lock on the repository has been taken 
  int Add(const Key& cKey, Data* pItem);
  int Remove(const Key& cKey);
  void Empty();
  int GetSize() {return m_cItems.Size();};

  // Access to items. When a global lock on the repository is owned, the
  // [] must be used instead 
  Data* Get(const Key& cKey);
  void Release(const Key& cKey);

  // Repository browsing. Must be used only when a lock on the
  // repository is owned
  Data* operator [] (const Key& cKey);
  Data* Find(const Key& cKey);
  C_RepositoryBrowser<Key, Data> CreateBrowser() const;

private:
  // Table of available items
  C_HashTable<Key, C_RepositoryItem<Data> > m_cItems;

  // Directory locking
  C_Mutex m_cLock;
  //  C_Condition m_cModifAllowed;
  //  unsigned int m_iPendingModifs;

#ifdef DEBUG
  bool m_bIsLocked;
  C_Thread* m_pLockOwner;
#endif
};



#else
#error "Multiple inclusions of repository.h"
#endif

