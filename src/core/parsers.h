/*******************************************************************************
* parsers.h: Command line and configuration files parsers
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _PARSERS_H_
#define _PARSERS_H_


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class E_Parser : public E_Exception
{
 public:
  E_Parser(int iCode, const C_String& strMsg);
  E_Parser(int iCode, const C_String& strMsg, E_Exception e);
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_ParserHandler
{
 public:
  // Called when the beginning of a section or a subsection is found
  virtual void OnStartSection(const C_String& strName) = 0;
  // Called upon end of a section
  virtual void OnEndSection(const C_String& strName) = 0;
  // Called when an entry is parsed
  virtual void OnProperty(const C_String& strName, const C_String& strVal) = 0;
  // To know the current section
  virtual C_String GetSection()=0;
  //KLUDGE
  virtual void ResetLocalCount()=0;
  virtual int GetLocalCount()=0;
  virtual void SetRoot(const C_String& strR)=0;
  virtual ~C_ParserHandler(void) { }
};



//------------------------------------------------------------------------------
// C_CfgFileParser: configuration file parsing
//------------------------------------------------------------------------------
// Parse configuration files whose format must be:
//  BEGIN Section
//    Setting = Value
//    ...
//  END
// Comments must follow a # char, empty lines and subsections are allowed.
//------------------------------------------------------------------------------
class C_CfgFileParser
{
 public:
  // Constructor
  C_CfgFileParser(C_ParserHandler* pCallBack);

  // Parse the data on the given stream
  C_Vector<C_String> Parse(const C_String& strFileName, bool bCompletePath, const C_String& strRoot);
  void AddCmdLineSetting(unsigned char sOption, C_String sValue);
  void AddCmdLineTarget(C_String sTarget, C_String sTags);

 protected:
  C_String ParseDVBTarget(C_String);
  C_String ParseDVDTarget(C_String);
  C_String ParseFILETarget(C_String);
  C_String ParseVIDEOTarget(C_String);
  C_String ParseV4LTarget(C_String);

  // Callback
  C_ParserHandler* m_pCallBack;
  
  // Stream info
  C_Stream<C_File>* m_pStream;
  unsigned int m_iLineNumber;

  // Final state machine tools
  C_Fifo<C_String> m_cContextStack;
};



#else
#error "Multiple inclusions of parsers.h"
#endif
