/*******************************************************************************
* File.h: File class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: file.h,v 1.5 2003/06/09 13:37:12 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This class encapsulate the ANSI C/C++ file access fonctions, and uses its
* semantic. It is therefore portable on every plateform
*
*******************************************************************************/


#ifndef _FILE_H_
#define _FILE_H_


//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

#define FILE_DEFAULT_RIGHTS          1

#define FILE_SEEK_CURRENT            SEEK_CUR
#define FILE_SEEK_BEGIN              SEEK_SET
#define FILE_SEEK_END                SEEK_END


//------------------------------------------------------------------------------
// File Exception
//------------------------------------------------------------------------------
class E_File : public E_Exception
{
 public:
  E_File(const C_String& strMsg);
};


//------------------------------------------------------------------------------
// Directory Exception
//------------------------------------------------------------------------------
class E_Directory : public E_Exception
{
 public:
  E_Directory(const C_String& strMsg);
};


//------------------------------------------------------------------------------
// C_File class
//------------------------------------------------------------------------------
class C_File
{
public:
  C_File(const C_String& strPath);
  void Open(const C_String& strMode, int iPermissions = FILE_DEFAULT_RIGHTS);
  void Close();

  s64 Read(byte* pBuff, s64 iSize);
  s64 Write(const byte* pData, s64 iSize);

  s64 GetPos();
  s64 Size() const;
  s64 Seek(s64 iOffset, s64 bStartPos);
                         
  bool Exists() const;

  C_String GetName() const;
  C_String GetInfo() const;
  FILE* GetHandle() const;

private:
  C_String m_strPath;
  FILE* m_hFd;
};


//------------------------------------------------------------------------------
// C_LinkedFile class
//------------------------------------------------------------------------------
class C_LinkedFile
{
public:
  C_LinkedFile(C_Vector<C_String>& vFileNames);
  void Open(const C_String& strMode, int iPermissions = FILE_DEFAULT_RIGHTS);
  void Close();

  s64 Read(byte* pBuff, s64 iSize);

  s64 GetPos();
  s64 Seek(s64 iOffset, s64 bStartPos);

private:
  unsigned int m_iFileCount;
  C_Vector<C_String> m_vFileNames;
  C_Vector<C_File> m_cFiles;

  unsigned int m_iFileIndex;
  C_File* m_pCurrentFile;
};


//------------------------------------------------------------------------------
// C_Directory class
//------------------------------------------------------------------------------
class C_Directory
{
 public:
  C_Directory(const C_String& strPath);
  
  void Open();
  void Close();

  // Resynch the class contents with the actual content of the directory
  int Update();
  
  const C_Vector<C_String>& GetFiles() const;
  const C_Vector<C_String>& GetSubDirs() const;

private:
  C_String m_strPath;

  C_Vector<C_String> m_vFiles;
  C_Vector<C_String> m_vSubDirs;

#ifdef HAVE_OPENDIR
  DIR* m_pDir;
#endif
};



#else
#error "Multiple inclusions of file.h"
#endif

