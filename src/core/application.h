/*******************************************************************************
* application.h: Application class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: application.h,v 1.7 2003/06/02 09:22:45 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The C_Application class provides some core mecanisms for applications, such
* as a global logging service
*
*******************************************************************************/


#ifndef _APPLICATION_H_
#define _APPLICATION_H_


//------------------------------------------------------------------------------
// C_Application class
//------------------------------------------------------------------------------
// The C_Application class is a singleton: only one instance can therefore 
// exist at a time. The constructor is therefore protected. To create your
// application, you can either also protect your wn constructor or hide it and
// provide a static Create method that will ensure that only one application is
// created at a time.
//------------------------------------------------------------------------------
class C_Application
{
 public:
  // Construction / Destruction
  virtual ~C_Application();

  // Access to the appliction object
  static void SetApp(C_Application* pApp)
  { s_pApplication = pApp; };
  static C_Application* GetApp()
  { ASSERT(s_pApplication); return s_pApplication; };

  static C_ModuleManager* GetModuleManager()
  {
    ASSERT(s_pApplication);
    ASSERT(s_pApplication->m_pModuleManager);
    return s_pApplication->m_pModuleManager;
  };

  // Application control
  int Init(int iArgc, char* paArg[]);
  int Run();
  int Stop();
  int Destroy();

  // Access to the settings
  C_String GetSetting(const C_String& strSetting, const C_String& strDflt) const
  { return m_cSettings.GetSetting(strSetting, strDflt); }
  C_Vector<C_Setting> GetSettings(const C_String& strSettingsGroup) const
  { return m_cSettings.GetSettings(strSettingsGroup); }
  C_Vector<C_Setting> GetAllSettings(void) const
  { return m_cSettings.GetAllSettings(); }
  void SetSettings(C_String& strKey, C_String& strValue)
  { m_cSettings.Update(strKey.ToLower(), strValue.ToLower()); }
  void DeleteSetting(C_String& strKey)
  { m_cSettings.Delete(strKey); }

  // Access to the logging service
  handle StartLog(const C_String& strClientDescr, u8 iFlags);
  void StopLog(handle hLog);
  void LogMsg(handle hLog, int iLevel, const C_String& strMsg);
  int GetLogFlags() {return m_iLogFlags;};

 protected:
  // All the methods below can be rewritten to fit the special needs of a
  // true application 

  // Actual instance creation
  C_Application(const C_String& strName);

  // Application lifecycle
  virtual int OnAppInit() = 0;
  virtual int OnAppRun() = 0;
  virtual int OnAppExit() = 0;
  virtual int OnAppDestroy() = 0;

  // Default signal handler
  static void SignalHandler(int iSignal);

  // Loging endpoint local to the C_Application object
  handle m_hLog;

  // Module manager
  C_ModuleManager* m_pModuleManager;

  // Protect the application against system signals
  int InstallSigHandler();
  // Build the property table ttat will store the application config parameters
  int RetrieveConfig(int iArgc, char* paArg[]);

 private:
  // Application name
  C_String m_strName;
  // Application instance
  static C_Application* s_pApplication;
  // Application settings
  C_Settings m_cSettings;
  // Application logs
  C_Log m_cLog;

  int m_iLogFlags;

  bool m_bOnStop;

};




//------------------------------------------------------------------------------
// Logging facilities
//------------------------------------------------------------------------------
// This functions are defined as macros so that it is possible to use the global
// C_Log object of the application without providing a reference to it each time
// we need to log something. Moreover, some additonal informations are added to
// the debugging messages thanks to the compiler extensions allowed in macros.
//------------------------------------------------------------------------------

// Normal logging
#define Log(handle, level, message)                                            \
  C_Application::GetApp()->LogMsg(handle, level, message)

// Debugging logs (works only in DEBUG mode)
#ifdef DEBUG
#define LogDbg(handle, message)                                                \
  C_Application::GetApp()->LogMsg(handle, LOG_DBG, C_String("In ")+__FILE__+ \
                                  " line "+__LINE__+" -> "+message)

#else
#define LogDbg(handle, message)
#endif

#else
#error "Multiple inclusions of application.h"
#endif

