/*******************************************************************************
* buffers.h: Buffer classes definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: buffers.h,v 1.2 2003/06/30 21:37:05 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _BUFFERS_H_
#define _BUFFERS_H_


//------------------------------------------------------------------------------
// class C_Buffer
//------------------------------------------------------------------------------
// Implementation of a buffer
//------------------------------------------------------------------------------
template <class T> class C_Buffer
{
 public:
  C_Buffer(unsigned int iCapacity);
  C_Buffer(const C_Buffer<T>& cBuffer);
  ~C_Buffer();

  C_Buffer<T>& operator = (const C_Buffer<T>& cBuffer);

  inline unsigned int GetCapacity() const;
  inline unsigned int GetSize() const;
  inline T& operator[] (unsigned int iPosition) const;

  inline void SetSize(unsigned int iSize);
  
 private:
  T* m_aData;
  unsigned int m_iCapacity;
  unsigned int m_iSize;
};

#else
#error "Multiple inclusions of buffers.h"
#endif

