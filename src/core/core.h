/*******************************************************************************
* core.h: core classes definitions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: core.h,v 1.12 2004/02/16 17:07:48 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This include file provides the definitions for all the core classes and
* the system types/call used by those classes.
* It is provided for programmer convenience only
*
*******************************************************************************/


#ifndef _CORE_H_
#define _CORE_H_


//------------------------------------------------------------------------------
// System definitions
//------------------------------------------------------------------------------
#include "config.h"

#include <stdio.h>                                      // For log.h and debug.h
#include <string.h>                                               // For debug.h
#include <stdio.h>                                                // For debug.h
#include <stdlib.h>

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>                                              // For socket.h
#endif
#include <sys/types.h>                                           // For socket.h

#ifdef PTHREAD_COND_T_IN_PTHREAD_H
#include <pthread.h>                                          // For... thread.h
#include <semaphore.h>                                           // For thread.h
#endif

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>                                              // For library.h
#endif

#ifdef HAVE_OPENDIR
#include <sys/stat.h>                                              // For file.h
#include <dirent.h>                                                // For file.h
#endif

#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
#include <sys/uio.h>                                             // For socket.h
#endif

#ifdef STRUCT_IFREQ_IN_NET_IF_H
#include <sys/socket.h>                                          // For socket.h
#include <net/if.h>                                        // fot 'struct ifreq'
#elif defined WIN32
#include <winsock2.h>                                            // For socket.h
#else
#include <sys/socket.h>                                          // For socket.h
#endif

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>                                          // For common.h
#endif

#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
#include <sys/un.h>
#endif


//------------------------------------------------------------------------------
// Core classes definition
//------------------------------------------------------------------------------

// For debugging, that is activated only if DEBUG is defined
#include "debug.h"

// Definitions/Macros used everywhere
#include "common.h"

// Language extensions
#include "reflect.h"
#include "serialization.h"

// Base types
#include "string.h"
//#include "date.h"

// Containers
#include "stack.h"
//#include "heap.h"
#include "list.h"
#include "vector.h"
#include "hashtable.h"
#include "map.h"

// Buffers
#include "buffers.h"

// Exception mecanism
#include "exception.h"

// Tools
//#include "maths.h"
//#include "events.h"

// Systems ressources access
#include "system.h"
#include "thread.h"
//#include "timer.h"
#include "file.h"
#include "socket.h"
#include "stream.h"
#include "pipe.h"


// Application stuffs
#include "log.h"
#include "parsers.h"
#include "settings.h"
#include "library.h"
#include "module.h"
#include "application.h"



//------------------------------------------------------------------------------
// Core template implementation
//------------------------------------------------------------------------------
#include "stack.cpp"
#include "list.cpp"
#include "vector.cpp"
#include "hashtable.cpp"
#include "map.cpp"

#include "buffers.cpp"

#include "stream.cpp"
#include "library.cpp"


#else
#error "Multiple inclusions of core.h"
#endif

