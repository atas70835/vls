/*******************************************************************************
* list.h: Doubly linked list class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: list.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


#ifndef _LIST_H_
#define _LIST_H_


//------------------------------------------------------------------------------
// Declaration forward
//------------------------------------------------------------------------------
template <class T> class C_List;
template <class T> class C_ListIterator;


//******************************************************************************
// class C_ListNode
//******************************************************************************
// Simple class used to store the data that must be kept by the list and their
// relations with the other nodes of the list. A C_ListNode should never be
// empty, so the default constructor is not public. Howewer, it is provided as a
// protected member for C_List optimisations. 
//******************************************************************************
template <class T> class C_ListNode
{
 friend class C_List<T>;
 friend class C_ListIterator<T>;

 public:
  C_ListNode(T* pObject);
  C_ListNode(const C_ListNode<T>& cNode);
  ~C_ListNode();

 protected:
  // Default constructor must only be used by the C_List class
  C_ListNode();

  // Previous and next member of the list
  C_ListNode* pPrevious;
  C_ListNode* pNext;
  T* pData;
};




//******************************************************************************
// class C_List
//******************************************************************************
// The C_List class is basically a container used to store some data in a doubly
// linked list. It also provides some iterations methods, but an real iterator
// should be written in the future.
// The T class must provide a copy constructor if the list must be duplicated
//******************************************************************************
template <class T> class C_List
{
 friend class C_ListIterator<T>;

 public:
  C_List(byte bAutoClean = YES);
  C_List(const C_List& cList);
  ~C_List();
  
  C_List<T>& operator = (const C_List<T>& cSrc);

  void PushStart(T* pData);
  void PushEnd(T* pData);
  T* Remove(unsigned int iNode);
  void Delete(unsigned int iNode);
  int Find(const T& cItem) const;
  
  unsigned int Size() const;
  T& operator [] (unsigned int iNode) const;

  // Removal of elements is done according to the bAutoClean flag
  void Empty();
  
  C_ListIterator<T> CreateIterator() const;

 protected:
  // Dummy nodes for algorithm optimisation
  C_ListNode<T>* pFirst;
  C_ListNode<T>* pLast;
  // Number of true nodes stored in the list
  unsigned int iNodeNumber;
  // Destruction mode
  byte m_bAutoClean;
};



//******************************************************************************
// class C_ListIterator
//******************************************************************************
//
//******************************************************************************
template <class T> class C_ListIterator
{
  friend class C_List<T>;

 public:
  // Direct List traversal
  bool HasNext();
  T* GetNext();
  // Reverse list traversal
  bool HasPrevious();
  T* GetPrevious();

  // Iteration reinitialisation
  void Reset();

 private:
  // The iterator can only be constructed by the List
  C_ListIterator(const C_List<T>& cList);
  //C_ListIterator(const C_ListIterator<T>& cIterator);

  // List to browse
  const C_List<T>& m_cList;
  // Position in the data of the list
  C_ListNode<T>* m_pCurrentNode;
};



#else
#error "Multiple inclusions of list.h"
#endif

