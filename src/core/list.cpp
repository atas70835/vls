/*******************************************************************************
* list.cpp: Lists manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: list.cpp,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
* TO DO:
* Warning: This class is not thread safe
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at list.h for further explanations



//******************************************************************************
// class C_ListNode
//******************************************************************************
// Simple class used to store the data that must be kept by the list and their
// relations with the other nodes of the list. A C_ListNode should never be
// empty, so the default constructor is not public. Howewer, it is provided as a
// protected memeber for C_List optimisations. 
//******************************************************************************


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_ListNode<T>::C_ListNode(T* pObject)
{
  pData = pObject;

  ZERO(pPrevious);
  ZERO(pNext);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_ListNode<T>::C_ListNode(const C_ListNode<T>& cNode)
{
  ASSERT(false);             // Shouldn't be used, the job is done by the C_List

  /*
  if(cNode.pData)
  {
    // We cannot call the copy constructor for it wouldn't work with virtual
    // classes: We use the Clone() method instead
    pData = cNode.pData->Clone();
  }
  else
    pData = NULL;

  ZERO(pPrevious);
  ZERO(pNext);
  */ 
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> C_ListNode<T>::C_ListNode()
{
  pData = NULL;
  ZERO(pPrevious);
  ZERO(pNext);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// pData can be NULL, so test this case
//------------------------------------------------------------------------------
template <class T> C_ListNode<T>::~C_ListNode()
{
  if (pData)
  delete pData;

  ZERO(pPrevious);
  ZERO(pNext);
  ZERO(pData);
}


//******************************************************************************
// class C_List
//******************************************************************************
// The C_List class is basically a container used to store some data in a doubly
// linked list. It also provides some iterations methods, but an real iterator
// should be written in the future.
//******************************************************************************

//------------------------------------------------------------------------------
// Default constructor: create an empty list
//------------------------------------------------------------------------------
template <class T> C_List<T>::C_List(byte bAutoClean)
{
  m_bAutoClean = bAutoClean;

  // Create the dummy elements
  pFirst = new C_ListNode<T>();
  pLast = new C_ListNode<T>();

  // Init the dummy elements
  pFirst->pPrevious = NULL;
  pFirst->pNext = pLast;
  pLast->pPrevious = pFirst;
  pLast->pNext = NULL;

  // No real node is stored in the list at that time
  iNodeNumber = 0;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_List<T>::~C_List()
{
  Empty();

  // Delete the 2 dummy elements
  delete pFirst;
  delete pLast;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_List<T>::C_List(const C_List<T>& cList)
{
  // Create the dummy elements
  pFirst = new C_ListNode<T>;
  pLast = new C_ListNode<T>;

  // Init the dummy elements
  pFirst->pPrevious = NULL;
  pFirst->pNext = pLast;
  pLast->pPrevious = pFirst;
  pLast->pNext = NULL;

  // No real node is stored in the list at that time
  iNodeNumber = 0;

  m_bAutoClean = cList.m_bAutoClean;

  // Now copy the elements
  T* pData;
  for(unsigned int iIndex = 0; iIndex < cList.iNodeNumber; iIndex++)
  {
    // We cannot call the copy constructor for it wouldn't work with virtual
    // classes: We use the Clone() method
    pData = cList[iIndex].Clone();
    ASSERT(pData);
    PushEnd(pData);
  }
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_List<T>& C_List<T>::operator = (const C_List<T>& cSrc)
{
  // To avoid pbs if the dst = scr operation is tried
  if(this != &cSrc)
  {
    // Delete old list excepted the dummy first and last elements
    C_ListNode<T>* pNode = pFirst->pNext;
    while (pNode->pNext != NULL)
    {
      pNode = pNode->pNext;
      delete pNode->pPrevious;
    }

    // Reinit the dummy elements
    pFirst->pNext = pLast;
    pLast->pPrevious = pFirst;

    // No real node is stored in the list at that time
    iNodeNumber = 0;

    // Now copy the elements of the src list in the new list
    T* pData;
    for(unsigned int iIndex = 0; iIndex < cSrc.iNodeNumber; iIndex++)
    {
      // We cannot call the copy constructor for it wouldn't work with virtual
      // classes: We use the Clone() method
      pData = cSrc[iIndex].Clone();
      ASSERT(pData);
      PushEnd(pData);
    }
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_List<T>::PushStart(T* pData)
{
  C_ListNode<T>* pNewNode = new C_ListNode<T>(pData);
  C_ListNode<T>* pFirstNode = pFirst->pNext;
  pFirstNode->pPrevious = pNewNode;
  pFirst->pNext = pNewNode;

  pNewNode->pNext = pFirstNode;
  pNewNode->pPrevious = pFirst;

  iNodeNumber++;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_List<T>::PushEnd(T* pData)
{
  C_ListNode<T>* pNewNode = new C_ListNode<T>(pData);
  C_ListNode<T>* pLastNode = pLast->pPrevious;
  pLastNode->pNext = pNewNode;
  pLast->pPrevious = pNewNode;

  pNewNode->pPrevious = pLastNode;
  pNewNode->pNext = pLast;

  iNodeNumber++;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Remove the node number iNode from the list and return a pointer to it.
// As for array, the index of the first node is 0.
//------------------------------------------------------------------------------
template <class T> T* C_List<T>::Remove(unsigned int iNode)
{
  // Avoid problems if the list is empty or if the programmer is stupid
  ASSERT(iNode < iNodeNumber);

  // Find the node
  C_ListNode<T>* pNode = pFirst->pNext;
  for(unsigned int iIndex = 0; iIndex < iNode; iIndex++)
  {
    pNode = pNode->pNext;
  }
  ASSERT(pNode);

  // Extract the node from the list
  pNode->pPrevious->pNext = pNode->pNext;
  pNode->pNext->pPrevious = pNode->pPrevious;

  // Decrease the node counter
  iNodeNumber--;

  T* pItem = pNode->pData;
  pNode->pData = NULL;
  delete pNode;

  return pItem;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Remove the node number iNode from the list and delete it.
// As for array, the index of the first node is 0.
//------------------------------------------------------------------------------
template <class T> void C_List<T>::Delete(unsigned int iNode)
{
  // Remove the item from the list
  T* pItem = Remove(iNode);
  ASSERT(pItem);
  
  // Delete it
  delete pItem;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the index of the T object is the list or GEN_ERR if the object is not
// in the list
// Use the == operator of the T class as the predicate of equality
//------------------------------------------------------------------------------
template <class T> int C_List<T>::Find(const T& cItem) const
{
  C_ListNode<T>* pNode = pFirst->pNext;
  for(unsigned int iIndex = 0; iIndex < iNodeNumber; iIndex++)
  {
    if(*pNode->pData == cItem)
      return iIndex;
    else
      pNode = pNode->pNext;
  };
  
  // Node was not found
  return GEN_ERR;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> unsigned int C_List<T>::Size() const
{
  return iNodeNumber;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// As for arrays, the index of the first node is 0
//------------------------------------------------------------------------------
template <class T> T& C_List<T>::operator [] (unsigned int iNode) const
{
  // Avoid problems if the list is empty or if the programmer is lazy
  ASSERT(iNode < iNodeNumber);
  
  // Start with the first real node
  C_ListNode<T>* pNode = pFirst->pNext;
  for(unsigned int iIndex = 0; iIndex < iNode; iIndex++)
  {
    pNode = pNode->pNext;
  }

  return *(pNode->pData);
}


//------------------------------------------------------------------------------
// Removal of elements is done according to the bAutoClean flag 
//------------------------------------------------------------------------------
template <class T> void C_List<T>::Empty()
{
  switch(m_bAutoClean)
  {
    case YES:
    {
      // Go through the list of nodes and delete them
      C_ListNode<T>* pNode = pFirst->pNext;
      while (pNode->pNext != NULL)
      {
        pNode = pNode->pNext;
        delete pNode->pPrevious;
      }
      break;
    }
    case NO:
    {
      // Go through the list of nodes but just delete the C_ListNode objects
      C_ListNode<T>* pNode = pFirst->pNext;
      while (pNode->pNext != NULL)
      {
        pNode->pData = NULL;
        pNode = pNode->pNext;
        delete pNode->pPrevious;
      }
      break;
    }
    case SMART:
    {
      // Delete the items as for YES, but also detect if an item is
      // stored twice to avoid to delete it more than once
      C_ListNode<T>* pBaseNode = pFirst->pNext;
      while(pBaseNode != pLast)
      {
        C_ListNode<T>* pCurrentNode = pBaseNode->pNext;

        // Delete the first occurence of the last base item        
        delete pCurrentNode->pPrevious;

        // Remove all the copies of the base node
        while(pCurrentNode != pLast)
        {
          if(pCurrentNode->pData == pBaseNode->pData)
          {
            pCurrentNode->pData = NULL;
            pCurrentNode->pPrevious->pNext = pCurrentNode->pNext;
            pCurrentNode->pNext->pPrevious = pCurrentNode->pPrevious;
            delete pCurrentNode;
          }

          pCurrentNode = pCurrentNode->pNext;
        }

        // Delete the first occurence of the current item
        pBaseNode = pCurrentNode->pNext;
        delete pCurrentNode->pPrevious;
      }
      break;
    }
    default:
    {
      ASSERT(false);    
      break;
    }
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_ListIterator<T> C_List<T>::CreateIterator() const
{
  return C_ListIterator<T>(*this);
}




//******************************************************************************
// class C_ListIterator
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_ListIterator<T>::C_ListIterator(const C_List<T>& cList) :
                                            m_cList(cList)
{
  Reset();
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> bool C_ListIterator<T>::HasNext()
{
  ASSERT(m_pCurrentNode);
  
  return m_pCurrentNode->pNext != NULL;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T* C_ListIterator<T>::GetNext()
{
  ASSERT(m_pCurrentNode);
  T* pResult = m_pCurrentNode->pData;
  ASSERT(pResult);

  m_pCurrentNode = m_pCurrentNode->pNext;

  return pResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> bool C_ListIterator<T>::HasPrevious()
{
  ASSERT(m_pCurrentNode);
  
  return m_pCurrentNode->pPrevious != NULL;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T* C_ListIterator<T>::GetPrevious()
{
  ASSERT(m_pCurrentNode);
  T* pResult = m_pCurrentNode->pData;
  ASSERT(pResult);

  m_pCurrentNode = m_pCurrentNode->pPrevious;

  return pResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_ListIterator<T>::Reset()
{
  m_pCurrentNode = m_cList.pFirst->pNext;
  ASSERT(m_pCurrentNode);
}

