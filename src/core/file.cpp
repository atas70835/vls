/*******************************************************************************
* file.cpp: File management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This class encapsulate the ANSI C/C++ file access fonctions, and uses its
* semantic. It is therefore portable on every plateform
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <stdio.h>
#include <string.h>
#ifdef HAVE_OPENDIR
#include <dirent.h>
#endif
#include <errno.h> 
#include <sys/stat.h>

#include "common.h"
#include "debug.h"
#include "string.h"
#include "exception.h"
#include "system.h"
#include "vector.h"
#include "file.h"

#include "vector.cpp"



/*******************************************************************************
* E_File
********************************************************************************
*
*******************************************************************************/
E_File::E_File(const C_String& strMsg) : E_Exception(errno, strMsg)
{
}



/*******************************************************************************
* E_Directory
********************************************************************************
*
*******************************************************************************/
E_Directory::E_Directory(const C_String& strMsg) : E_Exception(errno, strMsg)
{
}




/*******************************************************************************
* C_File
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_File::C_File(const C_String& strPath) : m_strPath(strPath)
{
  m_hFd = NULL;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// See the fread manual for an explanation of the strMode parameter.
// Throws a FileException if the file could not be opened.
//------------------------------------------------------------------------------
void C_File::Open(const C_String& strMode, int)
{
  m_hFd = fopen(m_strPath.GetString(), strMode.GetString());

  if(!m_hFd)
  {
    throw E_File("Could not open file '" + m_strPath + "': " +
                 GetErrorMsg());
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Throws a FileException if the file could not be closed.
//------------------------------------------------------------------------------
void C_File::Close()
{
  // Close the file only if it has been sucessfully opened before
  if(m_hFd)
  {
    int iRc = fclose(m_hFd);

    if(iRc)
      throw E_File("Could not close file '" + m_strPath + "': " +
                   GetErrorMsg());
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the number of bytes read, FILE_EOF if the end of the file has been
// reached so that no data could be read.
// Throws a FileException if an error occurred.
//------------------------------------------------------------------------------
s64 C_File::Read(byte* pBuff, s64 iSize)
{
  ASSERT(m_hFd);      // Make sure that the file has been opened
  ASSERT(pBuff);
  ASSERT(iSize > 0);

  s64 iRc = fread(pBuff, sizeof(byte), iSize, m_hFd);
  if(iRc != iSize)
  {
    if(feof(m_hFd) && iRc == 0)
    {
      // No byte could be read before the end of the file
      iRc = FILE_EOF;
    }
    else if(ferror(m_hFd))
    {
      // An error occurred
      throw E_File("Could not read file '" + m_strPath + "': " +
                   GetErrorMsg());
    }
  }
  
  return iRc;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the number of byte written.
// Throws a FileException if an error occurred.
//------------------------------------------------------------------------------
s64 C_File::Write(const byte* pData, s64 iSize)
{
  ASSERT(m_hFd);      // Make sure that the file has been opened
  ASSERT(pData);
  ASSERT(iSize > 0);

  s64 iRc = fwrite(pData, sizeof(byte), iSize, m_hFd);
  if(iRc != iSize)
  {
    ASSERT(!feof(m_hFd));
    if(ferror(m_hFd))
    {
      throw E_File("Could not write file '" + m_strPath + "': " +
                   GetErrorMsg());
    }
  }
  
  return iRc;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return the position in the file.
// Throws a FileException if an error occurred.
//------------------------------------------------------------------------------
s64 C_File::GetPos()
{
  ASSERT(m_hFd);

  s64 iRc = ftell(m_hFd);
  if(iRc < 0)
  {
    throw E_File("Could not get position in file '" + m_strPath +
                 "': "+GetErrorMsg());
  }

  return iRc;  
}


s64 C_File::Size() const
{
  struct stat buf;
  ASSERT(m_hFd);

  int iRc = fstat( fileno(m_hFd), &buf);
  if (iRc<0)
  {
    throw E_File("Could not get file size of '" + m_strPath + "': " +
                 GetErrorMsg());
  }
  return (s64) (buf.st_size);
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Set the position for the file
// Throws a FileException if an error occurred.
//------------------------------------------------------------------------------
s64 C_File::Seek(s64 iOffset, s64 iStartPos)
{
  ASSERT(m_hFd);
  s64 iRc = NO_ERR;
  s64 size = Size();
  s64 pos = GetPos();

  // Check if seek request is sensible
  switch(iStartPos)
  {
    case FILE_SEEK_BEGIN:
      if (iOffset < 0)
        iRc = FILE_BOF;
      else if (iOffset > size)
        iRc = FILE_EOF;
      break;
    case FILE_SEEK_CURRENT:
      if (pos + iOffset < 0)
        iRc = FILE_BOF;
      else if (pos + iOffset > size)
        iRc = FILE_EOF;
      break;
    case FILE_SEEK_END:
      if (iOffset > -pos)
        iRc = FILE_BOF;
      else if (iOffset > 0)
        iRc = FILE_EOF;
      break;
    default:
      iRc = GEN_ERR;
      break;
  }

  if(iRc == NO_ERR)
  {
    iRc = fseek(m_hFd, iOffset, iStartPos);
    if (iRc < 0)
      throw E_File("Could not seek in file '" + m_strPath + "': " +
                 GetErrorMsg());
  }
  else if (iRc == FILE_BOF)
  {
    iRc = fseek(m_hFd, 0, FILE_SEEK_BEGIN);
    if (iRc < 0)
      throw E_File("Could not seek to start of file '" + m_strPath + "': " +
                 GetErrorMsg());
    iRc = FILE_BOF;
  }
  else if (iRc == FILE_EOF)
  {
    iRc = fseek(m_hFd, 0, FILE_SEEK_END);
    if (iRc < 0)
      throw E_File("Could not seek to end of file '" + m_strPath + "': " +
                 GetErrorMsg());
    iRc = FILE_EOF;
  }
 
  return iRc;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_File::Exists() const
{
  FILE* hFd = fopen(m_strPath.GetString(), "r");

  if(hFd)
  {
    fclose(hFd);
    return true;
  }
  else
  {
    return false;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the name (ie the path) of the opened file
//------------------------------------------------------------------------------
C_String C_File::GetName() const
{
  return m_strPath;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns some info on the file
//------------------------------------------------------------------------------
C_String C_File::GetInfo() const
{
  return m_strPath;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the file handle
//------------------------------------------------------------------------------
FILE* C_File::GetHandle() const
{
  return m_hFd;
}


/*******************************************************************************
* C_LinkedFile
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_LinkedFile::C_LinkedFile(C_Vector<C_String>& vFileNames) :
                                                m_vFileNames(vFileNames)
{
  m_iFileCount = m_vFileNames.Size();
  ASSERT(m_iFileCount)
  for(unsigned int i = 0; i < m_iFileCount; i++)
  {
    C_String strFileName = m_vFileNames[i];
    C_File* pFile = new C_File(strFileName);
    ASSERT(pFile);
    m_cFiles.Add(pFile);
  }

  m_iFileIndex = 0;
  m_pCurrentFile = &m_cFiles[0];
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_LinkedFile::Open(const C_String& strMode, int iPermissions)
{
  for(unsigned int i = 0; i < m_iFileCount; i++)
  {
    C_File* pFile = &m_cFiles[i];
    pFile->Open(strMode, iPermissions);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_LinkedFile::Close()
{
  C_String strExcept;

  for(unsigned int i = 0; i < m_iFileCount; i++)
  {
    C_File* pFile = &m_cFiles[i];
    try
    {
      pFile->Close();
    }
    catch(E_Exception e)
    {
      strExcept += e.Dump();
    }
  }

  if(strExcept != "")
    throw E_File(strExcept);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_LinkedFile::Read(byte* pBuff, s64 iSize)
{
  s64 iRc = 0;
  s64 iByteRead = 0;

  while((iByteRead != iSize) && (iRc != FILE_EOF))
  {
    iRc = m_pCurrentFile->Read(pBuff + iByteRead, iSize - iByteRead);
    if(iRc != FILE_EOF)
    {
      iByteRead += iRc;
    }
    else
    {
      if(m_iFileIndex < m_iFileCount - 1)
      {
        m_pCurrentFile = &m_cFiles[++m_iFileIndex];
        iRc = 0;
      }
    }
  }

  if((iByteRead == 0) && (iRc == FILE_EOF))
    return FILE_EOF;
  else
    return iByteRead;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_LinkedFile::GetPos()
{
  s64 iPos = 0;

  for(unsigned int i = 0; i < m_iFileIndex; i++)
  {
    C_File* pFile = &m_cFiles[i];
    pFile->Seek(0, FILE_SEEK_END);
    iPos += pFile->GetPos();
  }
  iPos += m_pCurrentFile->GetPos();

  return iPos;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_LinkedFile::Seek(s64 iOffset, s64 bStartPos)
{
  switch(bStartPos)
  {
  case FILE_SEEK_CURRENT :
    if(iOffset < 0)
    {
      s64 iSeeked = 0;
      while(iSeeked != iOffset)
      {
        s64 iSubPos = m_pCurrentFile->GetPos();
        if(iOffset - iSeeked >= -iSubPos)
        {
          m_pCurrentFile->Seek(iOffset - iSeeked, FILE_SEEK_CURRENT);
          iSeeked = iOffset;
        }
        else
        {
          m_pCurrentFile->Seek(0, FILE_SEEK_BEGIN);
          iSeeked -= iSubPos;
          if(m_iFileIndex != 0)
          {
            m_pCurrentFile = &m_cFiles[--m_iFileIndex];
            m_pCurrentFile->Seek(0, FILE_SEEK_END);
          }
          else
            throw E_File("Could not seek in linked file: out of file");
        }
      }
    }
    else if(iOffset > 0)
    {
      s64 iSeeked = 0;
      while(iSeeked != iOffset)
      {
        s64 iSubPos = m_pCurrentFile->GetPos();
        m_pCurrentFile->Seek(0, FILE_SEEK_END);
        s64 iSubPos2 = m_pCurrentFile->GetPos();
        if(iSubPos2 - iSubPos > iOffset - iSeeked)
        {
          m_pCurrentFile->Seek(iOffset - iSeeked + iSubPos - iSubPos2,
                               FILE_SEEK_END);
          iSeeked = iOffset;
        }
        else if(iSubPos2 - iSubPos == iOffset - iSeeked)
          iSeeked = iOffset;
        else
        {
          iSeeked += iSubPos2 - iSubPos;
          if(m_iFileIndex < m_iFileCount)
            m_pCurrentFile = &m_cFiles[++m_iFileIndex];
          else
            throw E_File("Could not seek in linked file: out of file");
        }
      }
    }
    break;
  case FILE_SEEK_BEGIN :
    if(iOffset >= 0)
    {
      m_iFileIndex = 0;
      m_pCurrentFile = &m_cFiles[0];
      m_pCurrentFile->Seek(0, FILE_SEEK_BEGIN);
      if(iOffset > 0)
      {
        s64 iSeeked = 0;
        while(iSeeked != iOffset)
        {
          m_pCurrentFile->Seek(0, FILE_SEEK_END);
          s64 iSubPos = m_pCurrentFile->GetPos();
          if(iSubPos > iOffset - iSeeked)
          {
            m_pCurrentFile->Seek(iOffset - iSeeked - iSubPos,
                                 FILE_SEEK_END);
            iSeeked = iOffset;
          }
          else if(iSubPos == iOffset - iSeeked)
            iSeeked = iOffset;
          else
          {
            iSeeked += iSubPos;
            if(m_iFileIndex < m_iFileCount)
              m_pCurrentFile = &m_cFiles[++m_iFileIndex];
            else
              throw E_File("Could not seek in linked file: out of file");
          }
        }
      }
      for(unsigned int i = m_iFileIndex + 1; i < m_iFileCount; i++)
        m_cFiles[i].Seek(0, FILE_SEEK_BEGIN);
    }
    else
      throw E_File("Could not seek in linked file: offset out of file");
    break;
  case FILE_SEEK_END :
    if(iOffset <=0)
    {
      m_iFileIndex = m_iFileCount - 1;
      m_pCurrentFile = &m_cFiles[m_iFileIndex];
      m_pCurrentFile->Seek(0, FILE_SEEK_END);
      if(iOffset < 0)
      {
        s64 iSeeked = 0;
        while(iSeeked != iOffset)
        {
          s64 iSubPos = m_pCurrentFile->GetPos();
          if(iOffset - iSeeked >= -iSubPos)
          {
            m_pCurrentFile->Seek(iOffset - iSeeked, FILE_SEEK_END);
            iSeeked = iOffset;
          }
          else
          {
            m_pCurrentFile->Seek(0, FILE_SEEK_BEGIN);
            iSeeked -= iSubPos;
            if(m_iFileIndex != 0)
            {
              m_pCurrentFile = &m_cFiles[--m_iFileIndex];
              m_pCurrentFile->Seek(0, FILE_SEEK_END);
            }
            else
              throw E_File("Could not seek in linked file: out of file");
          }
        }
      }
      for(int i = m_iFileIndex - 1; i >= 0; i--)
        m_cFiles[i].Seek(0, FILE_SEEK_END);
    }
    else
      throw E_File("Could not seek in linked file: offset out of file");
    break;
  default :
    ASSERT(false);
  }
  return 0;
}


/*******************************************************************************
* C_Directory
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Directory::C_Directory(const C_String& strPath)
{
  m_strPath = strPath;

#ifdef HAVE_OPENDIR
  ZERO(m_pDir);

#else
  ASSERT(false);
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Directory::Open()
{
#ifdef HAVE_OPENDIR
  m_pDir = opendir(m_strPath.GetString());
  if(!m_pDir)
  {
    throw E_Directory("Could not open directory '" +
                      m_strPath + "': " + GetErrorMsg());
  }
  else
  {
    // Read the directory content
    Update();
  }
  
#else
  ASSERT(false);
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Directory::Close()
{
#ifdef HAVE_OPENDIR
  if(m_pDir)
  {
    int iRc = closedir(m_pDir);
    if(iRc)
      throw E_Directory("Could not close directory '" +
                        m_strPath + "': " + GetErrorMsg());
  }
  
#else
  ASSERT(false);
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// To do: update only if necessary by checking the modification date of the 
// file '.'
// Return UPDATED if the directory content as been modified, NO_CHANGE if not
//------------------------------------------------------------------------------
int C_Directory::Update()
{
  ASSERT(m_pDir);

  // First empty old vectors
  m_vFiles.Empty();
  m_vSubDirs.Empty();

#ifdef HAVE_OPENDIR
  // Resets the position of the directory stream to the beginning
  rewinddir(m_pDir);

  // Read the directory content
  struct dirent* pFile = readdir(m_pDir);
  while(pFile)
  {
    C_String strFileName(pFile->d_name);
    
    // Skip the dummy '.' and '..' files
    if(strFileName != "." && strFileName != "..")
    {
      // Determines if the entry must be stored as a file or as a directory
      C_String strFilePath = m_strPath + "/" + strFileName;
      struct stat sStat;
      int iRc = stat(strFilePath.GetString(), &sStat);
      if(iRc)
      {
        // Error just means an invalid file, don't raise an exception
        // for that
      }
      else
      {
        if(sStat.st_mode & S_IFDIR)
          m_vSubDirs.Add(new C_String(strFileName));
        else
          m_vFiles.Add(new C_String(strFileName));
      }
    }
      
    pFile = readdir(m_pDir);
  }

  ASSERT(GetErrorCode() != EBADF);

#else
  ASSERT(false);
#endif

  return UPDATED;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const C_Vector<C_String>& C_Directory::GetFiles() const
{
  ASSERT(m_pDir);
  return m_vFiles;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const C_Vector<C_String>& C_Directory::GetSubDirs() const
{
  ASSERT(m_pDir);
  return m_vSubDirs;
}

