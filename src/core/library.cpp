/*******************************************************************************
* library.cpp: Dynamic library loader
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: library.cpp,v 1.6 2003/07/31 14:46:36 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at vector.h for further explanation



//******************************************************************************
// class C_Library
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg>
 C_Library<Type, Arg>::C_Library(const C_String& strName) : m_strName(strName)
{
  m_hHandle = NULL;
  ZERO(m_InstanceCreator);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg>
 C_Library<Type, Arg>::~C_Library()
{
  if(m_hHandle != NULL)
    Unload();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg> void C_Library<Type, Arg>::Load()
{
#ifdef HAVE_DLFCN_H
  // Load the library code
# ifdef RTLD_NOW
  m_hHandle = dlopen(m_strName.GetString(), RTLD_NOW);
# else
  m_hHandle = dlopen(m_strName.GetString(), DL_LAZY);
# endif
  if(m_hHandle == NULL)
  {
    throw E_Exception(GEN_ERR, "Unable to load " + m_strName + ": " +
                      GetLoaderError());
  }

  // Init the library: Init the library copies of the application global
  // variables with the value of those variables in the application
  int (*LibInitializer) (C_Application*);
  LibInitializer = (int (*)(C_Application*)) dlsym(m_hHandle, "InitLib");

  if(!LibInitializer)
  {
    throw E_Exception(GEN_ERR, "Library "+m_strName+" is invalid: "+
                      GetLoaderError());
  }
  else
  {
    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);
    int iRc = LibInitializer(pApp);
    if(iRc)
      throw E_Exception(GEN_ERR, "Unable to init library "+m_strName);
  }

  // Load the function that will create the instances of the library class 
  m_InstanceCreator = (Type* (*) (Arg)) dlsym(m_hHandle, "CreateInstance"); 
  if(!m_InstanceCreator)
  {
    throw E_Exception(GEN_ERR, "Library "+m_strName+" is invalid: "+
                      GetLoaderError());
  }

#elif defined WIN32
  ASSERT(false);
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg> void C_Library<Type, Arg>::Unload()
{
  ASSERT(m_hHandle);    // Check that the lib has been sucessfully loaded before

#ifdef HAVE_DLFCN_H
  int iRc = dlclose(m_hHandle);
  if(iRc)
  {
    throw E_Exception(GEN_ERR, "Unable to close library "+m_strName+": "+
                      GetLoaderError());
  }

#elif defined WIN32
  ASSERT(false);
#endif

  m_hHandle = NULL;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg> 
 Type* C_Library<Type, Arg>::CreateInstance(const Arg& cArg)
{
  ASSERT(m_hHandle);
  ASSERT(m_InstanceCreator);
  
  Type* pNewClass = m_InstanceCreator(cArg);
  return pNewClass;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Type, class Arg>
 C_String C_Library<Type, Arg>::GetLoaderError()
{
  C_String strError;

#ifdef HAVE_DLFCN_H
  char* pszError = (char*)dlerror();
  if(pszError)
    strError = pszError;
  else
    strError = "No error";

#elif defined WIN32
  ASSERT(false);
#endif
  
  return strError;
}


