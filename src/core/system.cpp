/*******************************************************************************
* system.cpp: System base functions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#ifdef HAVE_STRERROR
#include <errno.h>
#include <string.h>
#endif
#include <time.h>
#ifdef HAVE_SLEEP
#include <unistd.h>
#endif
#if defined WIN32
#include <windows.h>
#endif

#include <stdio.h>

#include "common.h"
#include "string.h"
#include "system.h"

#include "debug.h"


//------------------------------------------------------------------------------
// GetErrorCode
//------------------------------------------------------------------------------
// Return the error code corresponding to the last error that occured
// This code may be invalid if the system doe not update it when it returns an
// error, so check the manual before using the method
//------------------------------------------------------------------------------
int GetErrorCode(int iSubSystem/* = SYSTEM_DEFAULT*/)
{
  (void)iSubSystem;
#ifdef _WIN32
  switch(iSubSystem)
  {
    case SYSTEM_DEFAULT:
      return GetLastError();
    case SYSTEM_NETWORK:
      return WSAGetLastError();
    default:
      ASSERT(false);
      return GEN_ERR;
  }
#elif defined HAVE_STRERROR
  ASSERT(iSubSystem != SYSTEM_LIBRARY);
  return errno;
#endif
}



//------------------------------------------------------------------------------
// GetErrorMsg
//------------------------------------------------------------------------------
// Return the error message corresponding to the last error that occured
// This message may be invalid if the system doe not update it when it returns
// an error, so check the manual before using the method
//------------------------------------------------------------------------------
C_String GetErrorMsg(int iSubSystem/* = SYSTEM_DEFAULT*/)
{
#ifdef _WIN32
  // Get the system error code
  DWORD dwErrorCode = GetErrorCode(iSubSystem);

  // Get the associated string message
  LPSTR lpBuff;
  DWORD dwRes = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                              FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwErrorCode,
                              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                              (LPSTR)&lpBuff, 0, NULL);

  if(!dwRes)
  {
    // The system was unable to find the error message
    lpBuff = (LPSTR)LocalAlloc(LMEM_FIXED, 60);
    dwRes = GetLastError();
    sprintf(lpBuff, "Unable to get the message for error 0x%lx (error 0x%lx)",
            (long int)dwErrorCode, (long int)dwRes);
  }

  C_String strMessage(lpBuff);
  // To do: trim the string (\n, \r and .)
  LocalFree(lpBuff);
  return strMessage;
#elif defined HAVE_STRERROR
  return C_String(strerror(GetErrorCode(iSubSystem)));
#endif
}


//------------------------------------------------------------------------------
// Pause
//------------------------------------------------------------------------------
// Sleep for the specified number of seconds. Only the thread in which the call
// is made is stopped
//------------------------------------------------------------------------------
void Pause(unsigned int iSeconds)
{
#if defined HAVE_NANOSLEEP
  struct timespec ts = { iSeconds, 0 };
  nanosleep(&ts, NULL);

#elif defined HAVE_SLEEP
  sleep(iSeconds);
  
#elif defined WIN32
  Sleep(iSeconds);

#endif
}

