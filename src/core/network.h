/*******************************************************************************
* network.h:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: network.h,v 1.2 2002/03/04 01:55:49 bozo Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _NETWORK_H_
#define _NETWORK_H_


//------------------------------------------------------------------------------
// C_ConnectionsHandler class
//------------------------------------------------------------------------------
template <class Session> class C_ConnectionsHandler
{
 public:
  C_ConnectionsHandler(handle hLog, void* pParam);
  virtual ~C_ConnectionsHandler();
  
  void Init(int iDomain, const C_String& strAddr, const C_String& strPort);
  void Run();
  void Stop();
  void Destroy();

 protected:
  // Client connections management
  void EstablishConnection(C_Socket* pServerSocket);
  void ProcessConnection(C_Socket* pClientSocket);
  void CloseConnection(C_Socket* pClientSocket);

  // Active sessions
  C_HashTable<handle, Session> m_cSessions;

 private:
  // Helpers
  handle m_hLog;
  void* m_pParam;

  // Connections
  C_Socket* m_pServerSocket;
  C_SocketPool m_cActiveSockets;

  // Status
  bool m_bContinue;
};



#else
#error "Multiple inclusions of network.h"
#endif

