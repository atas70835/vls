/*******************************************************************************
* exception.h: Exception base class
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: exception.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_


//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
class E_Exception;



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_ExceptionString : public C_String
{
  // to write
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class E_Exception
{
 public:
  // Base constructor, used to build a stand alone exception
  E_Exception(int iCode, const C_String& strMsg);

  // Extended constructor, used to build nested exceptions
  E_Exception(int iCode, const C_String& strMsg, const E_Exception& eException);

  // Copy constructor
  E_Exception(const E_Exception& eException);

  // Destructor
  ~E_Exception();

  // Build a string that dumps the exception and its nested exceptions
  C_String Dump() const;

  // Return the code and the message of the exception
  int GetCode() const
  { return m_iCode; }
  C_String GetMsg() const
  { return m_strMsg; }

  // Return the type of the exception
  C_String GetType() const;

  // Returns the last nested exception if any
  E_Exception* GetPrevious() const
  { return m_pNestedException; }

 protected:
  // Dynamic memory is actually taken on a static heap to let the exception
  // work even when there is no more virtual memory (to write)
  //void* operator new(unsigned int iSize);
  //void operator delete(void* pException, unsigned int iSize);

  // Used by the constructors/destructors to optimise the management
  // of nested exceptions
  unsigned int GetRefCount() const
  { return m_iRefCount; }
  void IncreaseRefCount();
  void DecreaseRefCount();

 private:
  // Exception info
  const int m_iCode;
  const C_String m_strMsg;

  // First of the nested exceptions
  E_Exception* m_pNestedException;

  // Heap allowing 100 exceptions to be chained (to write)
  //static byte m_abHeap[100*sizeof(E_Exception)];

  // Reference counter
  unsigned int m_iRefCount;
};




#else
#error "Multiple inclusions of exception.h"
#endif

