/*******************************************************************************
* exception.cpp: Exception class
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: exception.cpp,v 1.3 2003/10/27 10:58:10 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <stdlib.h>
#include <stdio.h>

//#ifndef SOLARIS
//#include <typeinfo>
//#else
//#include <typeinfo.h>
//#endif

#include "common.h"
#include "string.h"

#include "debug.h"
#include "exception.h"



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Exception::E_Exception(int iCode, const C_String& strMsg) :
                                                m_iCode(iCode), m_strMsg(strMsg)
{
#ifdef DEBUG
  printf("*** Exception *** in constructor 1 (%p)\n", this);
#endif
  // No nested exception
  m_pNestedException = NULL;

  m_iRefCount = 0;
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Exception::E_Exception(int iCode, const C_String& strMsg,
                         const E_Exception& eException) :
                                                m_iCode(iCode), m_strMsg(strMsg)
{
#ifdef DEBUG
  printf("*** Exception *** in constructor 2 (%p)\n", this);
#endif
  // Copy the nested exception
  m_pNestedException = new E_Exception(eException);
  m_pNestedException->IncreaseRefCount();

  m_iRefCount = 0;
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Exception::E_Exception(const E_Exception& eException) :
                      m_iCode(eException.m_iCode), m_strMsg(eException.m_strMsg)
{
#ifdef DEBUG
  printf("*** Exception *** in copy constructor (%p, copy of %p)\n", this,
         &eException);
#endif
  // Also copy the nested exception if any
  if(eException.m_pNestedException)
  {
    // Smart copy, using the reference counter
    m_pNestedException = eException.m_pNestedException;
    m_pNestedException->IncreaseRefCount();
  }
  else
    m_pNestedException = NULL;

  m_iRefCount = 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
E_Exception::~E_Exception()
{
#ifdef DEBUG
  printf("*** Exception *** in destructor (%p)\n", this);
#endif

// if(GetRefCount() > 0)
// {
//   // Just remove the reference to this instance
//   DecreaseRefCount();
// }
// else
// {
//   // Delete the instance
   if(m_pNestedException)
   {
     if(m_pNestedException->GetRefCount() > 0)
        m_pNestedException->DecreaseRefCount();
     else
       delete m_pNestedException;
   } 
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String E_Exception::Dump() const
{
  // First dump the current exception
  C_String strDump = GetType() + ": ";
  strDump += GetMsg();

  // Now dump the nested exceptions if any
  E_Exception* pException = GetPrevious();
  while(pException)
  {
    strDump += "\n";
    strDump += pException->GetType() + ": ";
    strDump += pException->GetMsg();

    pException = pException->GetPrevious();
  }
  
  return strDump;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Return the type of the exception
// Call the RTTI typeid operator to get the name of the class. We therefore
// can get the name of the most derived subclass of E_Exception
//-----------------------------------------------------------------------------
C_String E_Exception::GetType() const
{
  C_String strType;
  
/*  if(typeid(*this) == typeid(E_Exception))
  {
    strType = "Global exception";
  }
  else
  {
    // Get the name of the class
    strType = typeid(*this).name();

    // Strip the 'E_' prefix
    unsigned int iStrLen = strType.Length();
    ASSERT(iStrLen > 2);
    strType = strType.SubString(2, iStrLen);

    // Add the 'exception' qualifier
    strType += " exception";
  }
*/
  strType = "Error";

  return strType;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void E_Exception::IncreaseRefCount()
{
  m_iRefCount++; 

  if(m_pNestedException)
    m_pNestedException->IncreaseRefCount();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void E_Exception::DecreaseRefCount()
{
  ASSERT(m_iRefCount > 0);
  m_iRefCount--; 

  if(m_pNestedException)
    m_pNestedException->DecreaseRefCount();
}

