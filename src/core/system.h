/*******************************************************************************
* system.h: System functions definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: system.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _SYSTEM_H_
#define _SYSTEM_H_


//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

// Common subsystems 
#define SYSTEM_DEFAULT          0
#define SYSTEM_NETWORK          1
#define SYSTEM_LIBRARY          2



//------------------------------------------------------------------------------
// GetErrorCode
//------------------------------------------------------------------------------
// Return the error code corresponding to the last error that occured in the
// the given subsystem
//------------------------------------------------------------------------------
int GetErrorCode(int iSubSystem = SYSTEM_DEFAULT);



//------------------------------------------------------------------------------
// GetErrorMsg
//------------------------------------------------------------------------------
// Return the error message corresponding to the last error that occured in the
// the given subsystem
//------------------------------------------------------------------------------
C_String GetErrorMsg(int iSubSystem = SYSTEM_DEFAULT);



//------------------------------------------------------------------------------
// Pause
//------------------------------------------------------------------------------
// Sleep for the specified number of seconds. Only the thread in which the call
// is made is stopped
//------------------------------------------------------------------------------
void Pause(unsigned int iSeconds);


#else
#error "Multiple inclusions of system.h"
#endif

