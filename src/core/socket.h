/*******************************************************************************
* socket.h: Socket class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _SOCKET_H_
#define _SOCKET_H_

/* Fix broken libc */
#ifndef HAVE_SOCKLEN_T
#define socklen_t int
#endif

//------------------------------------------------------------------------------
// Declaration forward
//------------------------------------------------------------------------------
class C_SocketPool;
class C_Socket;


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class E_InetAddr : public E_Exception
{
 public:
  E_InetAddr(const C_String& strMsg);
  E_InetAddr(const C_String& strMsg, const E_InetAddr& e);
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class E_Socket : public E_Exception
{
 public:
  E_Socket(const C_String& strMsg);
  E_Socket(const C_String& strMsg, const E_Socket& e);
  E_Socket(const C_String& strMsg, const E_InetAddr& e);
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_SocketBuff
{
  friend class C_Socket;
  
 public:
  C_SocketBuff(unsigned int iSlotCount);
  ~C_SocketBuff();
  
  void AssignSlot(unsigned int iSlot, char* pBuff, unsigned int iBuffLen);
  void SetSlotBuff(unsigned int iSlot, char* pBuff);
  void SetSlotSize(unsigned int iSlot, unsigned int iBuffLen);

  unsigned int GetSlotCount() const
  { return m_iSlotCount; }

   protected:
#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  typedef struct iovec  sock_buff;
#elif defined WIN32
  typedef WSABUF        sock_buff;
#else
#error "No type available for sock_buff."
#endif

  sock_buff* GetRawBuffers() const
  { return m_pBuffers; }
   
 private:
  unsigned int m_iSlotCount;
  sock_buff* m_pBuffers;
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Inet4Addr
{
 public:
  void Build(const C_String& strHost, const C_String& strPort);

  const struct sockaddr_in* GetInetAddr() const
  {
    return &m_sInetAddr;
  };

 private:
  struct sockaddr_in m_sInetAddr;
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
#ifdef HAVE_IPV6
class C_Inet6Addr
{
 public:
  void Build(const C_String& strHost, const C_String& strPort,
             int iType);

  const struct sockaddr_in6* GetInetAddr() const
  {
    return &m_sInetAddr;
  };

 private:
  struct sockaddr_in6 m_sInetAddr;
};
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
class C_UnixAddr
{
 public:
  void Build(const C_String& strPath);
  void Build(C_String& strPath, C_String&, int)
       { Build(strPath); };  // ignore port
  const struct sockaddr_un* GetUnixAddr() const
  {
    return &m_sUnixAddr;
  };

  // Remove the file if it already exists
  // Otherwise, there may be bind() error
  int Unlink()
  {  return unlink(m_sUnixAddr.sun_path); };

 private:
  struct sockaddr_un m_sUnixAddr;
};
#endif


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Socket
{
  friend class C_SocketPool;
  
 public:
  // Constructor
  C_Socket();

  // Open / Close the socket
  void Open(int iDomain, int iType, int iProtocol = 0);
  void Close();

  // Return the Address Family (iDomain) of the socket
  int GetDomain();
  
  // Modify the socket default options
  void SetOption(int iLevel, int iOption, const char* pOptVal, socklen_t iOptLen);
  void GetOption(int iLevel, int iOption, char* pOptVal, socklen_t* pOptLen);

  // Bind the 2 endpoints of the socket to the given addreses
  void Bind(const C_String& strIntf, const C_String& strPort);
  void Connect(const C_String& strIntf, const C_String& strPort);

  // Server
  void Listen(int iBackLog);
  C_Socket* Accept();

  // Send / Receive on this socket without timeout
  int Write(const char* pBuff, int iBuffLen, int iFlags = 0);
  int WriteTo(C_Inet4Addr& cPeerAddr,
              const char* pBuff, int iBuffLen, int iFlags = 0);
#ifdef HAVE_IPV6
  int WriteTo(C_Inet6Addr& cPeerAddr,
              const char* pBuff, int iBuffLen, int iFlags = 0);
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  int WriteTo(C_UnixAddr& cPeerAddr,
              const char* pBuff, int iBuffLen, int iFlags = 0);
#endif

  int Read(char* pBuff, int iBuffLen, int iFlags = 0);

  // Receive with timeout
  int ReadTimeOut(char* pBuff, int iBuffLen, int iTimeOut, int iFlags = 0);

  // Send / Receive into pool of buffers
  int Send(const C_SocketBuff& cBuffers);
  int Send(const C_SocketBuff& cBuffers, unsigned int iBuffCount,unsigned int iOffset);
  int SendTo(C_Inet4Addr& cPeerAddr, const C_SocketBuff& cBuffers);
  int SendTo(C_Inet4Addr& cPeerAddr,
             const C_SocketBuff& cBuffers, unsigned int iBuffCount);
  int Recv(C_SocketBuff& cBuffers);
  int Recv(C_SocketBuff& cBuffers, unsigned int iBuffCount);

  // Get informations on the connection
  C_String GetLocalName() const;
  C_String GetLocalPort() const;
  C_String GetPeerName() const;
  C_String GetPeerPort() const;

  // Get the 'name' of the connection
  C_String GetName() const;
  C_String GetInfo() const;

  bool operator == (const C_Socket& cArg);

 private:
  // Private data
#ifdef WIN32
typedef SOCKET          sock_handle;
#else
typedef int             sock_handle;
#endif

  int m_iDomain;
  int m_iType;

  sock_handle m_hSocket;
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_SocketPool
{
 public:
  C_SocketPool();
  ~C_SocketPool();
  
  // Add a socket to watch to detect the arrival of data
  void AddRecv(C_Socket* pSocket);
  // Add a socket to watch to detect the possibility of an immediate send
  void AddSend(C_Socket* pSocket);
  // Add a socket to watch to detect possible exceptions
  void AddExcept(C_Socket* pSocket);

  // Remove one of those sckets from the corresponding sets
  C_Socket* RemoveRecv(const C_Socket& cSocket);
  C_Socket* RemoveSend(const C_Socket& cSocket);
  C_Socket* RemoveExcept(const C_Socket& cSocket);
  
  // Wait until something interessting happen 
  C_Socket* Monitor();
  // Same as before but with a timeout -> NULL can be returned
  C_Socket* Monitor(u32 iTimeout);
  
 private:
  C_Socket* InternalMonitor(struct timeval* pTimeOut);
  
  // Lists of sockets to monitor
  C_Vector<C_Socket> m_vRecvSockets;
  C_Vector<C_Socket> m_vSendSockets;
  C_Vector<C_Socket> m_vExceptSockets;

  // List of sockets that have interessting pending events
  C_Vector<C_Socket> m_vWaitingSockets;
};



#else
#error "Multiple inclusions of socket.h"
#endif

