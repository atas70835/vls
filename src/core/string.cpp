/*******************************************************************************
* string.cpp: Strings manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* TO DO: a reference counter to implement the copy on write scheme
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "string.h"

#include "debug.h"



//******************************************************************************
// class C_String
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Default constructor
//------------------------------------------------------------------------------
// The string is initialised to '\0' in order to have a homogeneous behaviour 
// even if the string is empty, so that all if(!m_pszBuff) can be avoided
//------------------------------------------------------------------------------
C_String::C_String()
{
  m_iLength = 0;
  m_pszBuff = new char[1];
  m_pszBuff[0] = '\0';
}


//------------------------------------------------------------------------------
// Simple constructor
//------------------------------------------------------------------------------
// Memory is dynamicly allocated but no test is made to detect memory leaks, so
// if the system run short of memory, a segfault will probably happen
//------------------------------------------------------------------------------
C_String::C_String(const char* pszSrc)
{
  ASSERT(pszSrc);
  
  m_iLength = strlen(pszSrc);
  m_pszBuff = new char[m_iLength + 1];
  memcpy(m_pszBuff, pszSrc, m_iLength + 1);
}


//------------------------------------------------------------------------------
// Simple constructor
//------------------------------------------------------------------------------
// Memory is dynamicly allocated but no test is made to detect memory leaks, so
// if the system run short of memory, a segfault will probably happen
//------------------------------------------------------------------------------
C_String::C_String(char cSrc)
{
  m_iLength = 1;
  m_pszBuff = new char[2];
  m_pszBuff[0] = cSrc;
  m_pszBuff[1] = '\0';
}


//------------------------------------------------------------------------------
// Simple constructor
//------------------------------------------------------------------------------
// Memory is dynamicly allocated but no test is made to detect memory leaks,
// so if the system run short of memory, a segfault will probably happen
//------------------------------------------------------------------------------
C_String::C_String(s32 iSrc)
{
  // int is at most 4 giga on a 32 bits computers, so 12 digits are enough
  ASSERT(sizeof(int) <= 4);
  char pszNumber[12];
  ASSERT(sprintf(pszNumber, "%d", iSrc));
  sprintf(pszNumber, "%d", iSrc);

  // Stores the value of iSrc in the buffer of the string
  m_iLength = strlen(pszNumber);
  m_pszBuff = new char[m_iLength + 1];
  memcpy(m_pszBuff, pszNumber, m_iLength+1);
}


//------------------------------------------------------------------------------
// Simple constructor
//------------------------------------------------------------------------------
// Memory is dynamicly allocated but no test is made to detect memory leaks,
// so if the system run short of memory, a segfault will probably happen
//------------------------------------------------------------------------------
C_String::C_String(u32 iSrc)
{
  // int is at most 4 giga on a 32 bits computers, so 12 digits are enough
  ASSERT(sizeof(int) <= 4);
  char pszNumber[12];
  ASSERT(sprintf(pszNumber, "%d", iSrc));
  sprintf(pszNumber, "%d", iSrc);

  // Stores the value of iSrc in the buffer of the string
  m_iLength = strlen(pszNumber);
  m_pszBuff = new char[m_iLength + 1];
  memcpy(m_pszBuff, pszNumber, m_iLength+1);
}


//------------------------------------------------------------------------------
// Copy constructor
//------------------------------------------------------------------------------
C_String::C_String(const C_String& strSrc)
{
  m_iLength = strSrc.m_iLength;
  m_pszBuff = new char[m_iLength+1];
  memcpy(m_pszBuff, strSrc.m_pszBuff, m_iLength+1);
}


//------------------------------------------------------------------------------
// Internal constructor
//------------------------------------------------------------------------------
// Memory is allocated but not initialised: this is why this constructor is
// protected and reserved to internal use
//------------------------------------------------------------------------------
C_String::C_String(unsigned int iSize, bool)
{
  m_iLength = iSize;
  m_pszBuff = new char[iSize+1];
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
// Strings are never empty in our implementation: the buffer contains always at
// least a NULL char
//------------------------------------------------------------------------------
C_String::~C_String()
{
  ASSERT(m_pszBuff);                                     // Prevent internal bug
  
  delete[] m_pszBuff;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Old buffer is freed, and a new one is allocated to store the string pzsSrc
//------------------------------------------------------------------------------
C_String& C_String::operator = (const char *pszSrc)
{
  ASSERT(pszSrc);
  
  // Delete current instance
  ASSERT(m_pszBuff);
  delete[] m_pszBuff;

  // Store the new string
  m_iLength = strlen(pszSrc);
  m_pszBuff = new char[m_iLength + 1];
  memcpy(m_pszBuff, pszSrc, m_iLength+1);
  
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Old buffer is freed, and a new one is allocated to store the string strSrc
//------------------------------------------------------------------------------
C_String& C_String::operator = (const C_String& strSrc)
{
  // To avoid pbs if the str = str operation is tried
  if(*this != strSrc)
  {
    // Delete old buffer
    ASSERT(m_pszBuff);
    delete[] m_pszBuff;
  
    // Store the new string
    m_iLength = strSrc.m_iLength;
    m_pszBuff = new char[m_iLength + 1];
    memcpy(m_pszBuff, strSrc.m_pszBuff, m_iLength+1);
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Old buffer is freed, and a new one is allocated to store the integer iSrc
// which is converted to its textual representation
//------------------------------------------------------------------------------
C_String& C_String::operator = (const int iSrc)
{ 
  // This is not the most efficient way to do this, but this allow us to deal
  // only once with the int to string conversion
  *this = C_String(iSrc);

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String* C_String::Clone()
{
  return new C_String(*this);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Final NULL char is not counted
//------------------------------------------------------------------------------
unsigned int C_String::Length() const
{
  return m_iLength;
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// This operator is read only, it cannot be used to modify the string
//------------------------------------------------------------------------------
const char& C_String::operator [] (unsigned int iIndex) const
{
  // Check that index range is correct
  ASSERT(iIndex < m_iLength);

  return m_pszBuff[iIndex];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Give read only access to the raw string
//------------------------------------------------------------------------------
const char* C_String::GetString() const
{
  return m_pszBuff;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_String::operator == (const char* pszArg) const
{
  ASSERT(pszArg);
  return !strcmp(m_pszBuff, pszArg);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_String::operator == (const C_String& strArg) const
{
  return (m_pszBuff[0] == strArg.m_pszBuff[0] &&
                                   !strcmp(m_pszBuff, strArg.m_pszBuff));
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_String::operator != (const char* pszArg) const
{
  ASSERT(pszArg);
  return strcmp(m_pszBuff, pszArg);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_String::operator != (const C_String& strArg) const
{
  return strcmp(m_pszBuff, strArg.m_pszBuff);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_String::operator < (const C_String& strArg) const
{
  return (strcmp(m_pszBuff, strArg.m_pszBuff) < 0 );
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_String::operator > (const C_String& strArg) const
{
  return (strcmp(m_pszBuff, strArg.m_pszBuff) > 0 );
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_String::operator + (const char* pszToken) const
{
  ASSERT(pszToken);
  
  C_String strResult(m_pszBuff);
  strResult += pszToken;

  return strResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_String::operator + (char cChar) const
{
  C_String strResult(m_iLength+1, true);
  memcpy(strResult.m_pszBuff, m_pszBuff, m_iLength);
  strResult.m_pszBuff[m_iLength] = cChar;
  strResult.m_pszBuff[m_iLength+1] = '\0';

  return strResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_String::operator + (s32 iNumber) const
{
  // This is not the most efficient way to do this, but this allow us to deal
  // only once with the int to string conversion
  C_String strResult(m_pszBuff);
  strResult += iNumber;

  return strResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_String::operator + (u32 iNumber) const
{
  // This is not the most efficient way to do this, but this allow us to deal
  // only once with the int to string conversion
  C_String strResult(m_pszBuff);
  strResult += iNumber;

  return strResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_String::operator + (const C_String& strToken) const
{
  C_String strResult(m_pszBuff);
  strResult += strToken;

  return strResult;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String& C_String::operator += (const char *pszToken)
{
  ASSERT(pszToken);

  int iTokLen = strlen(pszToken);
  char* pszTmp = m_pszBuff;

  m_pszBuff = new char[m_iLength + iTokLen + 1];
  memcpy(m_pszBuff, pszTmp, m_iLength);
  memcpy(m_pszBuff+m_iLength, pszToken, iTokLen+1);
  m_iLength += iTokLen;

  delete[] pszTmp;
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String& C_String::operator += (char cChar)
{
  *this += C_String(cChar);
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String& C_String::operator += (s32 iNumber)
{
  *this += C_String(iNumber);
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String& C_String::operator += (u32 iNumber)
{
  *this += C_String(iNumber);
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String& C_String::operator += (const C_String& strToken)
{
  *this += strToken.m_pszBuff;
  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_String::ToLower() const
{
  C_String strResult(m_iLength, true);

  for(unsigned int i = 0; i < m_iLength; i++)
    strResult.m_pszBuff[i] = tolower(m_pszBuff[i]);

  strResult.m_pszBuff[m_iLength] = '\0';

  return strResult;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_String::ToUpper() const
{
  C_String strResult(m_iLength+1, true);

  for(unsigned int i = 0; i < m_iLength; i++)
    strResult.m_pszBuff[i] = toupper(m_pszBuff[i]);

  strResult.m_pszBuff[m_iLength] = '\0';

  return strResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_String::SubString(unsigned int iBeginning, unsigned int iEnd) const
{
  ASSERT(iEnd <= m_iLength);
  ASSERT(iEnd >= iBeginning);
  unsigned int iLen = iEnd - iBeginning;
  C_String strResult(iLen, true);
  memcpy(strResult.m_pszBuff, m_pszBuff+iBeginning, iLen);
  strResult.m_pszBuff[iLen] = '\0';
  return strResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_String::Strip(const C_String& strInvalidChars) const
{
  unsigned int iStart = 0;
  while(iStart < m_iLength)
  {
    if(strInvalidChars.Find(m_pszBuff[iStart]) >= 0)
      iStart++;
    else
      break;
  }

  unsigned int iEnd = m_iLength;
  while(iEnd > iStart)
  {
    if(strInvalidChars.Find(m_pszBuff[iEnd]) >= 0)
      iEnd--;
    else
      break;
  }

  return SubString(iStart, iEnd);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// No check is mode on the validity of the string, so overflows and underflows
// are possible
//------------------------------------------------------------------------------
int C_String::ToInt() const
{
  // Base is set to 0 so that the strtol function will try to detect it
  return strtol(m_pszBuff, NULL, 0);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the position of the first occurence of the char if any or GEN_ERR
// if not found
//------------------------------------------------------------------------------
int C_String::Find(char cChar, unsigned int iStart) const
{
  for(unsigned int iResult = iStart; iResult < m_iLength; iResult++)
  {
    if(m_pszBuff[iResult] == cChar)
      return iResult;
  }
  
  return GEN_ERR;
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Replace all occurences of the char returns number of occurences
//------------------------------------------------------------------------------
int C_String::Replace(char cChar, char cChar2, unsigned int iStart) 
{
  int count=0;
  for(unsigned int i = iStart; i < m_iLength; i++)
  {
    if(m_pszBuff[i] == cChar)
    {
      m_pszBuff[i] = cChar2;
      count++;
    }
  }
  
  return count;
}
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the position of the first occurence of the substring if any or
// GEN_ERR if not found
// Build a kind of pseudo automata for the substring lookup 
//------------------------------------------------------------------------------
int C_String::Find(const C_String& strItem, unsigned int iStart/* = 0*/) const
{
  int iRc = GEN_ERR;

  // Do those tests now to avoid doing them at each iteration below
  if(strItem.m_iLength > 0 && iStart + strItem.m_iLength < m_iLength)
  {
    const char* pCurrentChar = m_pszBuff + iStart;
    while(*pCurrentChar != '\0')
    {
      // We are in state 1: nothing has been found. Try to find the first char
      // of the item
      if(strItem.m_pszBuff[0] == pCurrentChar[0])
      {
        // We now are in state 2: remain in this state as long as we found the
        // end of the item in the string
        unsigned int i = 1;
        while(strItem.m_pszBuff[i] == pCurrentChar[i])
          i++;

        // Don't go back to state 1 if the item has not been fully matched
        if(i >= strItem.m_iLength)
        {
          iRc = pCurrentChar - m_pszBuff;
          break;
        }
      }
      // First char of the item not found: look further in the string
      pCurrentChar++;
    }
  }
  
  return iRc;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns true if and only if the string starts with the given item
//------------------------------------------------------------------------------
bool C_String::StartsWith(const C_String& strItem) const
{
  bool bResult = false;

  unsigned int iSize = strItem.Length();
  if(m_iLength >= iSize && memcmp(m_pszBuff, strItem.m_pszBuff, iSize) == 0)
    bResult = true;

  return bResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns true if and only if the string ends with the given item
//------------------------------------------------------------------------------
bool C_String::EndsWith(const C_String& strItem) const
{
  bool bResult = false;

  unsigned int iSize = strItem.Length();
  char* pszBegin = m_pszBuff + m_iLength - iSize;
  if(m_iLength >= iSize && memcmp(pszBegin, strItem.m_pszBuff, iSize) == 0)
    bResult = true;
  
  return bResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Look at http://ourworld.compuserve.com/homepages/bob_jenkins/doobs.htm
// for an explanation of the algorithm
//------------------------------------------------------------------------------
u32 C_String::GetHashCode() const
{
  // Mix the bits of 3 u32 integer
  #define Mix(a,b,c) \
  { \
    a -= b; a -= c; a ^= (c>>13); \
    b -= c; b -= a; b ^= (a<<8);  \
    c -= a; c -= b; c ^= (b>>13); \
    a -= b; a -= c; a ^= (c>>12); \
    b -= c; b -= a; b ^= (a<<16); \
    c -= a; c -= b; c ^= (b>>5);  \
    a -= b; a -= c; a ^= (c>>3);  \
    b -= c; b -= a; b ^= (a<<10); \
    c -= a; c -= b; c ^= (b>>15); \
  }

  // Set up the internal state
  u32 uiLen = m_iLength;
  u32 uiToken1 = 0x9e3779b9;    // the golden ratio; an arbitrary value
  u32 uiToken2 = uiToken1;
  u32 uiToken3 = 0;             // Just to have a != value
  char* pszIter = m_pszBuff;

  // Handle most of the key
  while (uiLen >= 12)
  {
    uiToken1 += (pszIter[0] +((u32)pszIter[1]<<8) +
                 ((u32)pszIter[2]<<16) + ((u32)pszIter[3]<<24));

    uiToken2 += (pszIter[4] +((u32)pszIter[5]<<8) +
                 ((u32)pszIter[6]<<16) + ((u32)pszIter[7]<<24));

    uiToken3 += (pszIter[8] +((u32)pszIter[9]<<8) +
                 ((u32)pszIter[10]<<16) + ((u32)pszIter[11]<<24));

    Mix(uiToken1, uiToken2, uiToken3);
    pszIter += 12;
    uiLen -= 12;
  }

  // Handle the last 11 bytes
  uiToken3 += uiLen;

  if(uiLen >= 11)
    uiToken3 += ((u32)pszIter[10]<<24);
  if(uiLen >= 10) 
    uiToken3 += ((u32)pszIter[9]<<16);
  if(uiLen >= 9) 
    uiToken3 += ((u32)pszIter[8]<<8);
  // The first byte of c is reserved for the length

  if(uiLen >= 8) 
    uiToken2 += ((u32)pszIter[7]<<24);
  if(uiLen >= 7) 
    uiToken2 += ((u32)pszIter[6]<<16);
  if(uiLen >= 6) 
    uiToken2 += ((u32)pszIter[5]<<8);
  if(uiLen >= 5) 
    uiToken2 += ((u32)pszIter[4]);

  if(uiLen >= 4) 
    uiToken1 += ((u32)pszIter[3]<<24);
  if(uiLen >= 3) 
    uiToken1 += ((u32)pszIter[2]<<16);
  if(uiLen >= 2) 
    uiToken1 += ((u32)pszIter[1]<<8);
  if(uiLen >= 1) 
    uiToken1 += ((u32)pszIter[0]);
  // Case 0: nothing left to add

   Mix(uiToken1, uiToken2, uiToken3);
  
  return uiToken3;
}





//******************************************************************************
// Additional external operators
//******************************************************************************
// Solve various problems when dealing with traditional strings (char*)
// such as this one: "my traditional string" + C_String
// To be extended 
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String operator + (const char* pszLeft, const C_String& strRight)
{
  C_String strResult(pszLeft);
  strResult += strRight;
  return strResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String operator + (const int iLeft, const C_String& strRight)
{
  C_String strResult;
  strResult += iLeft;
  strResult += strRight;
  return strResult;
}



//******************************************************************************
// class C_StringTokenizer
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_StringTokenizer::C_StringTokenizer(const C_String& strSource, char cSep):
                           m_strSource(strSource)
{
  m_cSeparator = cSep;
  m_iPosition = 0;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_StringTokenizer::SetSeparator(char cSep)
{
  m_cSeparator = cSep;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_StringTokenizer::HasMoreToken()
{
  unsigned int iStrLen = m_strSource.Length();

  // Skip the initial separators if any
  while((m_iPosition < iStrLen) && (m_strSource[m_iPosition] == m_cSeparator))
    m_iPosition++;
  
  // There is a token only if we stopped at least one char before the end
  // of the string. The test is made on m_iPosition+1 and not on iStrLen-1
  // because iStrLen is unsigned
  return m_iPosition < iStrLen;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_String C_StringTokenizer::NextToken()
{
  unsigned int iStrLen = m_strSource.Length();

  // Skip the initial separators if any
  while((m_iPosition < iStrLen) && (m_strSource[m_iPosition] == m_cSeparator))
    m_iPosition++;

  unsigned int iStart = m_iPosition;

  ASSERT(m_iPosition < iStrLen);

  while((m_iPosition < iStrLen) && (m_strSource[m_iPosition] != m_cSeparator))
  {
    m_iPosition++;
  }

  return m_strSource.SubString(iStart, m_iPosition);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_StringTokenizer::Reset()
{
  m_iPosition = 0;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
unsigned int C_StringTokenizer::CountTokens() const
{
  unsigned int iResult = 1;

  unsigned int iStrLen = m_strSource.Length();
  for(unsigned int i = m_iPosition; i < iStrLen; i++)
  {
    if(m_strSource[i] == m_cSeparator)
      iResult++;
  }

  return iResult;
}

