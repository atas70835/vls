/*******************************************************************************
* lexer.lex: Lexical analyzer for configuration files
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: lexer.lex,v 1.3 2003/12/12 11:43:34 adq Exp $
*
* Authors: Cyril Deguet <asmax@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
* To build lexer.cpp, run "make lexer"
* 
*******************************************************************************/

%{
  #include "core/lexer.h"
%}

SPACE   [ \t]
VAR     [^ \t\"=#\n]+
VALUE   \"[^\"\n]*\"
COMMENT #.*

%%

BEGIN     return TOK_BEGIN;
END       return TOK_END;
{VAR}     return TOK_VAR;
=         return TOK_EQUAL;
{VALUE}   return TOK_VALUE;
{COMMENT}
{SPACE}   
\n        return TOK_NEWLINE;

