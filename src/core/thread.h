/*******************************************************************************
* thread.h: Thread classes definition
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: thread.h,v 1.5 2003/07/02 15:05:28 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _THREAD_H_
#define _THREAD_H_

#ifdef _WIN32
typedef unsigned (__stdcall *PTHREAD_START) (void *);
#endif

//------------------------------------------------------------------------------
// Thread attributes
//------------------------------------------------------------------------------

// Not yet implemented: TO DO if options others than default ones of
// pthread_create are needed
#define THREAD_SCHED_REGULAR    4
#define THREAD_SCHED_REALTIME   8


//------------------------------------------------------------------------------
// Thread status
//------------------------------------------------------------------------------
#define THREAD_STATUS_NOTSTARTED    0
#define THREAD_STATUS_STARTED       1
#define THREAD_STATUS_STOPPED       2
#define THREAD_STATUS_INTERRUPTED   3


//------------------------------------------------------------------------------
// C_Thread is a wrapper on top of the notion of thread. The C_Thread class
// itself is guarantied to exist as long as the thread is running, so that you
// can safely access any data stored in a subclass of C_Thread from the XYZWork
// methods
// C_Thread are always created joinable, which means that you must either call
// the Cancel or Stop method to avoid a memory leak.
// C_Thread also provides some mecanism to handle exceptions that occur within a
// thread. If you don't catch an exception within the DoWork method, this
// exception will terminate the thread and be available in the Cancel/Stop
// methods
//------------------------------------------------------------------------------
class C_Thread
{
 public:
  C_Thread(unsigned int iProperties = 0);
  virtual ~C_Thread();

  // Init and then start the thread
  void Create();
  // Interrupt the thread and clean all the ressouce it used 
  void Cancel();
  // Ask the thread to die and clean all the ressouce it used
  // Should be preferred to Cancel unless you know what you do
  void Stop();

  // Interrupt the normal processing of a thread unless it has been created
  // ininterruptible (but this is not yet implemented :)
  void Interrupt();
  // Wait during a fixed delay before interrupting the thread if needed
  void DelayedInterruption();

  // Thread comparison
  bool operator == (const C_Thread& cThread);

  // Return the identifier of the calling thread
  static C_Thread* Self();

  // Thread status (only the 1st one is implemented yet)
  bool IsRunning() const;
  bool IsCanceled() const;
  bool IsInterrupted() const;

 protected:
  // Called at startup to do user initialisations
  virtual void InitWork() = 0;
  // Called when the thread is launched to do interessting job
  virtual void DoWork() = 0;
  // Called when the thread must be shutdowned to stop normal processing
  virtual void StopWork() = 0;
  // Called after shutdown to do user cleanings
  virtual void CleanWork() = 0;

 private:
  // Internal method used to allow pthread "extern C" functions to call internal
  // C++ methods
  static void* StartRoutine(void* pThread);
  static C_List<C_Thread> s_cThreadList;

  // Thread properties
  unsigned int m_iFlags;

  // Status
  int m_iStatus;

  // Thread identity
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  pthread_t tId;
#elif defined WIN32
  HANDLE hThread;
  WORD dwId;
#endif
};



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Mutex
{
 public:
  C_Mutex();
  ~C_Mutex();

  // Lock the mutex
  void Lock();
  // Lock the mutex unless it has already been locked
  int TryLock();
  // Release it
  void UnLock();
  
 private:
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  pthread_mutex_t m_sMutex;
#elif defined WIN32
  CRITICAL_SECTION m_sMutex;
#endif  
};



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Semaphore
{
 public:
  C_Semaphore(unsigned int iInitialValue);
  ~C_Semaphore();

  // Increases the semaphore count by one
  int Post();
  // Wait until the semaphore count is greater than 0, and then decreases the
  // count by one
  int Wait();
  // Non bloquing variant of the previous method
  int TryWait();
  // Return the count of the semaphore
  unsigned int GetValue();

 private:
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  /* Disabled the use of sem_t because they're not available on some
   * platforms (read : Darwin) */
  sem_t sSemaphore;
# else
  unsigned int sSemaphore;
  pthread_mutex_t m_sMutex;
  pthread_cond_t m_sCondition;
# endif
#elif defined _WIN32
  HANDLE sSemaphore;
#endif
};



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// This is an extension to the semaphore mecanism. Semaphores are a kind of
// reference counter on a shared ressource. Conditions allow you to synchronize
// 2 or more thread on the value of any shared data.
//------------------------------------------------------------------------------
class C_Condition
{
 public:
  C_Condition();
  ~C_Condition();

  // Protect the shared data against concurrent access
  void Protect();
  
  // Release access to the shared data
  void Release();

  // Signal the condition. Protect must have been called before (therefore the
  // shared data can be modified before the call to Signal/Broadcast)
  // Signal warns a single thread among all the ones which are waiting for
  // the condition, Broadcast restarts all the waiting threads.
  void Signal();
  void Broadcast();

   // Wait until the condition is signaled. Protect must have been called before
  // (therefore the value of the shared data can be tested before the call to
  // Wait()).
  // Exclusive access to the shared data is garanted until Released is called
  // or until another Wait().
  void Wait();
  int Wait(unsigned int iTimeout);

 private:
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  pthread_mutex_t m_sMutex;
  pthread_cond_t m_sCondition;
#elif defined _WIN32
  CRITICAL_SECTION m_sMutex;
  int m_iWaitingThreads;
  HANDLE m_sSignal;
#endif
};


#else
#error "Multiple inclusions of thread.h"
#endif

