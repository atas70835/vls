/*******************************************************************************
* map.cpp: map container class implementation
*-------------------------------------------------------------------------------
* (c)2003 VideoLAN
* $Id$
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


//******************************************************************************
// class C_Map
//******************************************************************************
// Implementation of map functionality for simple datatypes.
//******************************************************************************

//------------------------------------------------------------------------------
// C_Map::Add
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_Map<Key, Value>::Add(const Key& cKey, const Value& cValue)
{
  m_vKeys.Add(cKey);
  m_vValues.Add(cValue);
}

//------------------------------------------------------------------------------
// C_Map::Delete
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_Map<Key, Value>::Delete(const Key& cKey)
{
  int found = m_vKeys.Find(cKey);

  if (found>=0)
  {
    m_vKeys.Delete(cKey);
    m_vValues.Delete(this->cValue);
  }
}

//------------------------------------------------------------------------------
// C_Map::Update
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_Map<Key, Value>::Update(const Key& cKey, Value& cNewValue)
{
  int index = m_vKeys.Find(cKey);

  if (index>=0)
    m_vValues[index] = cNewValue;
}

//------------------------------------------------------------------------------
// C_Map::Find
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
bool C_Map<Key, Value>::Find(const Key& cKey) const
{
  return (m_vKeys.Find(cKey)>=0);
}

//------------------------------------------------------------------------------
// C_Map::Get
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Return *empty* value when key cannot be found
//------------------------------------------------------------------------------
template <class Key, class Value>
Value C_Map<Key, Value>::Get(const Key& cKey) const
{
  int index = m_vKeys.Find(cKey);

  if (index>=0)
    return m_vValues[index];

  throw E_Exception(GEN_ERR, "Key not found in C_Map class. Use Find() method first.");
}


//******************************************************************************
// class C_MapIterator
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// C_MapIterator::C_MapIterator
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key,class Value>
C_MapIterator<Key, Value>::C_MapIterator(const C_Map<Key,Value>& cMap)
      : m_mMap(cMap),
        m_iIndex(-1)
{
}

//------------------------------------------------------------------------------
// C_MapIterator::Init
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key,class Value>
inline void C_MapIterator<Key, Value>::Init()
{
  m_iIndex = 0;
}


//------------------------------------------------------------------------------
// C_MapIterator::HasNext
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key,class Value>
inline bool C_MapIterator<Key, Value>::HasNext()
{
  return m_iIndex < m_mMap.m_vKeys.size();
}


//------------------------------------------------------------------------------
// C_MapIterator::GetNext
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key,class Value>
inline void C_MapIterator<Key, Value>::GetNext()
{
  m_iIndex++;
}

//------------------------------------------------------------------------------
// C_MapIterator::Current
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key,class Value>
inline Key C_MapIterator<Key, Value>::Current()
{
  return m_mMap.m_vKeys[m_iIndex];
}


