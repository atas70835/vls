/*******************************************************************************
* vector.h: Vector container class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: vector.h,v 1.5 2003/08/28 18:03:26 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


#ifndef _VECTOR_H_
#define _VECTOR_H_


//******************************************************************************
// class C_Vector
//******************************************************************************
// The simplest of the container classes, and in many cases the most efficient.
// It is often used where a variable size array is needed.
// Supports random access to elements, insertion and removal of elements
// anywhere in the container. The number of elements in a vector may vary
// dynamically; memory management is automatic.
//******************************************************************************
template <class T> class C_Vector
{
 public:
  // Creates an empty vector that will grow by iGrothFactor elements
  C_Vector(unsigned int iGrowthFactor = 20, byte bAutoClean = YES);
  // Destroy the vector
  ~C_Vector();
  // Copy constructor
  C_Vector(const C_Vector<T>& vSrc);
  C_Vector<T>& operator = (const C_Vector<T>& vSrc);

  // Sort methods
  void Sort();
  void QuickSort(int iLeft, int iRight);
  void Swap(unsigned int i, unsigned int j);
  
  void Add(T* pElem);

  T* Remove(unsigned int iIndex);
  void Delete(unsigned int iIndex);
  T& operator [] (unsigned int iIndex) const;

  int Find(const T& cElem) const;

  void Reserve(unsigned int iNbElem);
  unsigned int Capacity() const;
  inline unsigned int Size() const {return m_uiSize;}

  // Removal of elements is done according to the bAutoClean flag
  void Empty();

 private:
  T** m_apElems;
  unsigned int m_uiSize;
  unsigned int m_uiCapacity;
  unsigned int m_uiGrowthFactor;
  byte m_bAutoClean;
};


// The user of this class is itself responsible for cleaning up all pointers to classes.
// The template is now useable for basic types, classes and pointers to classes.
template <class T> class C_VectorBase
{
 public:
  // Creates an empty vector that will grow by iGrothFactor elements
  C_VectorBase(unsigned int iGrowthFactor = 20);
  // Destroy the vector
  ~C_VectorBase();
  // Copy constructor
  C_VectorBase(const C_VectorBase<T>& vSrc);
  C_VectorBase<T>& operator = (const C_VectorBase<T>& vSrc);


  void Add(const T& pElem);

  T Remove(unsigned int iIndex);
  T& operator [] (unsigned int iIndex) const;

  int Find(const T& cElem) const;

  void Reserve(unsigned int iNbElem);
  unsigned int Capacity() const;
  unsigned int Size() const;

  // To do :)
  void Sort();

 private:
  T* m_apElems;
  unsigned int m_uiSize;
  unsigned int m_uiCapacity;
  unsigned int m_uiGrowthFactor;
};


#else
#error "Multiple inclusions of vector.h"
#endif

