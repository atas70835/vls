/*******************************************************************************
* pipe.h: communication with and external helper
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: pipe.h,v 1.1 2003/10/28 16:18:09 tooney Exp $
*
* Authors: Tristan Leteurtre <tristan.leteurtre@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/


#ifndef _PIPE_H_
#define _PIPE_H_


//******************************************************************************
// class C_DoublePipe
//******************************************************************************
// Use to communicate with an external program using its StdIn and StdErr
//           VLS Process
//    stdin1 ||      /\ stderr0          0 = read
//           ||      ||                  1 = write
//    stdin0 \/      || stderr1
//          Child Process
//******************************************************************************

class C_DoublePipe
{
 public:
  C_DoublePipe();
  int FatherClose();
  int ChildDup2();
  int GetWriteFd() {return m_iStdIn[1];};
  int GetReadFd()  {return m_iStdErr[0];};
  
 private:
  int m_iStdIn[2];
  int m_iStdErr[2];
};

#else
#error "Multiple inclusions of pipe.h"
#endif

