/*******************************************************************************
* normaltrickplay.h: Header file for the normal trickplay implementation
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: normaltrickplay.h,v 1.4 2003/08/14 17:20:36 adq Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _NORMAL_TRICKPLAY_H_
#define _NORMAL_TRICKPLAY_H_

//------------------------------------------------------------------------------
// C_NormalTrickPlay class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_NormalTrickPlay : public C_TrickPlay
{
 public:
  C_NormalTrickPlay() {};
  C_NormalTrickPlay(C_Module* pModule,
                      C_TrickPlayConfig& cConfig);

  void HandlePacket(C_TsPacket* pPacket);

  // TrickPlay policy is implemented in these functions
  virtual void Resume();
  virtual void Suspend();
  virtual void Forward(int speed);
  virtual void Rewind(int speed);

 protected:
  virtual void InitWork();
  virtual void DoWork();
  virtual void StopWork();
  virtual void CleanWork();

private:
  C_Fifo<C_TsPacket>* m_pPackets;
};

// Declaration and implementation of C_TrickPlayModule
DECLARE_MODULE(Normal,TrickPlay,"normal",C_TrickPlayConfig&);

#else
#error "Multiple inclusions of normaltrickplay.h"
#endif

