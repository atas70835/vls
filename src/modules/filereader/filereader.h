/*******************************************************************************
* filereader.h: file reader
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Implementation of a file stream reader (C_FileMpegReader).
*******************************************************************************/


#ifndef _FILE_READER_H_
#define _FILE_READER_H_


//------------------------------------------------------------------------------
// C_MpegFileReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_FileMpegReader : public C_MpegReader 
{
public:
  C_FileMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual int GetFrame(byte **,int) { return 0;};
  virtual int GetAudioFD() { return 0;};
  
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();
  virtual void ResetEndOfStream();

protected:
  C_File m_cFile;

  bool m_bEnd;
};


// Declaration and implementation of C_FileMpegReaderModule
DECLARE_MODULE(File, MpegReader, "file", C_Broadcast*);


#else
#error "Multiple inclusions of filereader.h"
#endif

