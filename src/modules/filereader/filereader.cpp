/*******************************************************************************
* filereader.cpp: file reader
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: filereader.cpp,v 1.14 2003/10/27 10:58:10 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "filereader.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_FileMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_filereader(handle hLog)
{
  return new C_FileMpegReaderModule(hLog);
}
#endif


/*******************************************************************************
* C_FileMpegReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_FileMpegReader::C_FileMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast),
                        m_cFile(pBroadcast->GetOption("filename"))
{
  if(pBroadcast->GetOption("end") == "1")
    m_bEnd = true;
  else
    m_bEnd = false;
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_FileMpegReader::Init()
{
  m_cFile.Open("rb");

  // Jump to end of buffer to a distance of about 3 seconds from end
  if (m_bEnd)
  {
    if (m_cFile.Seek( (long)-(3*8*188*1024), FILE_SEEK_END)==-1)
        printf( "Option --end failed to start.\n");
    else
        printf( "Option --end started.\n" );
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_FileMpegReader::Close()
{
  m_cFile.Close();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_FileMpegReader::Read(byte* pBuff, s64 iSize)
{
  try
  {
    s64 iRc = m_cFile.Read(pBuff, iSize);

    // check for end of stream
    if ((iRc == FILE_EOF) || (iRc != iSize)) {
      m_bEndOfStream = true;
    }
    return iRc;
  }
  catch(E_File e)
  {
    return MPEG_STREAMERROR;
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_FileMpegReader::ResetEndOfStream()
{
  m_cFile.Seek((s64)0, FILE_SEEK_BEGIN);
  m_bDiscontinuity = true;
  m_bEndOfStream = false;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_FileMpegReader::Seek(s64 iOffset, s64 bStartPos)
{
  return m_cFile.Seek(iOffset, bStartPos);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_FileMpegReader::Size()
{
  return m_cFile.Size();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_FileMpegReader::GetPos()
{
  return m_cFile.GetPos();
}
