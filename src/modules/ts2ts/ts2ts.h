/*******************************************************************************
* ts2ts.h: TS to TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: ts2ts.h,v 1.6 2003/08/23 21:38:48 adq Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _TS2TS_CONVERTER_H_
#define _TS2TS_CONVERTER_H_


//------------------------------------------------------------------------------
// C_Ts2TsMpegConverter class
//------------------------------------------------------------------------------
// Mpeg 2 TS -> Mpeg 2 TS (no conversion)
//------------------------------------------------------------------------------
class C_Ts2TsMpegConverter : public C_MpegConverter
{
public:
  C_Ts2TsMpegConverter(C_Module* pModule, C_MpegConverterConfig& cConfig);
  virtual void InitWork();

  virtual s64 GetNextTsPackets(C_Fifo<C_TsPacket> * pPacket, int skip = 0);
  virtual s64 GetPrevTsPackets(C_Fifo<C_TsPacket> * pPacket, int skip = 0);

protected:
  int Synch();
  s64 inline CorrectForward(s64 size);
  s64 inline CorrectRewind(s64 size);
  void ZeroPacket(C_TsPacket* pPacket);

private:
  const int m_iCorrectPercentage;
  int m_iLoop;
};


// Declaration and implementation of C_Ts2TsMpegConverterModule
DECLARE_MODULE(Ts2Ts, MpegConverter, "ts2ts", C_MpegConverterConfig&);


#else
#error "Multiple inclusions of ts2ts.h"
#endif

