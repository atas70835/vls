/*******************************************************************************
* v4linput.cpp: Reading from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: v4linput.cpp,v 1.21 2003/10/27 10:58:11 sam Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"

#include "v4linput.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_v4lInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_v4linput(handle hLog)
{
  return new C_v4lInputModule(hLog);
}
#endif


/*******************************************************************************
* C_v4lInput class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_v4lInput::C_v4lInput(C_Module* pModule,
                           const C_String& strName) : C_Input(pModule,
                                                              strName)
{
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_v4lInput::~C_v4lInput()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_v4lInput::OnInit()
{
  // Build the program list
  C_String* pStr = new C_String("video");
  m_vProgramNames.Add(pStr);
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_v4lInput::OnDestroy()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_v4lInput::OnGetAvailablePgrms()
{
  C_List<C_Program> cPgrmList;

  for(unsigned int ui = 0; ui < m_vProgramNames.Size(); ui++)
  {
    C_Program* pProgram = new C_Program(/*ui + 1,*/ m_vProgramNames[ui]);
    ASSERT(pProgram);
    cPgrmList.PushEnd(pProgram);
  }

  return cPgrmList;
}


//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_v4lInput::OnStartStreaming(C_Broadcast* pBroadcast)
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  ASSERT(pBroadcast);

  // We choose a TS packet buffer able to store up to 3s of stream at 8 Mbits/s
  C_NetList* pTsProvider = new C_NetList(3*3*797);

  // The netlist must be at least of the size of the Reader buffer +
  // the size of the output buffer + 2 to be sure that there are always free

  // packets in it
  const C_Channel* pChannel = pBroadcast->GetChannel();
  ASSERT(pTsProvider->Capacity() - pChannel->GetBuffCapacity() - 2 > 0);
  unsigned int uiSize = pTsProvider->Capacity() -pChannel->GetBuffCapacity()-2;
  C_SyncFifo* pBuffer;

  C_String strDevice = pApp->GetSetting(GetName()+".device", "/dev/video");
  pBroadcast->SetOption("v4l_device", strDevice);

  C_String strAudioDevice =
                  pApp->GetSetting(GetName()+".audiodevice", "/dev/dsp");
  pBroadcast->SetOption("v4l_audiodevice", strAudioDevice);
  
  C_String strV4LQuality = pApp->GetSetting(GetName()+".quality", "31.0");
  pBroadcast->SetOption("v4l_quality", strV4LQuality);

  C_String strV4LChannel = pApp->GetSetting(GetName()+".channel", "0");
  pBroadcast->SetOption("v4l_channel", strV4LChannel);

  C_String strV4LSize = pApp->GetSetting(GetName()+".size", "");
  pBroadcast->SetOption("v4l_size", strV4LSize);

  C_String strV4LNorm = pApp->GetSetting(GetName()+".norm", "0");
  pBroadcast->SetOption("v4l_norm", strV4LNorm);
  
  C_String strV4LBitrate = pApp->GetSetting(GetName()+".bitrate", "1000");
  pBroadcast->SetOption("v4l_bitrate", strV4LBitrate);

   C_String strV4LMute = pApp->GetSetting(GetName()+".audiomute", "false");
  pBroadcast->SetOption("v4l_mute", strV4LMute);
  
  C_String strV4LAudioChannel =
    pApp->GetSetting(GetName()+".audiochannel", "2");
  pBroadcast->SetOption("v4l_audiochannel", strV4LAudioChannel);

  C_String strV4LAudioFreq =
    pApp->GetSetting(GetName()+".audiofreq", "44100");
  pBroadcast->SetOption("v4l_audiofreq", strV4LAudioFreq);

  C_String strV4LAudioBitrate =
    pApp->GetSetting(GetName()+".audiobitrate", "64");
  pBroadcast->SetOption("v4l_audiobitrate", strV4LAudioBitrate);

  C_String strV4LFrequency = pApp->GetSetting(GetName()+".frequency", "-1");
  pBroadcast->SetOption("v4l_frequency", strV4LFrequency);

  C_String strV4LDeInt = pApp->GetSetting(GetName()+".deinterlace", "0");
  pBroadcast->SetOption("v4l_deinterlace", strV4LDeInt);

  C_String strV4LAudioCodec = pApp->GetSetting(GetName()+".audiocompression", "mp2");
  pBroadcast->SetOption("v4l_audiocodec", strV4LAudioCodec);
    
  C_String strV4Lcodec = pApp->GetSetting(GetName()+".compression", "mpeg1");
  pBroadcast->SetOption("v4l_codec", strV4Lcodec);
    
  C_String strReaderType;
  C_String strConverterType;
  C_String strTrickPlayType;

  // Update the size of the buffer and create it
  uiSize -= 2;
  pBuffer = new C_SyncFifo(uiSize);
   
  // Reader configuration
  C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
               C_Application::GetModuleManager()->GetModule("mpegreader","v4l");

  C_MpegReader* pReader;

  if(pReaderModule)
  {
    pReader = pReaderModule->NewMpegReader(pBroadcast);
    ASSERT(pReader);
  }
  else
  {
    throw E_Exception(GEN_ERR, "Module mpegreader: v4l not present");
  }

  // Create the converter
  C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
               C_Application::GetModuleManager()->GetModule("mpegconverter","raw2ts");

  C_MpegConverter* pConverter;

  if(pConverterModule)
  {
    C_MpegConverterConfig cConfig;
    cConfig.m_hLog = m_hLog;
    cConfig.m_pBroadcast = pBroadcast;
    cConfig.m_pReader = pReader;
    cConfig.m_pTsProvider = pTsProvider;
    cConfig.m_pEventHandler = m_pEventHandler;
    pConverter = pConverterModule->NewMpegConverter(cConfig);
    ASSERT(pConverter);
  }
  else
  {
    throw E_Exception(GEN_ERR, "Module mpegconverter: raw2ts not present");
  }

  pReader->SetConverter(pConverter);

  // Create the TrickPlay
  C_TrickPlay* pTrickPlay;
  strTrickPlayType = pApp->GetSetting(GetName() + ".trickPlay",
                      "normal").ToLower();
  C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("trickplay",
                                                    strTrickPlayType);
  if (pTrickPlayModule)
  {
    C_TrickPlayConfig cTrickPlayConfig;
    cTrickPlayConfig.m_hLog = m_hLog;
    cTrickPlayConfig.m_pBroadcast = pBroadcast;
    cTrickPlayConfig.m_pReader = pReader;
    cTrickPlayConfig.m_pHandler = pBuffer;
    cTrickPlayConfig.m_iInitFill = 0 ;
    cTrickPlayConfig.m_pEventHandler = m_pEventHandler;
    cTrickPlayConfig.m_pTsProvider = pTsProvider;
    cTrickPlayConfig.m_pConverter = pConverter;

    pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
    ASSERT(pTrickPlay);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module TrickPlay:" + strTrickPlayType +
                      " not present");
  }

  // Create the streamer
  C_TsStreamer* pStreamer = new C_TsStreamer(m_hLog, pBroadcast, pTsProvider, pBuffer,
                                   m_pEventHandler, true, false);
  ASSERT(pStreamer);

  m_cTrickPlay.Add(pBroadcast, pTrickPlay);
  m_cStreamers.Add(pBroadcast, pStreamer);

  try
  {
    pTrickPlay->Create();
    pStreamer->Create();
  }
  catch(E_Exception e)
  {
    pStreamer->Stop();
    pTrickPlay->Stop();

    // Unregister the 2 thread and delete them
    m_cTrickPlay.Delete(pBroadcast);
    m_cStreamers.Delete(pBroadcast);

    throw E_Exception(GEN_ERR, "unable to start streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_v4lInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the converter that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  // Resume the thread
  try
  {
    pTrickPlay->Resume();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_v4lInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the converter that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  C_String strPgrmName = pBroadcast->GetProgram()->GetName();

  // Suspend the thread
  try
  {
    pTrickPlay->Suspend();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_v4lInput::OnStopStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the reader and the streamer that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Remove(pBroadcast);
  ASSERT(pTrickPlay);
  C_TsStreamer* pStreamer = m_cStreamers.Remove(pBroadcast);
  ASSERT(pStreamer);

  // Stop the threads
  try
  {
    pTrickPlay->Stop();
    pStreamer->Stop();
    delete pTrickPlay;
    delete pStreamer;
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_v4lInput::OnUpdateProgram(C_String strProgram,
                                     C_String strFileName, C_String strType)
{
    // TODO Write this part
    ASSERT(false);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_v4lInput::OnDeleteProgram(C_String strProgram)
{
  ASSERT(false);
}
