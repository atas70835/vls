/*******************************************************************************
* netchannel.cpp: network channel
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: netchannel.cpp,v 1.12 2004/02/16 17:07:48 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"
#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"

#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"

#include "netoutput.h"
#include "netchannel.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_NetChannelModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_netchannel(handle hLog)
{
  return new C_NetChannelModule(hLog);
}
#endif


//******************************************************************************
// class C_NetChannel
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_NetChannel::C_NetChannel(C_Module* pModule,
                           const C_ChannelConfig& cConfig) : C_Channel(pModule,
                                                                cConfig)
{
  m_pOutput = NULL;
  if(cConfig.m_strDomain == "inet4")
    m_pOutput = new C_Net4Output(cConfig);
#ifdef HAVE_IPV6
  else if(cConfig.m_strDomain == "inet6")
    m_pOutput = new C_Net6Output(cConfig);
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(cConfig.m_strDomain == "unix")
    m_pOutput = new C_UnixOutput(cConfig);
#endif
  else
    throw E_Exception(GEN_ERR, "Unknown domain \"" + cConfig.m_strDomain +
                      "\" for network channel \"" + cConfig.m_strName + "\"");
}


