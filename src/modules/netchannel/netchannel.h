/*******************************************************************************
* netchannel.h: network channel
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: netchannel.h,v 1.2 2003/08/05 23:18:19 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _NETCHANNEL_H_
#define _NETCHANNEL_H_


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_NetChannel : public C_Channel
{
 public:
  C_NetChannel(C_Module* pModule, const C_ChannelConfig& cConfig);
};


DECLARE_MODULE(Net, Channel, "network", const C_ChannelConfig&);


#else
#error "Multiple inclusions of netchannel.h"
#endif

