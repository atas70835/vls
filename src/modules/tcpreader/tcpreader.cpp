/*******************************************************************************
* TCP reader.cpp: TCP reader
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "tcpreader.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_TcpMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_tcpreader(handle hLog)
{
  return new C_TcpMpegReaderModule(hLog);
}
#endif


/*******************************************************************************
* C_XXXMpegReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_TcpMpegReader::C_TcpMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast)
{
    m_bXOR=0;
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_TcpMpegReader::Init()
{
    // We need to create the socket and connect it
     m_cSocket.Open(AF_INET, SOCK_STREAM);

     // TODO
     m_cSocket.Connect("127.0.0.1", "4212");

    return;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TcpMpegReader::Close()
{
    try
    {
        m_cSocket.Close();
    }
    catch(E_Exception e)
    {
        throw E_Exception(GEN_ERR, "Reader termination failed to close socket");
    }
    return;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_TcpMpegReader::Read(byte* pBuff, s64 iSize)
{
     unsigned int iRc = m_cSocket.Read((char*)pBuff,iSize);

     CS_XOR(pBuff, iSize);

     if(iRc<iSize) fprintf(stderr, "EOF reached\n");

     return iRc;
}

void C_TcpMpegReader::CS_XOR(byte* pBuff, s64 iSize)
{
    for(unsigned int i=0; i<iSize; i++) m_bXOR ^= pBuff[i];
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TcpMpegReader::ResetEndOfStream()
{
    ASSERT(false);
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_TcpMpegReader::Seek(s64, s64)
{
    ASSERT(false);
    return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_TcpMpegReader::Size()
{
    ASSERT(false);
    return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_TcpMpegReader::GetPos()
{
    ASSERT(false);
    return 0;
}
