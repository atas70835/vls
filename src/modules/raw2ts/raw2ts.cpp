/*******************************************************************************
* raw2ts.cpp: raw -> ES ffmpeg encoder -> TS
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: raw2ts.cpp,v 1.39 2003/10/28 23:48:45 tooney Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*          Eric Petit <titer@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/pes.h"

#include "../../server/buffer.h"
#include "../../server/program.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/psi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#else
#   include "src/dvbpsi.h"
#   include "src/psi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#endif

#include "../../mpeg/dvbpsi.h"

#include "rawcapture.h"
#include "raw2ts.h"

#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/mman.h>
#include <unistd.h>

/* For example : 0,1s = 100000us -> about 10000 in 90 kHz clock */
#define DELTA_PCR_PTS 50000

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_Raw2TsMpegConverterModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_raw2ts(handle hLog)
{
  return new C_Raw2TsMpegConverterModule(hLog);
}
#endif


/*******************************************************************************
* C_Raw2TsConverter
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Raw2TsMpegConverter::C_Raw2TsMpegConverter(C_Module* pModule,
                                           C_MpegConverterConfig& cConfig) :
                            C_MpegConverter(pModule, cConfig)
{
  m_VideoCodec = NULL;
  m_VideoCodecContext = NULL;
  m_AudioCodec = NULL;
  m_AudioCodecContext = NULL;
  m_pDatedBuffer = NULL;
  m_iPos = 0;

}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_Raw2TsMpegConverter::InitWork()
{
  // Inherited method
  C_MpegConverter::InitWork();

  iVideoContinuityCounter = 0;
  iAudioContinuityCounter = 0;

  // get configuration
  m_iHeight = m_pBroadcast->GetOption("FrameHeight").ToInt();
  m_iWidth = m_pBroadcast->GetOption("FrameWidth").ToInt();
  m_iFrameRate = m_pBroadcast->GetOption("FrameRate").ToInt();
  m_iPalette = m_pBroadcast->GetOption("Palette").ToInt();
  m_bDeInterlace = m_pBroadcast->GetOption("v4l_deinterlace").ToInt();
  m_strCompression = m_pBroadcast->GetOption("v4l_codec") ;
  m_audioCompression = m_pBroadcast->GetOption("v4l_audiocodec") ;
  m_channel = m_pBroadcast->GetOption("v4l_audiochannel").ToInt() ;
  m_audiofreq = m_pBroadcast->GetOption("v4l_audiofreq").ToInt() ;
  m_quality = atof(m_pBroadcast->GetOption("v4l_quality").GetString()) ;
  m_bMute = m_pBroadcast->GetOption("v4l_mute") == "true" ? true : false;

  if (m_bDeInterlace)
    fprintf( stderr, "Info : Image de-interlacement ON\n");

  // Init ffmpeg AV codec stuff
  avcodec_init();
  avcodec_register_all();

  if(m_strCompression == "mpeg4") {
    m_type = 0x10 ;
  }
  else if(m_strCompression == "mpeg1video") {
    m_type = 0x01 ; /* iso 11172 video */
  }
  else { // All other encoders are not working with vlc.
    m_type = 0x01 ; /* iso 11172 video */
    m_strCompression = "mpeg1video" ;
  }
  // prepare the video encoder
  m_VideoCodec = avcodec_find_encoder_by_name(m_strCompression.GetString());
  if (!m_VideoCodec)
  {
    m_strCompression="mpeg1video";
    m_VideoCodec = avcodec_find_encoder(CODEC_ID_MPEG1VIDEO);
  }
  if (!m_VideoCodec)
  {
    throw E_Exception(GEN_ERR,
                      "Cannot find encoder " + m_strCompression + " Video");
  }

  // prepare the audio encoder
  // register_avcodec(&mp2_encoder);
  if (!m_bMute)
  {
    m_AudioCodec=avcodec_find_encoder_by_name(m_audioCompression.GetString());

    // XXX temporary
    if ( m_audioCompression.GetString() != "mp2")
    {
      printf("Only mp2 compression is supposed to work for the moment !\n");
    }

    if( !m_AudioCodec )
    {
      m_audioCompression = "mp2" ;
      m_AudioCodec = avcodec_find_encoder(CODEC_ID_MP2);
    }

    if( !m_AudioCodec )
    {
      throw E_Exception(GEN_ERR, "Cannot find encoder "
                                          + m_audioCompression + " Audio");
    }
  }
  
  // video parameters
  m_VideoCodecContext = avcodec_alloc_context();
  m_VideoCodecContext->bit_rate =
                        m_pBroadcast->GetOption("v4l_bitrate").ToInt() * 1000;
  m_VideoCodecContext->bit_rate_tolerance =
                                  m_VideoCodecContext->bit_rate / 5; // 20%
  m_VideoCodecContext->width = m_iWidth;
  m_VideoCodecContext->height = m_iHeight;
  m_VideoCodecContext->frame_rate = m_iFrameRate;
  m_VideoCodecContext->frame_rate_base = 1;
  m_VideoCodecContext->gop_size =
                          m_iFrameRate; // emit one intra frame every second
  m_VideoCodecContext->pix_fmt = (enum PixelFormat)m_iPalette;
  m_VideoCodecContext->qmin = 2;
  m_VideoCodecContext->qmax = 31;
  m_VideoCodecContext->max_qdiff = 5;
  //m_VideoCodecContext->qblur = 0.5;
  //m_VideoCodecContext->qcompress = 0.5;
  //m_VideoCodecContext->me_method = 0;

  switch(m_iPalette)
  {
    case PIX_FMT_YUV420P:
      m_iPictureBufSize = (m_iHeight * m_iWidth) * 3 / 2 ;
      break;
    case PIX_FMT_YUV422:
      m_iPictureBufSize = (m_iHeight * m_iWidth) * 2 ;
      break;
    case PIX_FMT_BGR24:
      m_iPictureBufSize = (m_iHeight * m_iWidth) * 3 ;
      break;
    default:
      Log( m_hLog, LOG_WARN, "Warning : unknown palette\n" );
      m_iPictureBufSize = 0;
      break;
  }

  /* Be carefull ! Some parameters may have changed, like palette ! */
  fprintf( stderr, "Info : Encoding video : %dx%d %d fps at %d kb/s in %s\n",
            m_iWidth, m_iHeight, m_iFrameRate,
            (m_VideoCodecContext->bit_rate / 1000),
            m_strCompression.GetString());

  // "open" the video encoder
  if (avcodec_open(m_VideoCodecContext, m_VideoCodec) < 0)
  {
    throw E_Exception(GEN_ERR, "Cannot init video encoder");
  }

  // "open" the audio encoder
  if (!m_bMute)
  {
    m_AudioCodecContext = avcodec_alloc_context();
    m_AudioCodecContext->sample_rate = m_audiofreq ;
    m_AudioCodecContext->channels = m_channel ;
    m_AudioCodecContext->codec_type = CODEC_TYPE_AUDIO;

    m_AudioCodecContext->bit_rate =
                   m_pBroadcast->GetOption("v4l_audiobitrate").ToInt() * 1000;
    /* Only in mp2 stereo !!
    if( m_AudioCodecContext->bit_rate < 32 )
    {
      // ffmpeg does not like bitrate under 32000, back to default
      m_AudioCodecContext->bit_rate = 64;
    }
    */
    fprintf( stderr, "Info : Encoding audio : %u Hz %s at %d kb/s in %s\n",
      m_audiofreq, (m_channel==2)?"stereo":"mono",
      (m_AudioCodecContext->bit_rate / 1000), m_audioCompression.GetString() );

    if( avcodec_open( m_AudioCodecContext, m_AudioCodec ) < 0 )
    {
      throw E_Exception(GEN_ERR, "Cannot init audio encoder");
    }
  }

  // prepare buffers
  m_iVideoOutBufSize = 500000;
  m_VideoOutBuf = new u8 [m_iVideoOutBufSize];
  m_Picture = (AVPicture *)malloc(sizeof(AVPicture));

  // start recording to fifos (size = 1 sec)
  m_pVideoFifo = new C_DatedFifo( m_iFrameRate );

  if (!m_bMute)
  {
    m_iFrameSize =  m_AudioCodecContext->frame_size; // 1152 in mp2
   
    m_iAudioSampleBufSize = 2 * m_AudioCodecContext->frame_size *
                                    m_AudioCodecContext->channels;
 
    m_iAudioOutBufSize = 1024*128; // FIXME: totally arbitrary !
    if (!m_bMute) m_AudioOutBuf = (byte*)malloc( m_iAudioOutBufSize );

    // start recording to fifos (size = 1 sec)
    m_pAudioFifo = new C_DatedFifo( 2 * m_channel * m_audiofreq /
                                                   m_iAudioSampleBufSize );
  }
  else
    m_pAudioFifo = new C_DatedFifo(1); //not zero, beware of divisions !

  m_pHasDataCondition = new C_Condition();
  m_pVideoThread = new C_VideoCaptureThread( m_hLog, m_pReader, m_pVideoFifo,
                                  m_iPictureBufSize, m_pHasDataCondition,
                                          m_quality );
  m_pVideoThread->Create();

  if (!m_bMute)
  {
    m_pAudioThread = new C_AudioCaptureThread( m_hLog, m_pReader, m_pAudioFifo,
                                m_iAudioSampleBufSize, m_pHasDataCondition,
                                            m_channel, m_audiofreq );
    m_pAudioThread->Create();
  }
  
  m_pPesPacket = new C_PesPacket();
  
  /* Build a PAT */
  m_cPat = new C_DvbPsiPat(0, 0, true);
  dvbpsi_PATAddProgram(m_cPat->GetLowLevelPat(), 0x1212, 0x12);
  m_cPat->Generate();
  m_cPat->TsReset();

  /* Build a PMT */
  // XXX TODO : with mpeg4 some descriptors have to be specified in PAT !

  m_cPmt = new C_DvbPsiPmt(0x1212, 0, true, 0xE0, 0x12);
  dvbpsi_PMTAddES(m_cPmt->GetLowLevelPmt(), m_type, 0xE0);
  if (!m_bMute)
  {
    switch (m_AudioCodec->id) {
      case CODEC_ID_MP2:
      case CODEC_ID_MP3LAME:
        m_atype = TS_TYPE_MPEG1_AUDIO;
        break;
      case CODEC_ID_AC3:
        m_atype = 0x81; /* should be TS_TYPE_AC3 */
        break;
      case CODEC_ID_VORBIS:
        /* FIXME: what is the stream type for Vorbis? */
      default:
        fprintf(stderr, "Warning: unknown FFmpeg audio codec %d, using bogus ES"
                " stream type of MPEG1 audio\n", m_AudioCodec->id);
        m_atype = TS_TYPE_MPEG1_AUDIO;
        break;
    }
    dvbpsi_PMTAddES(m_cPmt->GetLowLevelPmt(), m_atype, 0xC0);
  }
  m_cPmt->Generate();
  m_cPmt->TsReset();
}

//------------------------------------------------------------------------------
// Get Previous Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Raw2TsMpegConverter::GetPrevTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip)
{
  return MPEG_BEGINOFSTREAM;
}


//------------------------------------------------------------------------------
// Get Next Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Raw2TsMpegConverter::GetNextTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip)
{
  s64 iRc = NO_ERR;
  AVPicture * TmpPicture;
  AVFrame m_Frame;
  u8 * TmpBuf;

  // if both fifos are empty, wait for a frame
#if 0
  int retry = 0;
  while( (m_bMute || !m_pAudioFifo->Size()) && !m_pVideoFifo->Size() )

  {
     retry++;
     // there may be a more intelligent way of doing this.!!
     sleep(1);
     if (retry>=5)
       return MPEG_ENDOFSTREAM;
  }
#else
  // This object is no longer Threaded so this may not work!!
  if( (m_bMute || !m_pAudioFifo->Size()) && !m_pVideoFifo->Size() )
  { 
    m_pHasDataCondition->Protect();
    m_pHasDataCondition->Wait();
    m_pHasDataCondition->Release();
  }
#endif


  // encoding audio is our priority if the CPU is too slow;
  // otherwise, encode data from the most filled fifo
  if( !m_bMute &&
      ( m_pAudioFifo->Size() > (m_pAudioFifo->Capacity() / 2) ) ||
      ( (100 * m_pAudioFifo->Size() / m_pAudioFifo->Capacity()) >
        (m_pVideoFifo->Size() / m_pVideoFifo->Capacity()) ) )
  {
    int outSize;

    m_pDatedBuffer = m_pAudioFifo->Pop();

    // encode the buffer
    outSize = avcodec_encode_audio( m_AudioCodecContext, m_AudioOutBuf,
                                    m_iAudioOutBufSize,
                                    (short*)m_pDatedBuffer->m_pData );

    // build TS packets and send them
    m_iPos = EStoTS( pPackets, m_AudioOutBuf, outSize,
                  m_pDatedBuffer->m_TimeStamp,0xC0, &iAudioContinuityCounter);
    delete m_pDatedBuffer;
  }
  else
  {
    byte * to_free = NULL; // for deinterlacing

    {
      m_pDatedBuffer = m_pVideoFifo->Pop();

      avpicture_fill(m_Picture, m_pDatedBuffer->m_pData,
                     m_VideoCodecContext->pix_fmt,
                     m_iWidth, m_iHeight);

      if (m_bDeInterlace)
        DeInterlace(m_VideoCodecContext, m_Picture, &to_free );

      // Palette convertions, if needed
      if (m_iPalette != m_VideoCodecContext->pix_fmt)
      {
        avpicture_fill(TmpPicture, TmpBuf, m_VideoCodecContext->pix_fmt,
                       m_VideoCodecContext->width,
                       m_VideoCodecContext->height);
        if (img_convert(TmpPicture, m_VideoCodecContext->pix_fmt,
                        m_Picture, m_iPalette,
                        m_VideoCodecContext->width,
                        m_VideoCodecContext->height) < 0)
        {
          Log(m_hLog, LOG_ERROR, "Could not convert image\n");
        }
        m_Picture = TmpPicture;
      }

      *(AVPicture*)&m_Frame= *m_Picture;
      m_Frame.quality = m_quality;
      m_Frame.pts = 0;

      // encode the video frame
      m_iVideoOutSize = avcodec_encode_video(m_VideoCodecContext,
                                             m_VideoOutBuf,
                                             m_iVideoOutBufSize, &m_Frame);
    }
                                           
    // stream it
    m_iPos = EStoTS( pPackets, m_VideoOutBuf, m_iVideoOutSize,
             m_pDatedBuffer->m_TimeStamp, 0xE0, &iVideoContinuityCounter );

    if( to_free ) free( to_free ); // free the deinterlace picture
    delete m_pDatedBuffer; // free the original picture
  }

  // send some PAT and PMT !
  // kludge : Send sometime, not too often, not too rare...
  // ContinuityCounter <= 2 is an arbitrary criteria
  if (iVideoContinuityCounter == 0)
  {
    m_cPat->TsReset();
    C_TsPacket * pPacket = m_pTsProvider->GetPacket();
    m_cPat->TsWrite(pPacket);
    pPackets->Push(pPacket);
  }
  else if (iVideoContinuityCounter == 1)
  {
    m_cPmt->TsReset();
    C_TsPacket * pPacket = m_pTsProvider->GetPacket();
    m_cPmt->TsWrite(pPacket);
    pPackets->Push(pPacket);
  }

  if(iRc == MPEG_ENDOFSTREAM)
  {
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    LogDbg(m_hLog, "End of program \"" + strPgrmName + "\" reached");
  }
  else if (iRc != NO_ERR)
  {
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    Log(m_hLog, LOG_ERROR, "Read error for program \"" + strPgrmName + "\"");
  }
  
  return iRc;
}

void C_Raw2TsMpegConverter::StopWork()
{
  // Inherited method
  C_MpegConverter::StopWork();
  

  m_pVideoThread->Stop();
  if (!m_bMute) m_pAudioThread->Stop();


  // Init ffmpeg AV codec stuff
//  avcodec_close(m_VideoCodecContext);
//  avcodec_close(m_AudioCodecContext);

//  delete m_VideoCodecContext;
//  delete m_AudioCodecContext;
//  delete m_VideoOutBuf;
//  delete m_Picture;
//  delete m_AudioOutBuf;
//  delete m_pVideoFifo;
//  delete m_pAudioFifo;
//  delete m_pHasDataCondition;
//  delete m_pVideoThread;
//  delete m_pAudioThread;
//  delete m_pPesPacket;
}


//-------------------------------------------------------------
// TODO: Suggestion separate ES to TS into 2 functions:
// -one ES to Pes to create one PES packet, then
// -create a TS packet from that.
//-------------------------------------------------------------
// Packetizer :
//    - ADDs PES Header
//    - packtizes in TS packets
int C_Raw2TsMpegConverter::EStoTS( C_Fifo<C_TsPacket> *pPackets,
                                   byte * EsData, int EsSize, int TimeStamp,
                                    u8 iPayloadType, int * iCC)
{
    int iTmpEsSize;
  bool bFirstPES = true;
  int iVectorSize = pPackets->Size();

  for( iTmpEsSize = EsSize; iTmpEsSize > 0;
       iTmpEsSize -= ( PES_MAX_SIZE - 20 ) )
  {
    // maximum ( PES_MAX_SIZE - 20 ) of ES data because we must let
    // some space for PES headers
    m_pPesPacket->BuildPacket( bFirstPES ? TimeStamp : 0, 0xE0,
                             EsData + EsSize - iTmpEsSize,
                             iTmpEsSize > ( PES_MAX_SIZE - 20 ) ?
                               ( PES_MAX_SIZE - 20 ) : iTmpEsSize);
    bFirstPES = false;

    unsigned int iTmpPesSize, iHeader;
    bool bUnitStart = true;

    for( iTmpPesSize = m_pPesPacket->GetSize(); iTmpPesSize > 0;
         iTmpPesSize -= (TS_PACKET_LEN - iHeader ) )
    {
      // add TS headers
      C_TsPacket * pTsPacket = m_pTsProvider->GetPacket();
      ASSERT(pTsPacket);
      pTsPacket->BuildHeader(iPayloadType, bUnitStart, (*iCC)++);
      iHeader = 4;

      if (bUnitStart)
      {
        pTsPacket->BuildAdaptionField((u64)(TimeStamp - (u64)DELTA_PCR_PTS));
        iHeader += 8;
        bUnitStart = false;
      }

      if (iTmpPesSize < TS_PACKET_LEN - iHeader)
        iHeader = pTsPacket->AddStuffingBytes(iTmpPesSize);

      pTsPacket->FillWith(*m_pPesPacket + m_pPesPacket->GetSize() - iTmpPesSize,
                        iHeader  , TS_PACKET_LEN - iHeader);
    
      pPackets->Push(pTsPacket);              /* Push to Fifo    */
    }
      (*iCC) &= 0xF;
  }

  return pPackets->Size() - iVectorSize;
}



void C_Raw2TsMpegConverter::DeInterlace(AVCodecContext *pCodec,
                                        AVPicture *pPicture, byte ** to_free)
{
  /* See ffmpeg.c */
  u8 * pBuf = NULL;
  AVPicture pTmpPicture;
  AVPicture * pNewPicture = &pTmpPicture;

  int size;

  /* create temporary picture */
  size = avpicture_get_size(pCodec->pix_fmt, pCodec->width, pCodec->height);
  pBuf = (u8*) malloc( size );
  if (!pBuf)
    throw E_Exception(GEN_ERR, "Cannot init deinterlace buffer");

  avpicture_fill(pNewPicture, pBuf, pCodec->pix_fmt,
                 pCodec->width, pCodec->height);

  if (avpicture_deinterlace(pNewPicture, pPicture, pCodec->pix_fmt,
                            pCodec->width, pCodec->height) < 0)
  {
    /* if error, do not deinterlace */
    pNewPicture = pPicture;
  }

  if (pPicture != pNewPicture)
    *pPicture = *pNewPicture;

  *to_free = pBuf;
}


