/*******************************************************************************
* asiinput.cpp: Input for ASI boards
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id: asiinput.cpp,v 1.3 2003/10/27 10:58:10 sam Exp $
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*----------------------------------------------------------------------------
* This input is for work with an ASI board.
* Thus it only contains the real-time device streaming functions 
*******************************************************************************/
//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------

#include "config.h"

#include "../../core/core.h"

#include <sys/sysctl.h>
#include <fcntl.h>
#include <sys/ioctl.h>


#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#else
#   include "src/dvbpsi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#endif


#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"
#include "../../mpeg/tsdemux.h"
#include "../../mpeg/dvbpsi.h"
#include "../../mpeg/tsmux.h"

#include <asi.h>

#include "asiinput.h"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_AsiInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_asiinput(handle hLog)
{
      return new C_AsiInputModule(hLog);
}
#endif


/*******************************************************************************
* C_AsiInput class
********************************************************************************
*
*******************************************************************************/
//------------------------------------------------------------------------------
// Constructor

//------------------------------------------------------------------------------
C_AsiInput::C_AsiInput(C_Module* pModule, const C_String& strName)
                    : C_Input(pModule, strName),
                      C_TsDemux(&m_cTsProvider),
                      m_cTsProvider(500),
                      m_cInputProgram(/*0,*/ "Input ASI " + strName),
                      m_cInputBroadcast(&m_cInputProgram, this, NULL, strName),
                      m_cPatDecoder(&m_cTsProvider, this),
                      m_cCurrentPat(0, 0, true)
{
    m_hFd = 0;
    m_iDemuxUsageCount = 0;    // Nothing using the demux yet
    m_pConverter = NULL;
    m_pTrickPlay = NULL;
    m_strTrickPlayType = "normal"; //TODO
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_AsiInput::~C_AsiInput()
{
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_AsiInput::OnInit()
{
    unsigned int iBufSize;
    int iDeviceType;
    int iMode;
    int iInvSynchro;
    int iDoubleSynchro;

    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);

    /* Retrieve configuration parameters */
    C_String strInvSynchro = pApp->GetSetting(GetName() + ".synchro", "none");
    if( strInvSynchro == "0x47" ) iInvSynchro = 0;
    else if ( strInvSynchro == "0xb8" ) iInvSynchro = 1;
    else iInvSynchro = -1;

    C_String strDblSynchro = pApp->GetSetting(GetName() +
                                              ".doublesynchro", "none");
    if ( strDblSynchro == "disable" ) iDoubleSynchro = 0;
    else if ( strDblSynchro == "enable" ) iDoubleSynchro = 1;
    else iDoubleSynchro = -1;

    m_strDeviceName = pApp->GetSetting(GetName() + ".device", "/dev/asirx0");
    int iPid = -1;

    // Wow, so many stuffs to do here --nitrox

    try
    {
        /* 1. Open the file */
        OpenDevice();

        /* 2. Get the receiver capabilities */
        GetReceiverCapabilities();

        /* 3. Get the sysctl key (device type) */
        iDeviceType = GetDeviceType();

        /* 4. Get the buffer size */
        iBufSize = GetBufferSize(iDeviceType);

        /* 5. Get the receiver operating mode */
        iMode = GetReceiverOperatingMode(iDeviceType);

        /* 6. Get the packet timestamp mode */
        GetPacketTimestampMode(iDeviceType);

        /* 7. Enable/Disable synchronisation on 0xb8 */
        SetInvSynchro(iInvSynchro);

        /* 8. Enable/Disable double packet synchronization */
        SetDoubleSynchro(iDoubleSynchro);

        /* 9. Set the PID counter */
        SetPIDCounter(iPid, iMode);

    }
    catch(E_Exception e)
    {
        if(m_hFd>0) close(m_hFd);
        throw E_Exception(GEN_ERR, e.Dump());
    }

//    C_String* pStr = new C_String("asi");
//    m_vProgramNames.Add(pStr);
 //   Log(m_hLog, LOG_NOTE, "Added program '" + *pStr+"'");

    // Just proceed the same way as the dvb input: we attach a PAT decoder
    // in order to be able to get the list of the programs on the device
    m_cPatDecoder.Attach();

    // So we create the reader there
    C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
                                        C_Application::GetModuleManager()
                                        ->GetModule("mpegreader", "asi");
    ASSERT(pReaderModule);

    m_pReader = pReaderModule->NewMpegReader(&m_cInputBroadcast);
    ASSERT(m_pReader);





    // And the converter
    C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegconverter",
                                                    "ts2ts");
    ASSERT(pConverterModule);
    C_MpegConverterConfig cConfig;
    cConfig.m_hLog = m_hLog;
    cConfig.m_pBroadcast = &m_cInputBroadcast;
    cConfig.m_pReader = m_pReader;
    cConfig.m_pTsProvider = m_pTsProvider;
    cConfig.m_pEventHandler = this;
    m_pConverter = pConverterModule->NewMpegConverter(cConfig);
    ASSERT(m_pConverter);


    // And the trickplay
    C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("trickplay",
                                                    m_strTrickPlayType);
    if (pTrickPlayModule)
    {
        C_TrickPlayConfig cTrickPlayConfig;
        cTrickPlayConfig.m_hLog = m_hLog;
        cTrickPlayConfig.m_pBroadcast = &m_cInputBroadcast;
        cTrickPlayConfig.m_pReader = m_pReader;
        cTrickPlayConfig.m_pEventHandler = this;
        cTrickPlayConfig.m_pTsProvider = m_pTsProvider;
        cTrickPlayConfig.m_pConverter = m_pConverter;
        cTrickPlayConfig.m_pHandler = this;
        cTrickPlayConfig.m_iInitFill = 0;
        m_pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
        ASSERT(m_pTrickPlay);
    }
    else
    {
        throw E_Exception(GEN_ERR, "Module TrickPlay:" + m_strTrickPlayType +
                      " not present");
    }

    m_pTrickPlay->Create();
    // Select the PAT pid TODO
    SelectPid(&m_cPatDecoder, 0x0000, TS_TYPE_NULL);

    // Wait for the first PAT arrival
    m_cEndInit.Protect();
    m_cEndInit.Wait();


}

//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_AsiInput::OnDestroy()
{
    // Destroy the "PAT decoder"
    m_cPatDecoder.Detach();

    //Then the converter and the trickplay
    try
    {
        if(m_pConverter)
        {
            delete m_pConverter;
        }
        if(m_pTrickPlay)
        {
            if(m_pTrickPlay->IsRunning())
                m_pTrickPlay->Stop();
            delete m_pTrickPlay;
        }
    }
    catch(E_Exception e)
    {
        m_cEndInit.Release();
        if (m_pConverter) delete m_pConverter;
        if (m_pTrickPlay) delete m_pTrickPlay;
        throw e;
    }

    m_cEndInit.Release();
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_AsiInput::OnGetAvailablePgrms()
{
  C_List<C_Program> cPgrmList;

  m_cLock.Lock();

  dvbpsi_pat_program_t *pPatProgram =
                        m_cCurrentPat.GetLowLevelPat()->p_first_program;
  while(pPatProgram)
  {
    C_Program* pProgram = new C_Program(pPatProgram->i_number,
                                        pPatProgram->i_number);
    ASSERT(pProgram);
    cPgrmList.PushEnd(pProgram);
    pPatProgram = pPatProgram->p_next;
  }

  m_cLock.UnLock();

  return cPgrmList;
}

//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_AsiInput::OnStartStreaming(C_Broadcast* pBroadcast)
{
    // Lock the demux usage
    m_cDemuxUsageM.Lock();

    // If we have not already Started the demux, do so and wait
    // for the first PAT arrival
    // XXX
#if 0 
    if ( m_iDemuxUsageCount == 0 )
    {
        // Create the trickplay
        m_pTrickPlay->Create();

        // Add a filter for the PAT
        // TODO

        // Wait for the first PAT
    fprintf(stderr, "Waiting for PAT\n"); //XXX nitrox
        m_cEndInit.Wait();
        m_cEndInit.Release();
    }
#endif
    
    fprintf(stderr, "C'est parti\n"); //XXX nitrox
    // Update Demux Counter and unlock
    m_iDemuxUsageCount++;
    m_cDemuxUsageM.UnLock();

    // Get the program
    dvbpsi_pat_program_t *pProgram = 
        m_cCurrentPat.GetProgram(pBroadcast->GetProgram()->GetName().ToInt());

    m_cLock.Lock();


    // Finaly we can laucnh the streaming line
    if ( pProgram )
    {
        C_SyncFifo* pBuffer = 
            new C_SyncFifo(2* pBroadcast->GetChannel()->GetBuffCapacity());
        C_TsStreamer *pStreamer = new C_TsStreamer(m_hLog, pBroadcast,
                                                   m_pTsProvider, pBuffer,
                                                   m_pEventHandler,
                                                   false, false);
        C_TsMux *pMux = new C_TsMux(m_pTsProvider, this, pBuffer);

        try
        {
            u16 iNumber = pBroadcast->GetProgram()->GetName().ToInt();
            pStreamer->Create();
            pMux->Attach();
            pMux->AttachProgram(pProgram->i_number, pProgram->i_pid);

            m_cMuxes.Add(iNumber, pMux);
            m_cStreamers.Add(iNumber, pStreamer);
        }
        catch(E_Exception e)
        {
            delete pStreamer,
            delete pMux;
            throw e;
        }
    }
    else
    {
        throw E_Exception(GEN_ERR, "Program \""+
                          pBroadcast->GetProgram()->GetName() +
                          "\" does not exist");
    }

    m_cLock.UnLock();
    return;
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_AsiInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
    throw E_Exception(GEN_ERR, "Resuming ASI program not available");
    return;
}

//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_AsiInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
    throw E_Exception(GEN_ERR, "Suspending ASI program not available");
    return;
}

//------------------------------------------------------------------------------
// Forward the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_AsiInput::OnForwardStreaming(C_Broadcast* pBroadcast, int speed)
{
    throw E_Exception(GEN_ERR, "Forwarding in ASI program not available");
    return;
}

//------------------------------------------------------------------------------
// Rewind the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_AsiInput::OnRewindStreaming(C_Broadcast* pBroadcast, int speed)
{
    throw E_Exception(GEN_ERR, "Rewinding in ASI program not available");
    return;
}


//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_AsiInput::OnStopStreaming(C_Broadcast* pBroadcast)
{

    m_cLock.Lock();

    //Lock the demux counter and dcrement usage counter
    m_cDemuxUsageM.Lock();
    m_iDemuxUsageCount--;

    // If the usage counter reached 0, we have to remove the PAT filter
    // and suspend the demux
    if ( m_iDemuxUsageCount == 0 )
    {
        // TODO - Remove Selection on PID 0
        m_pTrickPlay->Stop();
    }
    m_cDemuxUsageM.UnLock();


    // Get a pointer to the Mux and the Streamer 
    // and delete them
    u16 iNumber = pBroadcast->GetProgram()->GetName().ToInt();
    C_TsMux *pMux = m_cMuxes.Remove(iNumber);
    ASSERT(pMux);
    C_TsStreamer *pStreamer = m_cStreamers.Remove(iNumber);
    ASSERT(pStreamer);
    m_cLock.UnLock();

    pMux->Detach();
    delete pMux;

    try
    {
        pStreamer->Stop();
    }
    catch(E_Exception e)
    {
        delete pStreamer;
        throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    delete pStreamer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AsiInput::OnUpdateProgram(C_String strProgram,
                                       C_String strFileName, C_String strType)
{
    throw E_Exception(GEN_ERR, "Updating ASI programs not implemented");
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AsiInput::OnDeleteProgram(C_String strProgram)
{
    throw E_Exception(GEN_ERR, "Deleting program not implemented");
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AsiInput::HandleEvent(const C_Event& cEvent)
{
  ASSERT(false);
}


//------------------------------------------------------------------------------
// New version of the PAT
//------------------------------------------------------------------------------
void C_AsiInput::OnDvbPsiPatEvent(int iEvent)
{

  if(iEvent == DVBPSI_EVENT_CURRENT)

  {
    dvbpsi_pat_t *pLLPat = m_pCurrentPat->GetLowLevelPat();

    dvbpsi_pat_program_t* p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE, C_String("New PAT\n") );
    Log(m_hLog, LOG_NOTE, C_String("  transport_stream_id : ")
                                                           + pLLPat->i_ts_id);
    Log(m_hLog, LOG_NOTE, C_String("  version_number      : ")
                                                           + pLLPat->i_version);
    Log(m_hLog, LOG_NOTE, C_String("    | program_number @ [NIT|PMT]_PID"));
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
          C_String(" @ 0x") +p_program->i_pid + C_String(" (")
          + p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }
    Log(m_hLog, LOG_NOTE,   "  active              : " +pLLPat->b_current_next);
    C_DvbPsiPat DiffPatSub(0, 0, false);
    C_DvbPsiPat DiffPatAdd(0, 0, false);
    if(m_pPreviousPat)
    {
      DiffPatSub = *m_pPreviousPat - *m_pCurrentPat;
      DiffPatAdd = *m_pCurrentPat - *m_pPreviousPat;
    }
    else
    {
      DiffPatAdd = *m_pCurrentPat;
    }

    pLLPat = DiffPatSub.GetLowLevelPat();
    p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE,   "\n");
    Log(m_hLog, LOG_NOTE,   "Deleted programs\n");
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
          C_String(" @ 0x") +p_program->i_pid + C_String(" (") +
          p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }
    pLLPat = DiffPatAdd.GetLowLevelPat();
    p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE,   "\n");
    Log(m_hLog, LOG_NOTE,   "Added programs\n");
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
        C_String(" @ 0x") +p_program->i_pid + C_String(" (") +
        p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }

    m_cLock.Lock();
    m_cCurrentPat = *m_pCurrentPat;
    m_cLock.UnLock();

    // Kludge: signal the first PAT arrival.
    m_cEndInit.Protect();
    m_cEndInit.Signal();
    m_cEndInit.Release();
  }
}



//Local private functions
//TODO documentation about them !

void C_AsiInput::OpenDevice()
{
    m_hFd = open (m_strDeviceName.GetString(), O_RDONLY | O_LARGEFILE, 0);
    if ( m_hFd < 0 )
        throw E_Exception(GEN_ERR, "Unable to open ASI device");
}

void C_AsiInput::GetReceiverCapabilities()
{
    if (ioctl (m_hFd, ASI_IOC_RXGETCAP, &m_iCap) < 0)
    {
        throw E_Exception(GEN_ERR,
                "Unable to get the ASI receiver capabilities");
    }
}

int C_AsiInput::GetDeviceType()
{
    struct stat sBuf;
    memset(&sBuf, 0, sizeof(sBuf));

    if (fstat (m_hFd, &sBuf) < 0)
    {
        throw E_Exception(GEN_ERR,
                "Unable to get the file status for ASI receiver");
    }
    return 1 + (sBuf.st_rdev & 0x00ff);
}

unsigned int C_AsiInput::GetBufferSize(int iDeviceType)
{
    unsigned int iBufSize;
    int iName[4] = {CTL_DEV, DEV_ASI, iDeviceType, DEV_ASI_BUFSIZE};
    unsigned int iLen = sizeof (iBufSize);
    if (sysctl (iName, sizeof (iName) / sizeof (int), &iBufSize,
                                                          &iLen, NULL, 0) < 0)
    {
        throw E_Exception(GEN_ERR, "Unable to get the ASI receiver "
                                            "buffer size");
    }
    return iBufSize;
}

int C_AsiInput::GetReceiverOperatingMode(int iDeviceType)
{
    int iMode;
    int iName[4] = {CTL_DEV, DEV_ASI, iDeviceType, DEV_ASI_MODE};
    if (m_iCap & ASI_CAP_RX_SYNC)
    {
        iName[3] = DEV_ASI_MODE;
        unsigned int iLen = sizeof (iMode);
        if (sysctl (iName, sizeof (iName) / sizeof (int), &iMode, &iLen,
                                                                NULL, 0) < 0)
        {
            throw E_Exception(GEN_ERR, "Unable to get the ASI receiver "
                                                "operating mode");
        } 
    }
    else
    {
        iMode = ASI_CTL_RX_MODE_RAW;
    }
    switch(iMode)
    {
        case ASI_CTL_RX_MODE_RAW:
            LogDbg(m_hLog, "Receiving in raw mode.");
            break;
        case ASI_CTL_RX_MODE_188:
            LogDbg(m_hLog, "Synchronizing on 188-byte packets.");
            break;
        case ASI_CTL_RX_MODE_204:
            LogDbg(m_hLog, "Synchronizing on 204-byte packets.");
            break;
        case ASI_CTL_RX_MODE_AUTO:
            LogDbg(m_hLog, "Synchronizing on detected packet size.");
            break;
        case ASI_CTL_RX_MODE_AUTOMAKE188:
            LogDbg(m_hLog, "Synchronizing on 204-byte packets (stripping).");
            break;
        default:
            Log(m_hLog, LOG_WARN, "Receiving in unknown mode.");
            break;
    }

    return iMode;
}

int C_AsiInput::GetPacketTimestampMode(int iDeviceType)
{
    int iTimeStamps;
    int iName[]={CTL_DEV, DEV_ASI, iDeviceType, DEV_ASI_TIMESTAMPS};
    if (m_iCap & ASI_CAP_RX_TIMESTAMPS)
    {
        unsigned int iLen = sizeof (iTimeStamps);
        if (sysctl (iName, sizeof (iName) / sizeof (int),
                                        &iTimeStamps, &iLen, NULL, 0) < 0)
        {
            throw E_Exception(GEN_ERR, "Unable to get the packet"
                                            "timestamping mode");
        }
    }
    else
    {
        iTimeStamps = 0;
    }

    if ( iTimeStamps == 1 )
        LogDbg(m_hLog, "Appending an 8B timestamp to each packet");
    else if ( iTimeStamps == 2 )
        LogDbg(m_hLog, "Prepending an 8B timestamp to each packet");

    return iTimeStamps;
}

void C_AsiInput::SetInvSynchro(int iInvSynchro)
{
    if ( iInvSynchro < 0 ) return;

    switch (iInvSynchro)
    {
        case 0:
            LogDbg(m_hLog, "Synchronizing on 0x47 packets");
            break;
        case 1:
            if (!(m_iCap & ASI_CAP_RX_INVSYNC))
            {
                throw E_Exception(GEN_ERR, "Synchronisation on 0xb8 not "
                                  "supported");
            }
            LogDbg(m_hLog, "Synchronizing on both 0xb8 and 0x47 packets");
            break;
        default:
            throw E_Exception(GEN_ERR, "Invalid 0xb8 packet synchro mode");
            break;
    }

    if (ioctl (m_hFd, ASI_IOC_RXSETINVSYNC, &iInvSynchro) < 0)
    {
        throw E_Exception(GEN_ERR, "Unable to set the 0xb8 packet synchro"
                                   " mode");
    }

    return;
}


void C_AsiInput::SetDoubleSynchro(int iDoubleSynchro)
{
    if ( iDoubleSynchro < 0 ) return;
    switch(iDoubleSynchro)
    {
        case 0:
            LogDbg(m_hLog, "Disabling double packet synchronization.");
            break;
        case 1:
            if (!(m_iCap & ASI_CAP_RX_DSYNC))
            {
                throw E_Exception(GEN_ERR, "Double packet synchronization "
                                  "not supported");
            }
            else
            {
                LogDbg(m_hLog, "Enabling double packet synchronization.");
            }
            break;
        default:
            throw E_Exception(GEN_ERR, "Invalid double packet synchronization"
                              " mode");
            break;
    }

    if (ioctl (m_hFd, ASI_IOC_RXSETDSYNC, &iDoubleSynchro) < 0)
    {
        throw E_Exception(GEN_ERR, "Unable to set the double packet "
                          "synchronization mode");
    }
}

void C_AsiInput::SetPIDCounter(int iPid, int iMode)
{
    if (iPid >= 0)
    {
        if ( iMode == ASI_CTL_RX_MODE_RAW )
        {
            throw E_Exception(GEN_ERR, "PID counter not supported in"
                              " raw mode");
        }
        if ( m_iCap & ASI_CAP_RX_PIDCOUNTER )
        {
            if ( ioctl (m_hFd, ASI_IOC_RXSETPID0, &iPid) < 0 )
            {
                throw E_Exception(GEN_ERR, "Unable to set the PID counter");
            }
            else
            {
                LogDbg(m_hLog, "Counting PID " + iPid);
            }
        }
        else
        {
            throw E_Exception(GEN_ERR, "PID counter not supported");
        }
    }
    else
    {
        LogDbg(m_hLog, "Ignoring the PID counter");
    }
}

void C_AsiInput::SetBufferSize(int iDeviceType, int iBuffSize)
{
    int iName[]={CTL_DEV, DEV_ASI, iDeviceType, DEV_ASI_BUFSIZE};
    int iLen = sizeof (iBuffSize);

    if (sysctl (iName, sizeof (iName) / sizeof (int), NULL, NULL,
                &iBuffSize, iLen) < 0 )
    {
        perror("tarass");
        throw E_Exception(GEN_ERR, "Unable to set the buffer size\n");
    }
    else
    {
        LogDbg(m_hLog, "Buffer size set to "+iBuffSize);
    }
}
