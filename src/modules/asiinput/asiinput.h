/*******************************************************************************
* asiinput.h: ASI Input class definition
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id: asiinput.h,v 1.3 2003/09/21 01:13:01 nitrox Exp $
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _ASI_INPUT_H_
#define _ASI_INPUT_H_

//------------------------------------------------------------------------------
// C_AsiInput class
//------------------------------------------------------------------------------
class C_AsiInput : public C_Input,
                   public C_EventHandler,
                   public C_TsDemux,
                   public I_DvbPsiPatHandler
{
public:
    C_AsiInput(C_Module* pModule, const C_String& strName);
    ~C_AsiInput();

    virtual C_String GetType() {return C_String("asi");}
protected:
    // Init/termination methods
    virtual void OnInit();
    virtual void OnDestroy();

    // Selection of the pgrms to broadcast
    virtual void OnStartStreaming(C_Broadcast* pBroadcast);
    virtual void OnResumeStreaming(C_Broadcast* pBroadcast);
    virtual void OnSuspendStreaming(C_Broadcast* pBroadcast);
    virtual void OnForwardStreaming(C_Broadcast* pBroadcast, int speed);
    virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed);
    virtual void OnStopStreaming(C_Broadcast* pBroadcast);
    virtual void OnUpdateProgram(C_String strProgram,C_String strFileName, C_String strType);
    virtual void OnDeleteProgram(C_String strProgram);
    virtual C_List<C_Program> OnGetAvailablePgrms();

    // The input is the event handler of its converter
    virtual void HandleEvent(const C_Event& cEvent);

    // PAT event handler
    void C_AsiInput::OnDvbPsiPatEvent(int iEvent);
private:
    void OpenDevice();
    void GetReceiverCapabilities();
    int GetDeviceType();
    unsigned int GetBufferSize(int iDeviceType);
    int GetReceiverOperatingMode(int iDeviceType);
    int GetPacketTimestampMode(int iDeviceType);
    void SetInvSynchro(int iInvSynchro);
    void SetDoubleSynchro(int iDoubleSynchro);
    void SetPIDCounter(int iPid, int iMode);
    void SetBufferSize(int, int);
    C_String m_strDeviceName;             /* Name of the ASI receiver device */
    int m_hFd;                         /* Handle for the ASI receiver device */
    int m_iCap;                     /* Capacities of the ASI receiver device */

    // Programs names
    // the program ID is m_vProgramNames.Find(strName) + 1
    C_Vector<C_String> m_vProgramNames;

    //Kludge: signal the first PAT arrival
    C_Condition m_cEndInit;
    int m_iDemuxUsageCount;
    C_Mutex m_cDemuxUsageM;

    // Only one netlist for all the programs
    C_NetList m_cTsProvider;

    // Input stream (demux)
    C_Program m_cInputProgram;
    C_Broadcast m_cInputBroadcast;
    C_MpegReader *m_pReader;
    C_MpegConverter *m_pConverter;

    // TrickPlay Module
    C_TrickPlay *m_pTrickPlay;
    C_String m_strTrickPlayType;

    // PAT stuffs
    C_DvbPsiPatDecoder m_cPatDecoder;
    C_Mutex m_cLock;
    C_DvbPsiPat m_cCurrentPat;

    // Muxes and streamers
    C_HashTable<u16, C_TsMux> m_cMuxes;
    C_HashTable<u16, C_TsStreamer> m_cStreamers;
};

// Declaration and implementation of C_AsiInputModule
DECLARE_MODULE(Asi, Input, "asi", const C_String&);

#else
#error "Multiple inclusins of XXXinput.h"
#endif
