/*******************************************************************************
* pvrreader.cpp: Reading from an Hauppauge PVR board
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <sys/poll.h>
#include <sys/types.h>
#include <fcntl.h>

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "pvrreader.h"

#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

#include "videodev2.h"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_PvrMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_pvrreader(handle hLog)
{
  return new C_PvrMpegReaderModule(hLog);
}
#endif

/* ivtv specific ioctls */
#define IVTV_IOC_G_CODEC    0xFFEE7703
#define IVTV_IOC_S_CODEC    0xFFEE7704


/* for use with IVTV_IOC_G_CODEC and IVTV_IOC_S_CODEC */

struct ivtv_ioctl_codec {
        int aspect;
        int audio_bitmask;
        int bframes;
        int bitrate_mode;
        int bitrate;
        int bitrate_peak;
        int dnr_mode;
        int dnr_spatial;
        int dnr_temporal;
        int dnr_type;
        int framerate;
        int framespergop;
        int gop_closure;
        int pulldown;
        int stream_type;
};

int xioctl(int fd, int cmd, void *arg)
{
    int rc;
    rc = ioctl(fd,cmd,arg);
/*    printf("ioctl: %u",cmd);
    printf(" : %s\n",(rc == 0) ? "ok" : strerror(errno));*/
    return rc;
}


E_Pvr::E_Pvr(const C_String& strMsg) : E_Exception(GEN_ERR, strMsg)
{
}

/*******************************************************************************
* C_PvrMpegReader
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_PvrMpegReader::C_PvrMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast)
{
  m_pBroadcast = pBroadcast;
  m_strDeviceName = pBroadcast->GetOption("pvr_device");
  
  C_String strNorm = pBroadcast->GetOption("pvr_norm");

  if (strNorm=="secam")
    m_iNorm = V4L2_STD_SECAM;
  else if (strNorm=="ntsc")
    m_iNorm = V4L2_STD_NTSC;
  else m_iNorm = V4L2_STD_PAL; /* Default */
  
  m_iFrequency = pBroadcast->GetOption("pvr_frequency").ToInt() * 16 / 1000;

  m_iFrameRate = pBroadcast->GetOption("pvr_framerate").ToInt();

  m_strSize = pBroadcast->GetOption("pvr_size");

  m_iBitRate = pBroadcast->GetOption("pvr_bitrate").ToInt() * 1000;
  
  m_iMaxBitRate = pBroadcast->GetOption("pvr_paxbitrate").ToInt() * 1000;

  m_iBitRateMode = (pBroadcast->GetOption("pvr_bitratemode")=="vbr")?0:1;

  m_iInputNumber = pBroadcast->GetOption("pvr_inputnumber").ToInt();

  if(m_strSize == "")
  {
    m_iWidth = (m_iHeight = 0);
  } else
  if(m_strSize == "subqcif")
  {
    m_iWidth = 128; m_iHeight = 96;
  } else
  if(m_strSize == "qsif")
  {
    m_iWidth = 160; m_iHeight = 120;
  } else
  if(m_strSize == "qcif")
  {
    m_iWidth = 176; m_iHeight = 144;
  } else
  if(m_strSize == "sif")
  {
    m_iWidth = 320; m_iHeight = 240;
  } else
  if(m_strSize == "cif")
  {
    m_iWidth = 352; m_iHeight = 288;
  } else
  if(m_strSize == "vga")
  {
    m_iWidth = 640; m_iHeight = 480;
  }
  else
  {
    m_iWidth = (m_strSize.SubString(0, (m_strSize.Find('x')))).ToInt();
    m_iHeight = (m_strSize.SubString(m_strSize.Find('x')+1,
                                          m_strSize.Length())).ToInt();
  }
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_PvrMpegReader::Init()
{
  struct v4l2_format sVfmt;
  struct v4l2_frequency sVfreq;
  struct v4l2_capability sCap;
  struct v4l2_input sInput;
  struct ivtv_ioctl_codec sCodec;

  if ( (m_hFd = open(m_strDeviceName.GetString(), O_RDWR)) < 0)
  {
    throw E_Pvr(C_String("Could not open device ") + m_strDeviceName);
    return;
  }

  /* Set pal, secam or ntsc */
  if (xioctl(m_hFd, VIDIOC_S_STD, &m_iNorm) < 0)
  {
    Log(m_hLog, LOG_NOTE, "Warning : Norm could not \
                                      be set (VIDIOC_S_STD) ! \n");
  }

 
  
  /* Get picture format */
  if( xioctl(m_hFd, VIDIOC_G_FMT, &sVfmt) < 0 )
  {
    Log(m_hLog, LOG_NOTE, "Warning : could not get picture format \
                                             (VIDIOC_G_FMT) ! \n");
  }
  else
  {
    /* Set picture size */
    sVfmt.fmt.pix.width = m_iWidth;
    sVfmt.fmt.pix.height = m_iHeight;
    if( xioctl(m_hFd, VIDIOC_S_FMT, &sVfmt ) < 0 )
    {
      Log(m_hLog, LOG_NOTE, "Warning : Picture size could not be set \
                                 (VIDIOC_S_FMT) ! \n");
    }
  }

  /* Frequency */
  sVfreq.tuner = 0;
  if( xioctl(m_hFd, VIDIOC_G_FREQUENCY, &sVfreq) < 0 )
  {
    Log(m_hLog, LOG_NOTE, "Warning : could not get frequency parameters \
                                  (VIDIOC_G_FREQUENCY) ! \n");
  }
  else
  {
    sVfreq.frequency = m_iFrequency;
    if( xioctl(m_hFd, VIDIOC_S_FREQUENCY, &sVfreq) < 0 )
    {
      Log(m_hLog, LOG_NOTE, "Warning : could not set frequency \
                                  (VIDIOC_S_FREQUENCY) !\n");
    }
  }

  /* Select input source : consosite, tuner, S-video ... */
  if (xioctl(m_hFd, VIDIOC_S_INPUT, &m_iInputNumber) < 0)
  {
    Log(m_hLog, LOG_NOTE, "Warning : Input could not \
                                      be set (VIDIOC_S_INPUT) ! \n");
  }

 

  /* codec parameters */
  if( xioctl(m_hFd, IVTV_IOC_G_CODEC, &sCodec) < 0 )
  {
     Log(m_hLog, LOG_NOTE, "Warning : could not get codec \
                                  (IVTV_IOC_G_CODEC) !\n");
  }
  else
  {
    switch(m_iFrameRate)
    {
      case 30:
        sCodec.framerate = 0;
       break;

      case 25:
        sCodec.framerate = 1;
       break;

      default:
         Log(m_hLog, LOG_NOTE,
               "Warning : invalid framerate, reverting to 25");
         m_iFrameRate = 25;
         sCodec.framerate = 1;
        break;
    }

    sCodec.bitrate = m_iBitRate;
    sCodec.bitrate_peak = m_iMaxBitRate;
    sCodec.bitrate_mode = m_iBitRateMode;
    if( xioctl(m_hFd, IVTV_IOC_S_CODEC, &sCodec ) < 0 )
      Log(m_hLog, LOG_NOTE, "Warning : could not set codec framerate \n");
  }

  if (xioctl(m_hFd, VIDIOC_QUERYCAP, &sCap) < 0)
    fprintf(stderr, "Warning : VIDIOC_QUERYCAP failed\n");
  else
    fprintf(stderr,"\nInfo : card %s found !\n", sCap.card);
    
  fprintf(stderr,"Info : ");
  sInput.index = 0;
  while (ioctl(m_hFd, VIDIOC_ENUMINPUT, &sInput) >= 0)
  {
    fprintf(stderr,"%sFound %s (Input number %u)  %s\n",
                    sInput.index?"       ":"", sInput.name,
                    sInput.index,
                    ((int)sInput.index==(int)m_iInputNumber)?"[selected]":"");
                    sInput.index++;
  }

  fprintf(stderr, "Info : Window %ux%u %u fps, %s\n",
                    m_iWidth, m_iHeight, m_iFrameRate,
                    (m_iNorm==V4L2_STD_SECAM)?"SECAM":
                    (m_iNorm==V4L2_STD_NTSC)?"NTSC":"PAL");
  fprintf(stderr, "Info: Encoding at %u kbps, %s, max %u kbps\n\n",
                m_iBitRate / 1000,
                m_iBitRateMode?"CBR":"VBR",
                m_iMaxBitRate / 1000);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_PvrMpegReader::Close()
{
  close(m_hFd);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_PvrMpegReader::Read(byte* pBuff, s64 iSize)
{
  s64 iRc;
/*
  struct pollfd pfd[1];
  s64 iRc;

  pfd[0].fd=m_hFd;
  pfd[0].events=POLLIN;

  pBuff[0]=0;
  
  if(poll(pfd, 1, 10000))
  {
    if(pfd[0].revents & POLLIN)
    {
      iRc=read(m_hFd, pBuff, 1000);
    }
    else
    {
      Log(m_hLog, LOG_NOTE, "Time out !\n");
      return MPEG_STREAMERROR;
    }
  }
  else
  {
    Log(m_hLog, LOG_NOTE, "Time out !\n");
    return MPEG_STREAMERROR;
  }

  return iRc;*/

// VLCCCCCCCC

/*  int iRc;
  struct timeval timeout;
  fd_set fds;
 
  FD_ZERO( &fds );
  FD_SET( m_hFd, &fds );
  timeout.tv_sec = 0;
  timeout.tv_usec = 500000;

  while( !( iRc=select(m_hFd+1, &fds,
                         NULL, NULL, &timeout)))
  {
    FD_ZERO( &fds );
    FD_SET( m_hFd, &fds );
    timeout.tv_sec = 0;
    timeout.tv_usec = 500000;
  }

  if( iRc < 0 )
  {
    Log(m_hLog, LOG_NOTE, "select error ");
    return MPEG_STREAMERROR;
  }
*/
 iRc=read(m_hFd, pBuff, iSize);
 return iRc;
 
}

int C_PvrMpegReader::GetFrame(byte **, int)
{
  return 0;
}

//int C_PvrMpegReader::GetAudioFD()
//{
//  return m_iAudioFD;
//}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_PvrMpegReader::Seek(s64, s64)
{
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_PvrMpegReader::Size()
{
  ASSERT(false);
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_PvrMpegReader::GetPos()
{
  return 0;
}
