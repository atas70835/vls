/*******************************************************************************
* dvdreader.cpp: DVD reader
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: dvdreader.cpp,v 1.9 2003/10/30 21:03:50 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*------------------------------------------------------------------------------
* DVD reader mainly inspired from the libdvdread example "play_title"
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../core/iso_lang.h"

#include <dvdread/dvd_reader.h>
#include <dvdread/ifo_types.h>
#include <dvdread/ifo_read.h>
#include <dvdread/nav_read.h>
#include <dvdread/nav_print.h>

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/psi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#else
#   include "src/dvbpsi.h"
#   include "src/psi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#endif

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/streamdescr.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/dvbpsi.h"
#include "../../mpeg/ps2ts.h"
#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "dvdreader.h"

#include "../ps2ts/ps2ts.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_DvdMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_dvdreader(handle hLog)
{
  return new C_DvdMpegReaderModule(hLog);
}
#endif


/*******************************************************************************
* E_Dvd
********************************************************************************
*
*******************************************************************************/
E_Dvd::E_Dvd(const C_String& strMsg) : E_Exception(GEN_ERR, strMsg)
{
}


/*******************************************************************************
* C_DvdReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DvdMpegReader::C_DvdMpegReader(C_Module* pModule, C_Broadcast* pBroadcast) :
                            C_MpegReader(pModule, pBroadcast),
                            m_strDevice(pBroadcast->GetOption("device"))
{
  if(pBroadcast->GetOption("loop") == "1")
    m_bLoop = true;
  else
    m_bLoop = false;

  C_String strOption = pBroadcast->GetOption("dvdtitle");
  fprintf(stderr, "title == \"%s\"\n", strOption.GetString());
  if(strOption.Length() != 0)
    m_iTitleId = strOption.ToInt() - 1;
  else
    m_iTitleId = 0;

  strOption = pBroadcast->GetOption("dvdchapter");
  fprintf(stderr, "chapter == \"%s\"\n", strOption.GetString());
  if(strOption.Length() != 0)
    m_iChapId = strOption.ToInt() - 1;
  else
    m_iChapId = 0;

  strOption = pBroadcast->GetOption("dvdangle");
  fprintf(stderr, "angle == \"%s\"\n", strOption.GetString());
  if(strOption.Length() != 0)
    m_iAngle = strOption.ToInt() - 1;
  else
    m_iAngle = 0;

  m_pPgrmDescriptor = NULL;

  m_pDvd = NULL;
  m_pTitle = NULL;
  m_pVmgFile = NULL;
  m_pVtsFile = NULL;
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_DvdMpegReader::Init()
{
  m_pPgrmDescriptor =
                ((C_Ps2TsMpegConverter*)m_pConverter)->GetPgrmDescriptor();

  m_pDvd = DVDOpen(m_strDevice.GetString());
  if(!m_pDvd)
    throw E_Dvd("Unable to open device \"" + m_strDevice + "\"");

  m_pVmgFile = ifoOpen(m_pDvd, 0);
  if(!m_pVmgFile)
    throw E_Dvd("Unable to open the IFO file");

  tt_srpt_t *pTtSrpt = m_pVmgFile->tt_srpt;

  if((m_iTitleId < 0) || (m_iTitleId >= pTtSrpt->nr_of_srpts))
    throw E_Dvd(C_String("Invalid title ") + (m_iTitleId + 1));

  if((m_iChapId < 0) || (m_iChapId >= pTtSrpt->title[m_iTitleId].nr_of_ptts))
    throw E_Dvd(C_String("Invalid chapter ") + (m_iChapId + 1));

  if((m_iAngle < 0) || (m_iAngle >= pTtSrpt->title[m_iTitleId].nr_of_angles))
  {
    throw E_Dvd(C_String("Invalid angle ") + (m_iAngle + 1));
  }

  m_pVtsFile = ifoOpen(m_pDvd, pTtSrpt->title[m_iTitleId].title_set_nr);
  if(!m_pVtsFile)
  {
    throw E_Dvd(C_String("Unable to open the IFO file for title") +
                (m_iTitleId + 1));
  }

  int iTtn = pTtSrpt->title[m_iTitleId].vts_ttn;
  vts_ptt_srpt_t* pVtsPttSrpt = m_pVtsFile->vts_ptt_srpt;
  int iPgcId = pVtsPttSrpt->title[iTtn - 1].ptt[0].pgcn;
  int iPgn = pVtsPttSrpt->title[iTtn - 1].ptt[m_iChapId].pgn;
  m_pCurPgc = m_pVtsFile->vts_pgcit->pgci_srp[iPgcId - 1].pgc;
  m_iStartCell = m_iNextCell = m_pCurPgc->program_map[iPgn - 1] - 1;
  iPgn = pVtsPttSrpt->title[iTtn - 1].ptt[0].pgn;
  m_iFirstCell = m_pCurPgc->program_map[iPgn - 1] - 1;

  // Get program's description
  // Video
  m_pPgrmDescriptor->AddES(0xe0);
  // Audio
  for(unsigned int ui = 0; ui < 8; ui++)
  {
    /* Calm the compiler with initialisation */
    /* TODO : check if it is a real pb */
    C_ElementDescriptor *pElement = 0; 
    u16 uiCtrl = m_pCurPgc->audio_control[ui];
    printf("audio : %04x\n", uiCtrl);
    if(uiCtrl >> 15)
    {
      switch(m_pVtsFile->vtsi_mat->vts_audio_attr[ui].audio_format)
      {
      case 0:
        // ac3
        pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_PRIV_ID_MIN_AC3 + ((uiCtrl >> 8) & 0x07));
        break;
      case 1:
        ASSERT(false);
        break;
      case 2:
        // mpeg 1
        pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_ID_MIN_AUDIO + ((uiCtrl >> 8) & 0x07));
        break;
      case 3:
        // mpeg 2
        pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_ID_MIN_AUDIO + ((uiCtrl >> 8) & 0x07));
        break;
      case 4:
        // lpcm
        pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_PRIV_ID_MIN_LPCM + ((uiCtrl >> 8) & 0x07));
        break;
      case 5:
        ASSERT(false);
        break;
      case 6:
        // dts
        // FIXME: the stream type is not specified and don't know the dts
        // specification. So DTS is considered as ac3 until the fix.
        pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_PRIV_ID_MIN_AC3 + ((uiCtrl >> 8) & 0x07));
        break;
      default:
        ASSERT(false);
        break;
      }
      u8 iso1Lang[2];
      iso1Lang[0] = m_pVtsFile->vtsi_mat->vts_audio_attr[ui].lang_code >> 8;
      iso1Lang[1] = m_pVtsFile->vtsi_mat->vts_audio_attr[ui].lang_code;
      iso639_lang_t *pLang = GetLang_1((char*)iso1Lang);
      if(pLang)
        pElement->AddLanguageCode(pLang->psz_iso639_2T);
    }
  }
  // Subtitles
  for(unsigned int ui = 0; ui < 32; ui++)
  {
    C_ElementDescriptor *pElement;
    u32 uiCtrl = m_pCurPgc->subp_control[ui];
    printf("subtitle : %08x\n", uiCtrl);
    if(uiCtrl >> 31)
    {
      pElement = m_pPgrmDescriptor->AddES(
                                PES_ID_PRIVATE_1,
                                PES_PRIV_ID_MIN_SPU + ((uiCtrl >> 8) & 0x1f));
      u8 iso1Lang[2];
      iso1Lang[0] = m_pVtsFile->vtsi_mat->vts_subp_attr[ui].lang_code >> 8;
      iso1Lang[1] = m_pVtsFile->vtsi_mat->vts_subp_attr[ui].lang_code;
      iso639_lang_t *pLang = GetLang_1((char*)iso1Lang);
      if(pLang)
        pElement->AddLanguageCode(pLang->psz_iso639_2T);
    }
  }

  m_pTitle = DVDOpenFile(m_pDvd, pTtSrpt->title[m_iTitleId].title_set_nr,
                         DVD_READ_TITLE_VOBS);
  if(!m_pTitle)
  {
    throw E_Dvd(C_String("Unable to open the VOB files") +
                (m_iTitleId + 1));
  }

  m_bJumpCell = true;

  int iRc = ReadBlock();
  if(iRc <= 0)
    throw E_Dvd("Unable to read the first block");
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvdMpegReader::Close()
{
  if(m_pVtsFile)
    ifoClose(m_pVtsFile);
  if(m_pVmgFile)
    ifoClose(m_pVmgFile);
  if(m_pTitle)
    DVDCloseFile(m_pTitle);
  if(m_pDvd)
    DVDClose(m_pDvd);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvdMpegReader::Read(byte* pBuff, s64 iSize)
{
  ASSERT(iSize <= DVD_VIDEO_LB_LEN);

  if(!m_uiByteLeft)
  {
    s64 iRc = ReadBlock();
    if(iRc <= 0)
      return iRc;
  }

  if(m_uiByteLeft >= (u64)iSize)
  {
    memcpy(pBuff, &m_bBlock[DVD_VIDEO_LB_LEN - m_uiByteLeft], iSize);
    m_uiByteLeft -= iSize;
    return iSize;
  }
  else
  {
    memcpy(pBuff,
           &m_bBlock[DVD_VIDEO_LB_LEN - m_uiByteLeft],
           m_uiByteLeft);
    u64 uiRead = m_uiByteLeft;

    s64 iRc = ReadBlock();
    if((iRc == MPEG_ENDOFSTREAM) || (iRc == 0))
      return uiRead;
    else if(iRc < 0)
      return iRc;

    memcpy(pBuff + uiRead,
           &m_bBlock[DVD_VIDEO_LB_LEN - m_uiByteLeft],
           iSize - uiRead);
    m_uiByteLeft += uiRead - iSize;
    return iSize;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvdMpegReader::Seek(s64 iOffset, s64 bStartPos)
{
  s64 iRc = 0;
   
  switch(bStartPos)
  {
  case FILE_SEEK_CURRENT:
    if(iOffset > 0)
    {
      // Although we read only the nav blocks, this code is very slow for
      // Big seeks.
      // Could it be improved ?
      u64 uiRead = (iOffset + DVD_VIDEO_LB_LEN - m_uiByteLeft) /
                            DVD_VIDEO_LB_LEN;
      u64 uiByteLeft = DVD_VIDEO_LB_LEN -
                                ((iOffset + DVD_VIDEO_LB_LEN - m_uiByteLeft) %
                                 DVD_VIDEO_LB_LEN);

      if(uiRead)
      {
        for(u64 ui = 0; ui < uiRead; ui++)
        {
          iRc = ReadBlock(ui == uiRead - 1);
          if(iRc <= 0)
            throw E_Dvd("Could not seek in DVD");
        }
      }

      m_uiByteLeft = uiByteLeft;
    }
    else if(iOffset < 0)
    {
      // Implementation not needed
      ASSERT(false);
    }
    break;
  case FILE_SEEK_BEGIN:
    m_iNextCell = m_iStartCell;
    m_bJumpCell = true;
    iRc = ReadBlock();
    if(iRc <= 0)
      throw E_Dvd("Could not seek in DVD");
    iRc = Seek(iOffset, FILE_SEEK_CURRENT);
    break;
  case FILE_SEEK_END:
    // Implementation not needed
    ASSERT(false);
    break;
#ifdef DEBUG
  default:
    ASSERT(false);
    break;
#endif
  }

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvdMpegReader::Size()
{
  ASSERT(false);
  return 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvdMpegReader::ReadBlock(bool bFill)
{
  // We have to read a new cell
  if(m_bJumpCell)
  {
    if(m_iNextCell >= m_pCurPgc->nr_of_cells)
    {
      if(m_bLoop)
      {
        m_iNextCell = m_iFirstCell;
        m_bDiscontinuity = true;
      }
      else
      {
        return MPEG_ENDOFSTREAM;
      }
    }

    m_iCurCell = m_iNextCell;

    // Check if we're entering an angle block
    if(m_pCurPgc->cell_playback[m_iCurCell].block_type ==
                                                        BLOCK_TYPE_ANGLE_BLOCK)
    {
      int i;

      m_iCurCell += m_iAngle;
      for(i = 0;;i++)
      {
        if(m_pCurPgc->cell_playback[m_iCurCell + i].block_mode ==
                                                        BLOCK_MODE_LAST_CELL)
        {
          m_iNextCell = m_iCurCell + i + 1;
          break;
        }
      }
    }
    else
    {
      m_iNextCell = m_iCurCell + 1;
    }

    m_uiCurBlock = m_pCurPgc->cell_playback[m_iCurCell].first_sector;
    m_bReadNav = true;
    m_bJumpCell = false;
  }

  // We have to read a nav block
  if(m_bReadNav)
  {
    s64 iLen = DVDReadBlocks(m_pTitle, m_uiCurBlock, 1, m_bBlock);
    if(iLen != 1)
      return MPEG_STREAMERROR;
    m_bReadNav = false;
    ASSERT((m_bBlock[41] == 0xbf) && (m_bBlock[1027] == 0xbf));

    dsi_t sDsiPack;
    navRead_DSI(&sDsiPack, &(m_bBlock[DSI_START_BYTE]));
    ASSERT(m_uiCurBlock == sDsiPack.dsi_gi.nv_pck_lbn);

    u64 uiNextIlvuStart;
    uiNextIlvuStart = m_uiCurBlock + sDsiPack.sml_agli.data[m_iAngle].address;
    m_uiBlockLeft = sDsiPack.dsi_gi.vobu_ea;

    if(sDsiPack.vobu_sri.next_vobu != SRI_END_OF_CELL)
    {
      m_uiNextVobu = m_uiCurBlock +
                     (sDsiPack.vobu_sri.next_vobu & 0x7fffffff );
    }
    else
    {
      m_uiNextVobu = m_uiCurBlock + m_uiBlockLeft + 1;
    }

    m_uiCurBlock++;
  }

  // Fill the buffer if needed
  if(bFill)
  {
    s64 iLen = DVDReadBlocks(m_pTitle, m_uiCurBlock, 1, m_bBlock);
    if(iLen != 1)
      return MPEG_STREAMERROR;
  }

  // What to do next
  if(--m_uiBlockLeft)
  {
    m_uiCurBlock++;
  }
  else
  {
    if(m_uiNextVobu >= m_pCurPgc->cell_playback[m_iCurCell].last_sector)
    {
      m_bJumpCell = true;
    }
    else
    {
      m_uiCurBlock = m_uiNextVobu;
      m_bReadNav = true;
    }
  }

  m_uiByteLeft = DVD_VIDEO_LB_LEN;
  return (s64)1;
}

s64 C_DvdMpegReader::GetPos()
{
  ASSERT(false);
  return 0;
}
