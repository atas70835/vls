/*******************************************************************************
* ps2ts.h: threaded PS to TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: ps2ts.h,v 1.5 2003/08/05 18:03:52 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _PS2TS_CONVERTER_H_
#define _PS2TS_CONVERTER_H_


//------------------------------------------------------------------------------
// C_Ps2TsMpegConverterConfig class
//------------------------------------------------------------------------------
// build parameters of C_Ps2TsMpegConverter
//------------------------------------------------------------------------------
/*class C_Ps2TsMpegConverterConfig : public C_MpegConverterConfig
{
public:
  unsigned int          m_iMpegVersion;
  bool                  m_bPreParse;
};*/


//------------------------------------------------------------------------------
// C_Ps2TsMpegConverter class
//------------------------------------------------------------------------------
// Mpeg 1/2 PS -> Mpeg 2 TS
//------------------------------------------------------------------------------
class C_Ps2TsMpegConverter : public C_MpegConverter
{
public:
  C_Ps2TsMpegConverter(C_Module* pModule, C_MpegConverterConfig& cConfig);

  // It's a virtual method to avoid errors during libraries loading.
  // In the future there will be C_Ps2AnyMpegConverter and the method
  // will be virtual too.
  virtual C_ProgramDescriptor* GetPgrmDescriptor();
  virtual void InitWork();

  virtual s64 GetNextTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip = 0);
  virtual s64 GetPrevTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip = 0);

private:
  C_Ps2Ts<C_MpegReader, C_NetList> m_cConverter;

  bool m_bPreParse;
};


// Declaration and implementation of C_Ps2TsMpegConverterModule
DECLARE_MODULE(Ps2Ts, MpegConverter, "ps2ts", C_MpegConverterConfig&);


#else
#error "Multiple inclusions of ps2ts.h"
#endif

