/*******************************************************************************
* v4lreader.cpp: Reading from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: v4lreader.cpp,v 1.22 2003/10/28 23:48:45 tooney Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <sys/poll.h>
#include <sys/types.h>
#include <fcntl.h>

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "v4lreader.h"

#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#include <avcodec.h> //for palette
}
#endif

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_v4lMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_v4lreader(handle hLog)
{
  return new C_v4lMpegReaderModule(hLog);
}
#endif


E_v4l::E_v4l(const C_String& strMsg) : E_Exception(GEN_ERR, strMsg)
{
}

/*******************************************************************************
* C_v4lMpegReader
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_v4lMpegReader::C_v4lMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast)
{
  m_pBroadcast = pBroadcast;
  m_strDeviceName = pBroadcast->GetOption("v4l_device");
  m_bMute = pBroadcast->GetOption("v4l_mute") == "true" ? true : false;
  m_strAudioDeviceName = pBroadcast->GetOption("v4l_audiodevice");
  
  m_iCurrentChannel = pBroadcast->GetOption("v4l_channel").ToInt();
  if (m_iCurrentChannel<0) m_iCurrentChannel = 0;

  C_String strSize = pBroadcast->GetOption("v4l_size").ToLower();

  if(strSize == "")
  {
    m_iWidth = (m_iHeight = 0);
  } else
  if(strSize == "subqcif")
  {
    m_iWidth = 128; m_iHeight = 96;
  } else
  if(strSize == "qsif")
  {
    m_iWidth = 160; m_iHeight = 120;
  } else
  if(strSize == "qcif")
  {
    m_iWidth = 176; m_iHeight = 144;
  } else
  if(strSize == "sif")
  {
    m_iWidth = 320; m_iHeight = 240;
  } else
  if(strSize == "cif")
  {
    m_iWidth = 352; m_iHeight = 288;
  } else
  if(strSize == "vga")
  {
    m_iWidth = 640; m_iHeight = 480;
  }
  else
  {
    m_iWidth = (strSize.SubString(0, (strSize.Find('x')))).ToInt();
    m_iHeight = (strSize.SubString(strSize.Find('x')+1, strSize.Length())).ToInt();
  }

  m_iNorm = ( pBroadcast->GetOption( "v4l_norm" ) ).ToInt();
  m_iFrequency = ( pBroadcast->GetOption( "v4l_frequency" ) ).ToInt();
  m_iSample_rate = pBroadcast->GetOption("v4l_audiofreq").ToInt();
  m_iChannel = pBroadcast->GetOption("v4l_audiochannel").ToInt();
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_v4lMpegReader::Init()
{
  int iRc;

  m_iCurrentFrame   = 0;
  if ( (m_hFd = open(m_strDeviceName.GetString(), O_RDWR)) < 0)
  {
    throw E_v4l(C_String("Could not open device ") + m_strDeviceName);
    return;
  }

  if (ioctl(m_hFd,VIDIOCGCAP,&m_Vcap) < 0)
  {
       Log(m_hLog, LOG_NOTE, "Warning : Capabilities Request failed ! \n");
  }
  else
  {
    fprintf(stderr,"Info : Found V4L device %s\n       %u channel(s)\n"
                   "%ux%u->%ux%u\n",
      m_Vcap.name,m_Vcap.channels,m_Vcap.minwidth,m_Vcap.minheight,
      m_Vcap.maxwidth,m_Vcap.maxheight);
  }

  /* Check the range of CurrentChannel */
  if (m_iCurrentChannel>m_Vcap.channels)
    m_iCurrentChannel = 0;

  /* Check the range of the m_iWidth */
  if (!( m_Vcap.minwidth <= m_iWidth ) || !( m_iWidth <= m_Vcap.maxwidth ) )
    m_iWidth = 0; // Arbitrary !!!

   /* Check the range of the m_iHeight */
  if (!( m_Vcap.minheight <= m_iHeight ) || !( m_iHeight <= m_Vcap.maxheight ) )
    m_iHeight = 0; // Arbitrary !!!        
  
  if (!(m_Vcap.type & VID_TYPE_CAPTURE))
  {
    Log(m_hLog, LOG_WARN, "Warning : Capture not supported !\n");
  }

  memset(&m_Vchan,0,sizeof(video_channel));
 
  m_Vchan.channel = m_iCurrentChannel;
//  m_Vchan.type = 2;
//  m_Vchan.flags = 2;
//  m_Vchan.norm = 0;
  
  /* Get Channel Name*/
  if (ioctl(m_hFd,VIDIOCGCHAN,&m_Vchan) < 0)
  {
    fprintf( stderr, "Warning : could not get info on channel %d\n", m_iCurrentChannel);
  }

  fprintf( stderr, "Info : Changing to channel %u (%s)\n",
           m_Vchan.channel, m_Vchan.name);

  m_Vchan.norm = m_iNorm; // PAL/NTSC/SECAM
  if( ioctl( m_hFd, VIDIOCSCHAN, &m_Vchan ) < 0 )
  {
    fprintf( stderr, "Warning : could not change to channel %u!\n",
             m_iCurrentChannel);
  }

  /* if it's a tuner, set the frequency */
  if( m_Vchan.flags & VIDEO_VC_TUNER )
  {
    int oldFrequency;
    if( ioctl( m_hFd, VIDIOCGFREQ, &oldFrequency ) < 0 )
    {
      fprintf( stderr, "Warning : could not get frequency\n" );
    }

    /* set the frequency only if the user specified one.
       otherwise, the current one is used */
    if( m_iFrequency > 0 )
    {
      fprintf( stderr, "Info : changing from frequency %d to %d\n",
               oldFrequency, m_iFrequency );
      if( ioctl( m_hFd, VIDIOCSFREQ, &m_iFrequency ) < 0 )
      {
        Log(m_hLog, LOG_WARN, "Warning : could not change frequency\n" );
      }
    }
    else
    {
      fprintf( stderr, "Info : current frequency is %d\n", oldFrequency );
    }
  }   

  /* unmute audio */
  ioctl(m_hFd, VIDIOCGAUDIO, &m_Audio);
  memcpy(&m_Audio_saved, &m_Audio, sizeof(m_Audio));
  m_Audio.flags &= ~VIDEO_AUDIO_MUTE;
  ioctl(m_hFd, VIDIOCSAUDIO, &m_Audio);

  iRc = ioctl(m_hFd,VIDIOCGMBUF,&m_Vbuf);

  if (!m_iWidth || !m_iHeight)
  {
    if (ioctl(m_hFd,VIDIOCGWIN,&m_Vwin) < 0)
    {
      Log(m_hLog, LOG_WARN, "Warning : VIDIOCGWIN failed \n");
    }
    m_iWidth     = m_Vwin.width;
    m_iHeight    = m_Vwin.height;
  }
    
  m_iFrameRate = 25;
  m_pBroadcast->SetOption("FrameWidth",C_String(m_iWidth));
  m_pBroadcast->SetOption("FrameHeight",C_String(m_iHeight));
  m_pBroadcast->SetOption("FrameRate",C_String(m_iFrameRate));

  if (iRc < 0)
  {
    /* try to use read based access */
    m_Vwin.x = 0;
    m_Vwin.y = 0;
    m_Vwin.width = m_iWidth;
    m_Vwin.height = m_iHeight;
    m_Vwin.chromakey = (u32)-1;
    m_Vwin.flags = 0;

    ioctl(m_hFd, VIDIOCSWIN, &m_Vwin);
    ioctl(m_hFd, VIDIOCGPICT, &m_Vpic);

    m_Vpic.palette=VIDEO_PALETTE_YUV420P;
    if (ioctl(m_hFd, VIDIOCSPICT, &m_Vpic) < 0)
    {
      m_Vpic.palette=VIDEO_PALETTE_YUV422;
      if (ioctl(m_hFd, VIDIOCSPICT, &m_Vpic) < 0)
      {
        m_Vpic.palette=VIDEO_PALETTE_RGB24;
        if (ioctl(m_hFd, VIDIOCSPICT, &m_Vpic) < 0)
          throw E_v4l(C_String("Could not find suitable palette"));
      }
    }

    int val = 1;
    ioctl(m_hFd, VIDIOCCAPTURE, &val); /* Enable Capture */
    m_bUseMmap = 0;
  }
  else
  {
    /* Video Buffer Mapping */
    m_pVideoMap = (byte*)mmap(0,m_Vbuf.size,PROT_READ|PROT_WRITE,MAP_SHARED,m_hFd,0);
    if ((unsigned char*)-1 == m_pVideoMap)
    {
      throw E_v4l(C_String("Could not map video buffer !"));
    }
    m_iCurrentFrame = 0;
     /* start to grab the first frame */
    m_Vmmap.frame =  m_iCurrentFrame % m_Vbuf.frames;
    m_Vmmap.height = m_iHeight;
    m_Vmmap.width = m_iWidth;
    m_Vmmap.format = VIDEO_PALETTE_YUV420P;

    fprintf(stderr,"Info : Found V4L window %ux%u\n",m_iWidth, m_iHeight);

     /* Try different palettes */
    if ((iRc = ioctl(m_hFd, VIDIOCMCAPTURE, &m_Vmmap)) < 0)
    {
      m_Vmmap.format = VIDEO_PALETTE_YUV420P;
      if ((iRc = ioctl(m_hFd, VIDIOCMCAPTURE, &m_Vmmap)) < 0 && errno != EAGAIN)
      {
        m_Vmmap.format = VIDEO_PALETTE_YUV422;
        if ((iRc = ioctl(m_hFd, VIDIOCMCAPTURE, &m_Vmmap)) < 0 && errno != EAGAIN)
        {
          m_Vmmap.format = VIDEO_PALETTE_RGB24;
          iRc = ioctl(m_hFd, VIDIOCMCAPTURE, &m_Vmmap);
        }
      }
    }

    if (iRc < 0)
    {
      if (errno != EAGAIN)
      {
        throw E_v4l(C_String("Could not find grabbing parameters !\n"));
      }
      else
      {
        throw E_v4l(C_String("No signal detected !\n"));
      }
    }
    m_bUseMmap = 1;
  }

  switch(m_Vmmap.format)
  {
    case VIDEO_PALETTE_YUV420P:
        m_pBroadcast->SetOption("Palette",PIX_FMT_YUV420P);
        Log(m_hLog, LOG_NOTE, "Info : Palette YUV420P\n");
        break;
    case VIDEO_PALETTE_YUV422:
        m_pBroadcast->SetOption("Palette",PIX_FMT_YUV422);
        Log(m_hLog, LOG_NOTE, "Info : Palette YUV422\n");
        break;
    case VIDEO_PALETTE_RGB24:
        Log(m_hLog, LOG_NOTE, "Info : Palette RGB32\n");
        m_pBroadcast->SetOption("Palette",PIX_FMT_BGR24);
        break;
    default:
        Log(m_hLog, LOG_NOTE, "Warning : Palette may not be supported\n");
        m_pBroadcast->SetOption("Palette",PIX_FMT_YUV420P);
        break;
  }

  /* sound stuff now */
  if (!m_bMute)
  {
    /* open the device */
    if( ( m_iAudioFD = open(m_strAudioDeviceName.GetString(), O_RDONLY ) ) < 0 )
    {
      Log(m_hLog, LOG_ERROR, "could not open " +
                          m_strAudioDeviceName+" for reading\n");
      return;
    }
    else
      Log(m_hLog, LOG_NOTE, "Info : Opening sound device " +
                      m_strAudioDeviceName);

    /* 16 bits samples, little endian */
    int format = AFMT_S16_LE;
    if( ioctl( m_iAudioFD, SNDCTL_DSP_SETFMT, &format ) < 0 )
    {
      Log(m_hLog, LOG_ERROR, "ioctl SNDCTL_DSP_SETFMT failed\n" );
      return;
    }
    if( format != AFMT_S16_LE )
    {
      Log(m_hLog, LOG_ERROR, "AFMT_S16_LE not supported\n" );
      return;
    }

    /* channel count */
    int stereo = (m_iChannel != 1 );

    if( ioctl( m_iAudioFD, SNDCTL_DSP_STEREO, &stereo ) < 0 )
    {
      fprintf( stderr, "ioctl SNDCTL_DSP_STEREO (%d) failed : %s\n", 
                                                 stereo, strerror(errno));
      fprintf( stderr, "Fall back to mono\n");

      /* Back to mono, and update value (raw2ts will catch it later) */
      m_iChannel = 1;
      m_pBroadcast->SetOption("v4l_audiochannel","1");
    }

    /* sample rate */
    if( ioctl( m_iAudioFD, SNDCTL_DSP_SPEED, &m_iSample_rate ) < 0 )
    {
      fprintf( stderr, "ioctl SNDCTL_DSP_SPEED failed (%d) : %s\n",
                                        m_iSample_rate, strerror(errno));
      fprintf( stderr, "back to 44100 Hz\n");

      /* Back to 44110 Hz, and update value (raw2ts will catch it later) */
      m_iSample_rate = 44100;
      m_pBroadcast->SetOption("v4l_audiofreq","44100");

      if( ioctl( m_iAudioFD, SNDCTL_DSP_SPEED, &m_iSample_rate ) < 0 )
      {
        fprintf( stderr, "ioctl SNDCTL_DSP_SPEED failed (%d) : %s\n",
                                       m_iSample_rate, strerror(errno));
      }

    }

  } else {
    m_iAudioFD = -1;
  }
  
  m_pAudioBuffer = NULL;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_v4lMpegReader::Close()
{
  /* mute audio */
  ioctl(m_hFd, VIDIOCGAUDIO, &m_Audio);
  memcpy(&m_Audio_saved, &m_Audio, sizeof(m_Audio));
  m_Audio.flags &= VIDEO_AUDIO_MUTE;
  ioctl(m_hFd, VIDIOCSAUDIO, &m_Audio);

  close(m_hFd);
  close(m_iAudioFD);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_v4lMpegReader::Read(byte* pBuff, s64 iSize)
{
  return read(m_hFd, pBuff, 188);
}

int C_v4lMpegReader::GetFrame(byte ** ppBuff , int iSize)
{
  if (m_bUseMmap)
  {
    m_Vmmap.frame = (m_iCurrentFrame + 1) % m_Vbuf.frames;

    if (ioctl(m_hFd, VIDIOCMCAPTURE, &m_Vmmap) < 0)
    {
      if (errno == EAGAIN)
        Log(m_hLog, LOG_ERROR, "Could not Synchronize ! \n");
      else
        printf("VIDIOCMCAPTURE failed (%s) \n", strerror( errno ) );
      return -EIO;
    }

    while (ioctl(m_hFd, VIDIOCSYNC, &m_iCurrentFrame) < 0 &&
          (errno == EAGAIN || errno == EINTR));

    *ppBuff = m_pVideoMap + m_Vbuf.offsets[m_iCurrentFrame];

    /* This is now the grabbing frame */
    m_iCurrentFrame = m_Vmmap.frame;
  }
  else
    Read(*ppBuff, iSize);

  return 0;
}

// FIXME : because the MpegReader cannot date buffers,
// we give the file descriptor to the converter so it can ioctl()
int C_v4lMpegReader::GetAudioFD()
{
  return m_iAudioFD;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_v4lMpegReader::Seek(s64 iOffset, s64 bStartPos)
{
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_v4lMpegReader::Size()
{
  ASSERT(false);
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_v4lMpegReader::GetPos()
{
  return 0;
}
