/*******************************************************************************
* tcpinput.h: TCP Input class definition
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id: tcpinput.h,v 1.2 2003/09/10 12:57:52 nitrox Exp $
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _TCP_INPUT_H_
#define _TCP_INPUT_H_

//------------------------------------------------------------------------------
// C_XXXInput class
//------------------------------------------------------------------------------
class C_TcpInput : public C_Input
{
public:
    C_TcpInput(C_Module* pModule, const C_String& strName);
    ~C_TcpInput();
    virtual C_String GetType() {return C_String("tcp");}

protected:
    // Init/termination methods
    virtual void OnInit();
    virtual void OnDestroy();

    // Selection of the pgrms to broadcast
    virtual void OnStartStreaming(C_Broadcast* pBroadcast);
    virtual void OnResumeStreaming(C_Broadcast* pBroadcast);
    virtual void OnSuspendStreaming(C_Broadcast* pBroadcast);
    virtual void OnForwardStreaming(C_Broadcast* pBroadcast, int speed);
    virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed);
    virtual void OnStopStreaming(C_Broadcast* pBroadcast);
    virtual void OnUpdateProgram(C_String strProgram,C_String strFileName, C_String strType);
    virtual void OnDeleteProgram(C_String strProgram);
    virtual C_List<C_Program> OnGetAvailablePgrms();

private:

    // Programs names
    // the program ID is m_vProgramNames.Find(strName) + 1
    C_Vector<C_String> m_vProgramNames;

    // List of currently running trickplay, streamers and converters
    C_HashTable<handle, C_TsStreamer> m_cStreamers;
    C_HashTable<handle, C_TrickPlay> m_cTrickPlay;
    C_HashTable<handle, C_MpegConverter> m_cConverters;
};

// Declaration and implementation of C_XXXInputModule
DECLARE_MODULE(Tcp, Input, "tcp", const C_String&);

#else
#error "Multiple inclusins of tcpinput.h"
#endif
