/*******************************************************************************
* rtp.cpp: RTP header manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: rtp.cpp,v 1.4 2003/10/27 10:58:11 sam Exp $
*
* Authors: Tristan Leteurtre <tooney@via.ecp.fr>
*          Jean-Paul Saman <saman@natlab.research.philips.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "mpeg.h"
#include "ts.h"
#include "rtp.h"



//******************************************************************************
// C_RtpHeader class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Give direct access to the raw buffer
//------------------------------------------------------------------------------
C_RtpHeader::operator byte* ()
{
  return bData;
}


//------------------------------------------------------------------------------
// BuildHeader: Build a RTP header and return its size
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
u8 C_RtpHeader::BuildHeader(u16 iCounter)
{
  bData[0] = 0x80; // version = 2
  bData[1] = 33; // Mpeg2-TS A/V at 90 kHz, as defined in RFC 1890

  // Set sequence number with initial random iCounter
  bData[2] = iCounter >> 8; //msb
  bData[3] = iCounter;      //lsb

  // byte 4,5,6,7 is the PCR time stamp in our case
  bData[4]=bData[5]=bData[6]=bData[7]=0xFF;
  // byte 8,9,10,11 are the synchronization source (SSRC)
  bData[8]=bData[9]=bData[10]=bData[11]=0x12;
  // payload should be TsPacket (188 bytes)
  return RTP_HEADER_LEN;
}

void C_RtpHeader::SetRtpDiscontinuity( bool bValue )
{
  if (bValue)
     // set M bit = 1
     // see RFC 2250 / RFC 1889 for more details
     bData[1] = (bData[1] | 0x80);
  else
     bData[1] = (bData[1] & 0x7F); // clear bit = 0
}

// Time should be given in the 90 kHz Clock

void C_RtpHeader::SetRtpTimeStamp(u32 time)
{
  SET_U32_TO(bData[4],time);
};

u32 C_RtpHeader::GetRtpTimeStamp(void)
{
  return ( ((u32)bData[4])<<24 ||  ((u32)bData[5])<<16 || ((u32)bData[6])<<8
                                                       || ((u32)bData[7]));
};

void C_RtpHeader::PrintPacket()
{
  for(int i= 0; i<12 ; i++)
    printf("%02X ", bData[i]);
  printf("\n");
};


