/*******************************************************************************
* dvbpsi.cpp: common tools to use the libdvbpsi
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"

#if defined(HAVE_INTTYPES_H)
#   include <inttypes.h>
#elif defined(HAVE_STDINT_H)
#   include <stdint.h>
#endif

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/psi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#else
#   include "src/dvbpsi.h"
#   include "src/psi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#endif

#include "mpeg.h"
#include "ts.h"

#include "../server/buffer.h"

#include "dvbpsi.h"


//******************************************************************************
// C_DvbPsiTable class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Default constructor
//------------------------------------------------------------------------------
C_DvbPsiTable::C_DvbPsiTable()
{
  m_pSections = NULL;
  m_pCurrentSection = NULL;
  m_pPosInSection = NULL;
  m_iContinuityCounter = 0;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_DvbPsiTable::~C_DvbPsiTable()
{
  if(m_pSections)
    dvbpsi_DeletePSISections(m_pSections);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiTable::TsReset()
{
  m_pCurrentSection = m_pSections;

  if(m_pCurrentSection)
    m_pPosInSection = m_pCurrentSection->p_data;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_DvbPsiTable::TsHasNext()
{
  return m_pCurrentSection;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiTable::TsWrite(C_TsPacket *pPacket)
{
  byte *pStart = *pPacket;
  byte *pPosInTs = *pPacket;
  byte *pEnd = m_pCurrentSection->p_payload_end
                        + (m_pCurrentSection->b_syntax_indicator ? 4 : 0);

  pPosInTs[0] = 0x47;
  pPosInTs[1] =   ((m_pPosInSection == m_pCurrentSection->p_data) ? 0x40 : 0x00)
                | m_iPid >> 8;
  pPosInTs[2] = m_iPid;
  pPosInTs[3] = 0x10 | m_iContinuityCounter;
  pPosInTs += 4;

  m_iContinuityCounter = (m_iContinuityCounter + 1) & 0x0f;

  if(m_pPosInSection == m_pCurrentSection->p_data)
    *(pPosInTs++) = 0x00;

  while((pPosInTs < pStart + 188) && (m_pPosInSection < pEnd))
    *(pPosInTs++) = *(m_pPosInSection++);
  while(pPosInTs < pStart + 188)
    *(pPosInTs++) = 0xff;

  if(m_pPosInSection == pEnd)
  {
    m_pCurrentSection = m_pCurrentSection->p_next;
    if(m_pCurrentSection)
      m_pPosInSection = m_pCurrentSection->p_data;
  }
}


//******************************************************************************
// C_DvbPsiPat class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Base constructor
//------------------------------------------------------------------------------
C_DvbPsiPat::C_DvbPsiPat(u16 iTsId, u8 iVersion, bool bCurrentNext)
{
  m_iPid = 0;
  dvbpsi_InitPAT(&m_sPat, iTsId, iVersion, bCurrentNext);
}


//------------------------------------------------------------------------------
// Low level constructor
//------------------------------------------------------------------------------
C_DvbPsiPat::C_DvbPsiPat(dvbpsi_pat_t* pPat)
{
  m_iPid = 0;
  ASSERT(pPat);
  m_sPat = *pPat;

  pPat->p_first_program = NULL;
}


//------------------------------------------------------------------------------
// Copy constructor
//------------------------------------------------------------------------------
C_DvbPsiPat::C_DvbPsiPat(const C_DvbPsiPat& cPat) : C_DvbPsiTable ()
{
  m_iPid = 0;
  dvbpsi_InitPAT(&m_sPat,
                 cPat.m_sPat.i_ts_id,
                 cPat.m_sPat.i_version,
                 cPat.m_sPat.b_current_next);

  dvbpsi_pat_program_t *pProgram = cPat.m_sPat.p_first_program;
  while(pProgram)
  {
    dvbpsi_PATAddProgram(&m_sPat, pProgram->i_number, pProgram->i_pid);
    pProgram = pProgram->p_next;
  }
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_DvbPsiPat::~C_DvbPsiPat()
{
  dvbpsi_EmptyPAT(&m_sPat);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPat::BuildFromNext(C_DvbPsiPat *pNextPat)
{
  ASSERT(pNextPat);

  if(    (m_sPat.i_ts_id == pNextPat->m_sPat.i_ts_id)
      && (m_sPat.i_version == pNextPat->m_sPat.i_version)
      && (!pNextPat->m_sPat.b_current_next)
      && (m_sPat.b_current_next)
      && (m_sPat.p_first_program == NULL))
  {
    m_sPat.p_first_program = pNextPat->m_sPat.p_first_program;
    pNextPat->m_sPat.p_first_program = NULL;
  }
  else
  {
    ASSERT(false);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
dvbpsi_pat_program_t* C_DvbPsiPat::GetProgram(u16 iNumber) const
{
  dvbpsi_pat_program_t *pProgram = m_sPat.p_first_program;
  while((pProgram != NULL) && (pProgram->i_number != iNumber))
    pProgram = pProgram->p_next;

  return pProgram;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_DvbPsiPat& C_DvbPsiPat::operator = (const C_DvbPsiPat& cPat)
{
  dvbpsi_InitPAT(&m_sPat,
                 cPat.m_sPat.i_ts_id,
                 cPat.m_sPat.i_version,
                 cPat.m_sPat.b_current_next);

  dvbpsi_pat_program_t *pProgram = cPat.m_sPat.p_first_program;
  while(pProgram)
  {
    dvbpsi_PATAddProgram(&m_sPat, pProgram->i_number, pProgram->i_pid);
    pProgram = pProgram->p_next;
  }

  return *this;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_DvbPsiPat C_DvbPsiPat::operator - (const C_DvbPsiPat& cPat) const
{
  C_DvbPsiPat cResult(0, 0, false);

  dvbpsi_pat_program_t *pProgram = m_sPat.p_first_program;
  while(pProgram != NULL)
  {
    dvbpsi_pat_program_t *pProgram2 = cPat.GetProgram(pProgram->i_number);
    if((!pProgram2) || (pProgram->i_pid != pProgram2->i_pid))
      dvbpsi_PATAddProgram(&cResult.m_sPat,
                           pProgram->i_number, pProgram->i_pid);
    pProgram = pProgram->p_next;
  }

  return cResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPat::Generate()
{
  if(m_pSections)
    dvbpsi_DeletePSISections(m_pSections);

  m_pSections = dvbpsi_GenPATSections(&m_sPat, 0);
}


//******************************************************************************
// I_DvbPsiPatHandler class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
I_DvbPsiPatHandler::I_DvbPsiPatHandler()
{
  m_pPreviousPat = m_pCurrentPat = m_pNextPat = NULL;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
I_DvbPsiPatHandler::~I_DvbPsiPatHandler()
{
  if(m_pPreviousPat)
    delete m_pPreviousPat;

  if(m_pCurrentPat)
    delete m_pCurrentPat;

  if(m_pNextPat)
    delete m_pNextPat;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void I_DvbPsiPatHandler::HandlePat(C_DvbPsiPat* pPat)
{
  ASSERT(pPat);

  if(!pPat->CurrentNext())
  {
    if(m_pNextPat)
      delete m_pNextPat;
    m_pNextPat = pPat;

    OnDvbPsiPatEvent(DVBPSI_EVENT_NEXT);
  }
  else
  {
    if(m_pPreviousPat)
      delete m_pPreviousPat;
    m_pPreviousPat = m_pCurrentPat;
    if(m_pNextPat)
    {
      pPat->BuildFromNext(m_pNextPat);
      m_pCurrentPat = pPat;
      delete m_pNextPat;
      m_pNextPat = NULL;
    }
    else
    {
      m_pCurrentPat = pPat;
    }

    OnDvbPsiPatEvent(DVBPSI_EVENT_CURRENT);
  }
}


//******************************************************************************
// C_DvbPsiPatDecoder class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DvbPsiPatDecoder::C_DvbPsiPatDecoder(C_NetList *pTsProvider,
                                       I_DvbPsiPatHandler *pHandler)
{
  ASSERT(pTsProvider);

  m_hDvbPsi = NULL;
  m_pTsProvider = pTsProvider;
  m_pHandler = pHandler;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPatDecoder::Attach()
{
  m_hDvbPsi = dvbpsi_AttachPAT((dvbpsi_pat_callback)Callback, this);

  if(!m_hDvbPsi)
    throw E_Exception(GEN_ERR,
                      "libdvbpsi was unable to create the PAT decoder");
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPatDecoder::Detach()
{
  if(m_hDvbPsi)
  {
    dvbpsi_DetachPAT(m_hDvbPsi);
    m_hDvbPsi = NULL;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPatDecoder::Callback(C_DvbPsiPatDecoder *pThis,
                                  dvbpsi_pat_t* p_new_pat)
{
  ASSERT(pThis);

  C_DvbPsiPat *pPat = new C_DvbPsiPat(p_new_pat);
  ASSERT(pPat);
  dvbpsi_DeletePAT(p_new_pat);

  pThis->m_pHandler->HandlePat(pPat);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_DvbPsiPatDecoder::HandlePacket(C_TsPacket* pPacket)
{
  dvbpsi_PushPacket(m_hDvbPsi, (byte*)pPacket);

  m_pTsProvider->ReleasePacket(pPacket);
  return true;
}


//******************************************************************************
// C_DvbPsiPmt class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Base constructor
//------------------------------------------------------------------------------
C_DvbPsiPmt::C_DvbPsiPmt(u16 iProgramNumber, u8 iVersion,
                         bool bCurrentNext, u16 iPcrPid, u16 iPid/* = 0*/)
{
  ASSERT(!(iPid & 0xe000))
  m_iPid = iPid;
  dvbpsi_InitPMT(&m_sPmt, iProgramNumber, iVersion, bCurrentNext, iPcrPid);
}


//------------------------------------------------------------------------------
// Low level constructor
//------------------------------------------------------------------------------
C_DvbPsiPmt::C_DvbPsiPmt(dvbpsi_pmt_t* pPmt)
{
  ASSERT(pPmt);
  m_sPmt = *pPmt;

  pPmt->p_first_descriptor = NULL;
  pPmt->p_first_es = NULL;

  m_iPid = 0;
}


//------------------------------------------------------------------------------
// Copy constructor
//------------------------------------------------------------------------------
C_DvbPsiPmt::C_DvbPsiPmt(const C_DvbPsiPmt& cPmt) : C_DvbPsiTable()
{
  m_iPid = cPmt.m_iPid;

  dvbpsi_InitPMT(&m_sPmt,
                 cPmt.m_sPmt.i_program_number,
                 cPmt.m_sPmt.i_version,
                 cPmt.m_sPmt.b_current_next,
                 cPmt.m_sPmt.i_pcr_pid);

  dvbpsi_descriptor_t *pDescriptor = cPmt.m_sPmt.p_first_descriptor;
  while(pDescriptor)
  {
    dvbpsi_PMTAddDescriptor(&m_sPmt, pDescriptor->i_tag,
                            pDescriptor->i_length, pDescriptor->p_data);
    pDescriptor = pDescriptor->p_next;
  }

  dvbpsi_pmt_es_t *pEs = cPmt.m_sPmt.p_first_es;
  while(pEs)
  {
    dvbpsi_pmt_es_t *pEs2 = dvbpsi_PMTAddES(&m_sPmt, pEs->i_type, pEs->i_pid);

    pDescriptor = pEs->p_first_descriptor;
    while(pDescriptor)
    {
      dvbpsi_PMTESAddDescriptor(pEs2, pDescriptor->i_tag,
                                pDescriptor->i_length, pDescriptor->p_data);
      pDescriptor = pDescriptor->p_next;
    }

    pEs = pEs->p_next;
  }
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_DvbPsiPmt::~C_DvbPsiPmt()
{
  dvbpsi_EmptyPMT(&m_sPmt);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPmt::BuildFromNext(C_DvbPsiPmt *pNextPmt)
{
  ASSERT(pNextPmt);

  if(    (m_sPmt.i_program_number == pNextPmt->m_sPmt.i_program_number)
      && (m_sPmt.i_version == pNextPmt->m_sPmt.i_version)
      && (m_sPmt.i_pcr_pid == pNextPmt->m_sPmt.i_pcr_pid)
      && (!pNextPmt->m_sPmt.b_current_next)
      && (m_sPmt.b_current_next)
      && (m_sPmt.p_first_descriptor == NULL)
      && (m_sPmt.p_first_es == NULL))
  {
    m_sPmt.p_first_descriptor = pNextPmt->m_sPmt.p_first_descriptor;
    pNextPmt->m_sPmt.p_first_descriptor = NULL;
    m_sPmt.p_first_es = pNextPmt->m_sPmt.p_first_es;
    pNextPmt->m_sPmt.p_first_es = NULL;
  }
  else
  {
    ASSERT(false);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
dvbpsi_pmt_es_t* C_DvbPsiPmt::GetEs(u16 iPid) const
{
  dvbpsi_pmt_es_t *pEs = m_sPmt.p_first_es;
  while((pEs != NULL) && (pEs->i_pid != iPid))
    pEs = pEs->p_next;

  return pEs;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_DvbPsiPmt& C_DvbPsiPmt::operator = (const C_DvbPsiPmt& cPmt)
{
  dvbpsi_InitPMT(&m_sPmt,
                 cPmt.m_sPmt.i_program_number,
                 cPmt.m_sPmt.i_version,
                 cPmt.m_sPmt.b_current_next,
                 cPmt.m_sPmt.i_pcr_pid);

  dvbpsi_descriptor_t *pDescriptor = cPmt.m_sPmt.p_first_descriptor;
  while(pDescriptor)
  {
    dvbpsi_PMTAddDescriptor(&m_sPmt, pDescriptor->i_tag,
                            pDescriptor->i_length, pDescriptor->p_data);
    pDescriptor = pDescriptor->p_next;
  }

  dvbpsi_pmt_es_t *pEs = cPmt.m_sPmt.p_first_es;
  while(pEs)
  {
    dvbpsi_pmt_es_t *pEs2 = dvbpsi_PMTAddES(&m_sPmt, pEs->i_type, pEs->i_pid);

    pDescriptor = pEs->p_first_descriptor;
    while(pDescriptor)
    {
      dvbpsi_PMTESAddDescriptor(pEs2, pDescriptor->i_tag,
                                pDescriptor->i_length, pDescriptor->p_data);
      pDescriptor = pDescriptor->p_next;
    }

    pEs = pEs->p_next;
  }

  return *this;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_DvbPsiPmt C_DvbPsiPmt::operator - (const C_DvbPsiPmt& cPmt) const
{
  C_DvbPsiPmt cResult(0, 0, false, 0);

  dvbpsi_pmt_es_t *pEs = m_sPmt.p_first_es;
  while(pEs != NULL)
  {
    dvbpsi_pmt_es_t *pEs2 = cPmt.GetEs(pEs->i_pid);
    if((!pEs2) || (pEs->i_type != pEs2->i_type))
      dvbpsi_PMTAddES(&cResult.m_sPmt, pEs->i_type, pEs->i_pid);
    pEs = pEs->p_next;
  }

  return cResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPmt::Generate()
{
  if(m_pSections)
    dvbpsi_DeletePSISections(m_pSections);

  m_pSections = dvbpsi_GenPMTSections(&m_sPmt);
}


//******************************************************************************
// I_DvbPsiPmtHandler class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
I_DvbPsiPmtHandler::I_DvbPsiPmtHandler()
{
  m_pPreviousPmt = m_pCurrentPmt = m_pNextPmt = NULL;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
I_DvbPsiPmtHandler::~I_DvbPsiPmtHandler()
{
  if(m_pPreviousPmt)
    delete m_pPreviousPmt;

  if(m_pCurrentPmt)
    delete m_pCurrentPmt;

  if(m_pNextPmt)
    delete m_pNextPmt;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void I_DvbPsiPmtHandler::HandlePmt(C_DvbPsiPmt* pPmt)
{
  ASSERT(pPmt);

  if(!pPmt->CurrentNext())
  {
    if(m_pNextPmt)
      delete m_pNextPmt;
    m_pNextPmt = pPmt;

    OnDvbPsiPmtEvent(DVBPSI_EVENT_NEXT);
  }
  else
  {
    if(m_pPreviousPmt)
      delete m_pPreviousPmt;
    m_pPreviousPmt = m_pCurrentPmt;
    if(m_pNextPmt)
    {
      pPmt->BuildFromNext(m_pNextPmt);
      m_pCurrentPmt = pPmt;
      delete m_pNextPmt;
      m_pNextPmt = NULL;
    }
    else
    {
      m_pCurrentPmt = pPmt;
    }

    OnDvbPsiPmtEvent(DVBPSI_EVENT_CURRENT);
  }
}


//******************************************************************************
// C_DvbPsiPmtDecoder class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DvbPsiPmtDecoder::C_DvbPsiPmtDecoder(u16 iProgramNumber,
                                       C_NetList *pTsProvider,
                                       I_DvbPsiPmtHandler *pHandler)
{
  ASSERT(pTsProvider);

  m_hDvbPsi = NULL;
  m_iProgramNumber = iProgramNumber;
  m_pTsProvider = pTsProvider;
  m_pHandler = pHandler;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPmtDecoder::Attach()
{
  m_hDvbPsi = dvbpsi_AttachPMT(m_iProgramNumber,
                               (dvbpsi_pmt_callback)Callback, this);

  if(!m_hDvbPsi)
    throw E_Exception(GEN_ERR,
                      "libdvbpsi was unable to create the PMT decoder");
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPmtDecoder::Detach()
{
  if(m_hDvbPsi)
  {
    dvbpsi_DetachPMT(m_hDvbPsi);
    m_hDvbPsi = NULL;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbPsiPmtDecoder::Callback(C_DvbPsiPmtDecoder *pThis,
                                  dvbpsi_pmt_t* p_new_pmt)
{
  ASSERT(pThis);

  C_DvbPsiPmt *pPmt = new C_DvbPsiPmt(p_new_pmt);
  ASSERT(pPmt);
  dvbpsi_DeletePMT(p_new_pmt);

  pThis->m_pHandler->HandlePmt(pPmt);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_DvbPsiPmtDecoder::HandlePacket(C_TsPacket* pPacket)
{
  dvbpsi_PushPacket(m_hDvbPsi, (byte*)pPacket);

  m_pTsProvider->ReleasePacket(pPacket);
  return true;
}

