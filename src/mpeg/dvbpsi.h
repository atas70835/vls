/*******************************************************************************
* dvbpsi.h: common tools to use the libdvbpsi
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _DVBPSI_H_
#define _DVBPSI_H_


//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
#define DVBPSI_EVENT_NEXT       (1 << 0)
#define DVBPSI_EVENT_CURRENT    (1 << 1)


//------------------------------------------------------------------------------
// C_DvbPsiTable
//------------------------------------------------------------------------------
class C_DvbPsiTable
{
public:
  C_DvbPsiTable();
  virtual ~C_DvbPsiTable();

  // TS Generation
  virtual void Generate() = 0;
  void TsReset();
  bool TsHasNext();
  void TsWrite(C_TsPacket *pPacket);
  void SetPid(uint16_t iPid) { m_iPid = iPid;}

protected:
  u16 m_iPid;
  dvbpsi_psi_section_t *m_pSections;

private:
  dvbpsi_psi_section_t *m_pCurrentSection;
  byte *m_pPosInSection;
  u8 m_iContinuityCounter;
};


//------------------------------------------------------------------------------
// C_DvbPsiPat
//------------------------------------------------------------------------------
class C_DvbPsiPat : public C_DvbPsiTable
{
public:
  // Base constructor
  C_DvbPsiPat(u16 iTsId, u8 iVersion, bool bCurrentNext);
  // Low level constructor
  C_DvbPsiPat(dvbpsi_pat_t* pPat);

  C_DvbPsiPat(const C_DvbPsiPat& cPat);
  virtual ~C_DvbPsiPat();

  void BuildFromNext(C_DvbPsiPat *pNextPat);

  bool CurrentNext() const
  {
    return m_sPat.b_current_next;
  };

  // Low level access
  dvbpsi_pat_t* GetLowLevelPat()
  {
    return &m_sPat;
  };

  dvbpsi_pat_program_t* GetProgram(u16 iNumber) const;

  // (A - B) == (A - intersection(A, B))
  C_DvbPsiPat operator - (const C_DvbPsiPat& cPat) const;
  C_DvbPsiPat& operator = (const C_DvbPsiPat& cPat);

  // PAT generation
  virtual void Generate();

protected:
private:
  dvbpsi_pat_t m_sPat;
};


//------------------------------------------------------------------------------
// I_DvbPsiPatHandler
//------------------------------------------------------------------------------
class I_DvbPsiPatHandler
{
public:
  I_DvbPsiPatHandler();
  virtual ~I_DvbPsiPatHandler();

  void HandlePat(C_DvbPsiPat* pPat);

protected:
  virtual void OnDvbPsiPatEvent(int iEvent) = 0;

  C_DvbPsiPat *m_pPreviousPat;
  C_DvbPsiPat *m_pCurrentPat;
  C_DvbPsiPat *m_pNextPat;

private:
};


//------------------------------------------------------------------------------
// C_DvbPsiPatDecoder
//------------------------------------------------------------------------------
class C_DvbPsiPatDecoder : public I_TsPacketHandler
{
public:
  C_DvbPsiPatDecoder(C_NetList *pTsProvider, I_DvbPsiPatHandler *pHandler);

  void Attach();
  void Detach();

  virtual bool HandlePacket(C_TsPacket* pPacket);
  virtual bool HandlePrefillPacket(C_TsPacket*) { return false; }
  virtual void Shutdown() {}
  virtual void PrefillStart() {}
  virtual void PrefillComplete() {}
private:
  static void Callback(C_DvbPsiPatDecoder *pThis, dvbpsi_pat_t* p_new_pat);

  C_NetList *m_pTsProvider;
  I_DvbPsiPatHandler *m_pHandler;

  dvbpsi_handle m_hDvbPsi;
};


//------------------------------------------------------------------------------
// C_DvbPsiPmt
//------------------------------------------------------------------------------
class C_DvbPsiPmt : public C_DvbPsiTable
{
public:
  // Base constructor
  C_DvbPsiPmt(u16 iProgramNumber, u8 iVersion, bool bCurrentNext, u16 iPcrPid,
              u16 iPid = 0);
  // Low level constructor
  C_DvbPsiPmt(dvbpsi_pmt_t* pPmt);

  C_DvbPsiPmt(const C_DvbPsiPmt& cPmt);
  virtual ~C_DvbPsiPmt();

  void BuildFromNext(C_DvbPsiPmt *pNextPmt);

  bool CurrentNext() const
  {
    return m_sPmt.b_current_next;
  };

  // Low level access
  dvbpsi_pmt_t* GetLowLevelPmt()
  {
    return &m_sPmt;
  };

  dvbpsi_pmt_es_t* GetEs(u16 iPid) const;

  // (A - B) == (A - intersection(A, B))
  C_DvbPsiPmt operator - (const C_DvbPsiPmt& cPmt) const;
  C_DvbPsiPmt& operator = (const C_DvbPsiPmt& cPmt);

  // PMT generation
  virtual void Generate();

protected:
private:
  dvbpsi_pmt_t m_sPmt;
};


//------------------------------------------------------------------------------
// I_DvbPsiPmtHandler
//------------------------------------------------------------------------------
class I_DvbPsiPmtHandler
{
public:
  I_DvbPsiPmtHandler();
  virtual ~I_DvbPsiPmtHandler();

  void HandlePmt(C_DvbPsiPmt* pPmt);
  C_DvbPsiPmt * GetCurrentPmt() { return m_pCurrentPmt; }

protected:
  virtual void OnDvbPsiPmtEvent(int iEvent) = 0;

  C_DvbPsiPmt *m_pPreviousPmt;
  C_DvbPsiPmt *m_pCurrentPmt;
  C_DvbPsiPmt *m_pNextPmt;

private:
};


//------------------------------------------------------------------------------
// C_DvbPsiPmtDecoder
//------------------------------------------------------------------------------
class C_DvbPsiPmtDecoder : public I_TsPacketHandler
{
public:
  C_DvbPsiPmtDecoder(u16 iProgramNumber,
                     C_NetList *pTsProvider,
                     I_DvbPsiPmtHandler *pHandler);

  void Attach();
  void Detach();

  virtual bool HandlePacket(C_TsPacket* pPacket);
  virtual bool HandlePrefillPacket(C_TsPacket*) { return false; }
  virtual void Shutdown() {}
  virtual void PrefillStart() {}
  virtual void PrefillComplete() {}

private:
  static void Callback(C_DvbPsiPmtDecoder *pThis, dvbpsi_pmt_t* p_new_pmt);

  u16 m_iProgramNumber;
  C_NetList *m_pTsProvider;
  I_DvbPsiPmtHandler *m_pHandler;

  dvbpsi_handle m_hDvbPsi;
};


#else
#error "Multiple inclusions of dvbpsi.h"
#endif

