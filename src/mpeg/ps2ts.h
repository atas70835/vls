/*******************************************************************************
* ps2ts.h: MPEG1 and MPEG2 PS to TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: ps2ts.h,v 1.8 2003/12/15 13:47:04 adq Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


#ifndef _PS2TS_H_
#define _PS2TS_H_


template <class Reader, class TsProvider> class C_Ps2Ts
{
 public:
  C_Ps2Ts(Reader* pReader, TsProvider* pTsProvider, unsigned int iMaxBufferedTs,
          unsigned int iMpegVersion, int iLoop, handle hLog);
  ~C_Ps2Ts();
  
  // Find the first pack header in the stream
  int Synch();

  // 
  C_TsPacket* GetPacket(bool bPreparsing = false);

  // To check whenever GetPacket returns NULL
  int GetStatus() { return m_iStatus; };

  C_ProgramDescriptor* GetPgrmDescriptor() { return &m_cPgrmDescriptor; };

 protected:
  int FetchPackets(bool bPreparsing);

  int ParsePackHeader(bool bPreparsing);
  int ParsePES(bool bPreparsing);
  int SkipPES();

  int ReadData(u8* buf, int length);
  int SkipData(int length);
  int ReadDataLength(u16* lengthDest);

 private:
  Reader* m_pReader;
  TsProvider* m_pTsProvider;
  unsigned int m_iMaxBufferedTs;
  handle m_hLog;

  // Parser configuration
  u8 m_iPackHeaderLen;

  // Parser state
  u32 m_iDataType;
  u16 m_iDataLen;
  u8 m_iPrivateId;
  bool m_bPESStart;
  byte m_bBuff[TS_PACKET_LEN];
  bool m_bJustSynched;

  // Output state
  u32 m_iTSCounter;
  bool m_bSendPSI;
  bool m_bGenPat;
  bool m_bGenPmt;
  bool m_bResetPSI;
  C_ProgramDescriptor m_cPgrmDescriptor;

  C_DvbPsiPat m_cPat;
  C_DvbPsiPmt m_cPmt;

  // Preparsed data (for efficiency reasons)
  C_List<C_TsPacket> m_cPendingTS;
  C_TsPacket *m_pDelayedPacket;

  // Global status
  int m_iStatus;
  u64 m_iNextPCR;
  bool m_bNeedToSendPCR;
  int m_iLoop;

  u8 m_bCache[512];
  int m_iCacheLen;
  bool m_bInMiddleOfPES;

  // discontinuity management
  u64 m_iPrevPCR;
  bool m_bDiscontinuity;
};


#else
#error "Multiple inclusions of ps2ts.h"
#endif

