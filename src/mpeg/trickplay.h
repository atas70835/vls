/*******************************************************************************
* trickplay.h: Threaded TrickPlay Module
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: trickplay.h,v 1.1 2003/06/02 19:50:36 jpsaman Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _MPEG_TrickPlay_H_
#define _MPEG_TrickPlay_H_

typedef enum {  
    TRICKPLAY_PLAY,
    TRICKPLAY_PAUSE,           
    TRICKPLAY_FORWARD,          
    TRICKPLAY_REWIND,
    TRICKPLAY_STOP              
 } trickplay_status_t;

class C_TrickPlayConfig
{
public:
  handle                m_hLog;

  C_Broadcast*          m_pBroadcast;
  C_MpegReader*         m_pReader;
  C_MpegConverter*      m_pConverter;
  unsigned int          m_iInitFill;
  I_TsPacketHandler*    m_pHandler;
  C_EventHandler*       m_pEventHandler;
  C_NetList*            m_pTsProvider;
};

/*******************************************************************************
* C_TrickPlay : Between converter and SyncFifo
********************************************************************************
*
*******************************************************************************/

class C_TrickPlay: public C_Thread
{
public:
  C_TrickPlay() {};
  C_TrickPlay(C_Module* pModule, C_TrickPlayConfig& cConfig);
  virtual ~C_TrickPlay();

  handle m_hLog;

  virtual void HandlePacket(C_TsPacket* pPacket) = 0;

  // These trickplay functions handle the setting off
  // m_iRequestTrickPlay for its childeren and handles
  // sensibility in doing transistions. The *real* 
  // transistion is done on the decission of the child
  // so it can implements its own trickplay policy.
  virtual void Resume();
  virtual void Suspend();
  virtual void Forward(int speed);
  virtual void Rewind(int speed);

  trickplay_status_t CurrentStatus();
  trickplay_status_t RequestedStatus();
  
protected:
  virtual void InitWork();
  virtual void DoWork() = 0;
  virtual void StopWork();
  virtual void CleanWork();

  C_Module *            m_pModule;
  C_Broadcast*          m_pBroadcast;
  unsigned int          m_iInitFill;
  I_TsPacketHandler*    m_pHandler;
  C_EventHandler*       m_pEventHandler;
  C_NetList*            m_pTsProvider;
  C_MpegConverter*      m_pConverter;
  
  trickplay_status_t    m_iTrickPlayStatus;
  trickplay_status_t    m_iRequestTrickPlayStatus;

  C_Condition           m_cResumeCond;
  int m_iRequestedSpeed;// Requested speed of trickplay
                        // < 0 rewind or backwards play
                        // 0   pause (suspend) playing
                        // 1   play normal or single speed
                        // > 1 forward playing with .. speed
  bool m_bStop;                      
};



// Declaration and implementation of C_TrickPlayModule which has to be
// inherited by each trickplay filter module => C_????TrickPlayModule
DECLARE_VIRTUAL_MODULE(TrickPlay, "trickplay", C_TrickPlayConfig&);

#else
#error "Multiple inclusions of trickplay.h"
#endif
