/*******************************************************************************
* mpeg.h: MPEG usefull constants and macros
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: mpeg.h,v 1.3 2002/08/30 22:23:08 massiot Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _MPEG_H_
#define _MPEG_H_


//------------------------------------------------------------------------------
// PES' main Ids
//------------------------------------------------------------------------------
#define PES_ID_END_OF_PS        0xb9
#define PES_ID_PACK_HEADER      0xba
#define PES_ID_SYSTEM_HEADER    0xbb
#define PES_ID_PGRM_MAP         0xbc
#define PES_ID_PRIVATE_1        0xbd
#define PES_ID_PADDING          0xbe
#define PES_ID_PRIVATE_2        0xbf
#define PES_ID_MIN_AUDIO        0xc0
#define PES_ID_MAX_AUDIO        0xdf
#define PES_ID_MIN_VIDEO        0xe0
#define PES_ID_MAX_VIDEO        0xef
#define PES_ID_PGRM_DIR         0xff

#define IsDataPesId(uiPesId)                                                   \
            (    (    ((uiPesId) >= PES_ID_MIN_AUDIO)                          \
                   && ((uiPesId) <= PES_ID_MAX_VIDEO))                         \
              || ((uiPesId) == PES_ID_PRIVATE_1))

//------------------------------------------------------------------------------
// PES' private Ids
//------------------------------------------------------------------------------
#define PES_PRIV_ID_MIN_SPU     0x20
#define PES_PRIV_ID_MAX_SPU     0x3f
#define PES_PRIV_ID_MIN_AC3     0x80
#define PES_PRIV_ID_MAX_AC3     0x8f
#define PES_PRIV_ID_MIN_LPCM    0xa0
#define PES_PRIV_ID_MAX_LPCM    0xaf

//------------------------------------------------------------------------------
// PES headers
//------------------------------------------------------------------------------
#define PES_START_CODE          0x00000100

#define PES_H_END_OF_PS         ( PES_START_CODE | PES_ID_END_OF_PS     )
#define PES_H_PACK_HEADER       ( PES_START_CODE | PES_ID_PACK_HEADER   )
#define PES_H_SYSTEM_HEADER     ( PES_START_CODE | PES_ID_SYSTEM_HEADER )
#define PES_H_PGRM_MAP          ( PES_START_CODE | PES_ID_PGRM_MAP      )
#define PES_H_PRIVATE_1         ( PES_START_CODE | PES_ID_PRIVATE_1     )
#define PES_H_PADDING           ( PES_START_CODE | PES_ID_PADDING       )
#define PES_H_PRIVATE_2         ( PES_START_CODE | PES_ID_PRIVATE_2     )
#define PES_H_MIN_AUDIO         ( PES_START_CODE | PES_ID_MIN_AUDIO     )
#define PES_H_MAX_AUDIO         ( PES_START_CODE | PES_ID_MAX_AUDIO     )
#define PES_H_MIN_VIDEO         ( PES_START_CODE | PES_ID_MIN_VIDEO     )
#define PES_H_MAX_VIDEO         ( PES_START_CODE | PES_ID_MAX_VIDEO     )
#define PES_H_PGRM_DIR          ( PES_START_CODE | PES_ID_PGRM_DIR      )

#define IsDataPesHeader(uiPesHeader)                                           \
            (    (    ((uiPesHeader) >= PES_H_MIN_AUDIO)                       \
                   && ((uiPesHeader) <= PES_H_MAX_VIDEO))                      \
              || ((uiPesHeader) == PES_H_PRIVATE_1))

//------------------------------------------------------------------------------
// Packet definition
//------------------------------------------------------------------------------
#define START_CODE_LEN          4
#define PES_SIZE_LEN            2
#define MPEG1_PACK_HEADER_LEN   8 + START_CODE_LEN
#define MPEG2_PACK_HEADER_LEN   10 + START_CODE_LEN
#define SYSTEM_HEADER_LEN       8 + START_CODE_LEN

//------------------------------------------------------------------------------
// Parsing 
//------------------------------------------------------------------------------
#define LOOK_AHEAD_LEN          START_CODE_LEN + PES_SIZE_LEN


//------------------------------------------------------------------------------
// TS
//------------------------------------------------------------------------------
#define TS_PACKET_LEN 188

#define TS_TYPE_NULL            0x00
#define TS_TYPE_MPEG1_VIDEO     0x01
#define TS_TYPE_MPEG2_VIDEO     0x02
#define TS_TYPE_MPEG1_AUDIO     0x03
#define TS_TYPE_MPEG2_AUDIO     0x04
#define TS_TYPE_MPEG2_PRIVATE   0x05
//WARNING : these have been remapped from 0x81-0x83, because the current
//implementation is buggy : we should remove the first bytes of the PES
//payload, when reading from a DVD (PES private ID is removed because it
//is redundant) --Meuuh 2002-08-30
#define TS_TYPE_AC3             0x91
#define TS_TYPE_SPU             0x92 // may break a norm
#define TS_TYPE_LPCM            0x93 // may break a norm

//------------------------------------------------------------------------------
// RTP
//------------------------------------------------------------------------------
#define RTP_HEADER_LEN 12


#else
#error "Multiple inclusions of mpeg.h"
#endif

