/*******************************************************************************
* rtp.h: RtpHeader class definition
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Tristan Leteurtre <tooney@via.ecp.fr>
*          Jean-Paul Saman <saman@natlab.research.philips.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _RTP_H_
#define _RTP_H_

/* RTP Header length is defined in bytes. */

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_RtpHeader
{
 public:
  C_RtpHeader() { m_iRefCount = 0; };

  // Direct access to the buffer. Provided only for efficiency reasons
  operator byte* ();

  // By hand packet header construction
  u8 BuildHeader(u16 iCounter);
  void SetRtpDiscontinuity( bool bValue = true );

  void SetRtpTimeStamp(u32 time);
  u32 GetRtpTimeStamp(void);

  void PrintPacket();

  // Reference counter
  unsigned int Ref()
  {
    ASSERT(m_iRefCount + 1);
    return ++m_iRefCount;
  };
  unsigned int Unref()
  {
    ASSERT(m_iRefCount);
    return --m_iRefCount;
  };
  unsigned int RefCount() const
  {
    return m_iRefCount;
  }

 protected:

  byte bData[RTP_HEADER_LEN];
  unsigned int m_iRefCount;

};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class I_RtpHeaderHandler
{
public:
  virtual void HandlePacket(C_RtpHeader* pPacket) = 0;
  virtual ~I_RtpHeaderHandler(void) { }

  bool operator == (const I_RtpHeaderHandler& cHandler) const
  {
    return (this == &cHandler);
  };
};


#else
#error "Multiple inclusions of rtp.h"
#endif

