/*******************************************************************************
* ts.cpp: TS packet manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: ts.cpp,v 1.11 2003/10/27 10:58:11 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Jean-Paul Saman <jpsaman@wxs.n>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "mpeg.h"
#include "ts.h"



//******************************************************************************
// C_TsPacket class
//******************************************************************************
//
//******************************************************************************

C_TsPacket::~C_TsPacket()
{
};

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Give direct access to the raw buffer
//------------------------------------------------------------------------------
C_TsPacket::operator byte* ()
{
  return bData;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Mark the packet as being invalid
//------------------------------------------------------------------------------
void C_TsPacket::IncrementCounter()
{
  u8 iByte = bData[3];
  bData[3] = ((iByte + 1) & 0x0F) | (iByte & 0xF0);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Mark the packet as being invalid
//------------------------------------------------------------------------------
void C_TsPacket::SetErrorFlag(bool bError/* = true*/)
{
  if(bError)
    bData[1] &= 0xFF;
  else
    bData[1] &= 0x7F;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Set the discontinuity flag. This is only possible if the packet carries an
// adaption field
// Return false if the flag could not be set
//------------------------------------------------------------------------------
bool C_TsPacket::SetDiscontinuityFlag(bool bDiscontinuity/* = true*/)
{
  bool bDone = false;

  // The job has to be done only if the flag must be set to true
  if(bDiscontinuity)
  {
    // Check that the adaptation_field_control is set and that the
    // adaptation_field_length is not 0
    if((bData[3] & 0x20) && bData[4])
    {
      // Set the discontinuity indicator
      bData[5] |= 0x80;
      bDone = true;
    }
  }
  else
  {
    // The flag can't be cleared
    ASSERT(false);
  }
  
  return bDone;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Get the PID
//------------------------------------------------------------------------------
u16 C_TsPacket::GetPid() const
{
  return ((u16)(bData[1] & 0x1f) << 8) + bData[2];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Look for a PCR field in the TS header
//------------------------------------------------------------------------------
bool C_TsPacket::HasPCR() const
{
  // Return true only if adaptation_field_control is set,
  // adaptation_field_length is not 0 and PCR_flag is set
  return ((bData[3] & 0x20) && bData[4] && (bData[5] & 0x10));
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_TsPacket::IsDiscontinuity() const
{
  // Return true only if adaptation_field_control is set,
  // adaptation_field_length is not 0 and discontinuity_flag is set
  return ((bData[3] & 0x20) && bData[4] && (bData[5] & 0x80));
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_TsPacket::HasDiscontinuityFlag() const
{
  return ((bData[3] & 0x20) && bData[4]);
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Extracts and converts the PCR to return a date in microseconds
//------------------------------------------------------------------------------
s64 C_TsPacket::GetPCRTime() const
{
  ASSERT(HasPCR());                   // Make sure that the packet carries a PCR

#ifdef SOLARIS
  // We need to align the data to avoid a bus error
  s64 i = *((s64*)(bData+4));
  i = i >> 16;
  i = U32_AT(i);
  return ((( i << 1) | (bData[10] >> 7)) * 300) / 27;

#else
  return ((( ((s64)U32_AT(bData[6])) << 1) | (bData[10] >> 7)) * 300) / 27;
#endif
}

u16 C_TsPacket::GetPMTPid( int number )
{
  ASSERT( number );
  ASSERT( GetPid()==0 );
  // protection against multisection pat
  ASSERT( (bData[4]==0) && (bData[5]==0) );

  if ( !HasAdaptationField() )
  {
    int id;
    int pmtPid;
    int nprograms;
    nprograms = ((bData[6]&0x0f <<8) + bData[7]-9)/4;
    number--;
    if (number<nprograms)
    {
      id = (bData[13+4*number] << 8) | bData[14+4*number] ;
      pmtPid = ((bData[15+4*number]&0x1f) << 8) + bData[16+4*number];
      return pmtPid;
    }
  }
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// GetLength of Payload.
//------------------------------------------------------------------------------
u8 C_TsPacket::GetPayloadLength() const
{
  if (!HasAdaptationField())
    return (TS_PACKET_LEN - 4); //184
  else
    return (TS_PACKET_LEN - 5 - GetAdaptationFieldLength());
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Returns a pointer to the start of the payload
//------------------------------------------------------------------------------
byte *C_TsPacket::GetPayloadStart()
{
  if (!HasAdaptationField())
    return (&bData[4]);
  else
  {
    u8 pos = 5 + GetAdaptationFieldLength();
    ASSERT( pos <= TS_PACKET_LEN );
    return (&bData[pos]);
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// PayloadUnitIndicator is 1 when a new PES packet starts.
//------------------------------------------------------------------------------
bool C_TsPacket::HasPayloadStartIndicator() const
{
  return (bData[1] & 0x40);
}

//------------------------------------------------------------------------------
// HasContinuityCounter
//------------------------------------------------------------------------------
// HasContinuityCounter is set deducted from adaptation field control values.
// It returns true when there is a payload. Otherwise it returns false.
//
//  The following meaning is given to the two bit values:
// 00 - reserved for future use by ISO/IEC
// 01 - no adaptation field, payload only
// 10 - adaptation field only, no payload
// 11 - adaptation field followed by a payload.
//------------------------------------------------------------------------------
bool C_TsPacket::HasContinuityCounter() const
{
  u8 iByte = ((bData[3] & 0xbf) >> 4);

  return ((iByte == 1) || (iByte==3));
}

bool C_TsPacket::IsNullPacket() const
{
  // 0x1fff == 8191 means NULL Packet.
  return (GetPid() == 8191);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Adaptation field detection.
//------------------------------------------------------------------------------
bool C_TsPacket::HasAdaptationField() const
{
  u8 iByte = bData[3];
  return (iByte & 0x20);
}

u8 C_TsPacket::GetAdaptationFieldLength() const
{
  if (HasAdaptationField())
    return (bData[4]);
  else
    return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// GetContinuityCounter
//------------------------------------------------------------------------------
u8 C_TsPacket::GetContinuityCounter() const
{
  u8 iByte = bData[3];
  return(iByte & 0x0F);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// SetContinuityCounter
//------------------------------------------------------------------------------
u8 C_TsPacket::SetContinuityCounter(int old_counter)
{
  int new_counter = (old_counter+1)%16;
  bData[3] = (bData[3]&0xF0) + new_counter;
  return new_counter;
}


//------------------------------------------------------------------------------
// BuildHeader: Build a TS header and return its size
//------------------------------------------------------------------------------
// Beware: only the 13 first bits of the iPid field are taken into account, and
// only the 4 first bits of the iCounter are used
// No adaption field can yet be put in the header
// The method returns the length of the header
//------------------------------------------------------------------------------
u8 C_TsPacket::BuildHeader(u16 iPid, bool bUnitStart, u8 iCounter)
{
  // For a PID is 13 bytes max
  ASSERT((iPid & 0xE000) == 0);

  // Sync byte
  bData[0] = 0x47;
  // Set PID value and both error indicator and priority to 0
  SET_U16_TO(bData[1], iPid & 0x1FFF);
  // Payload unit start
  bData[1] |= bUnitStart << 6;
  // Continuity counter and no scrambling
  bData[3] = iCounter & 0x0F;
  // Adaption field control: always payload and never adaption field
  bData[3] |= 0x10;

  return 4;
}




//------------------------------------------------------------------------------
// BuildAdaptionField: Build an adaption field for TS header and return its size
//------------------------------------------------------------------------------
// Append a PCR base to the adaption field. Overwrite all data that can follow
// the header, so this method is to use before writing the actual payload
//------------------------------------------------------------------------------
u8 C_TsPacket::BuildAdaptionField(u64 iPCR)
{
  // Adaption field control: adaption field is present
  bData[3] |= 0x20;

  // Adaption field length
  bData[4] = 1 + 6 /*PCR*/;

  // PCR_base is the only extension present in the adaption field
  bData[5] = 0x10;

  // PCR_base value
  SET_U32_TO(bData[6], (iPCR >> 1) & 0xFFFFFFFF);

  /* last bit of PCR, and PCR_ext to 0 */
  bData[10] = (iPCR & 0x01) << 7;
  bData[11] = 0;

  return 4+2+6;
}

u8 C_TsPacket::FillWith(byte * pBuff, int iBeginning, int iBuffLength)
{
  ASSERT(iBeginning + iBuffLength <= TS_PACKET_LEN);
  memcpy(bData + iBeginning, pBuff, iBuffLength);
  return 0;
}


//------------------------------------------------------------------------------
// AddStuffingBytes: Add stuff to complete the TS packet
//------------------------------------------------------------------------------
// Modify the TS header so that the necessary stuffing bytes are added to
// complete the payload
// Overwrite all data that can follow the header and the adaption field, so
// this method is to use before writing the actual payload in the packet
//------------------------------------------------------------------------------
u8 C_TsPacket::AddStuffingBytes(u8 iPayloadLen)
{
  // Check that the given payload length can fit in the packet and that it
  // is not big enough to fill it completely
  ASSERT(iPayloadLen < TS_PACKET_LEN - 4 - ((bData[3]&0x20)?bData[4]:0));

  if(bData[3] & 0x20)
  {
    // There is an adaption field, we just have to extend it
    ASSERT(bData[4] > 0);       // (Empty adaption fields are for stuffing only)
    ASSERT(TS_PACKET_LEN - 4 - 1 - bData[4] - iPayloadLen > 0);
    u8 iStuffStart = 4+1+bData[4];
    bData[4] += TS_PACKET_LEN - iStuffStart - iPayloadLen;
    for(u8 i = 0; i < bData[4]; i++)
      bData[iStuffStart+i] = 0xFF;
  }
  else
  {
    // We must create the adaption field
    bData[3] |= 0x20;
    ASSERT(TS_PACKET_LEN - 4 - 1 - iPayloadLen >= 0);
    bData[4] = TS_PACKET_LEN - 4 - 1 - iPayloadLen;
    if(bData[4] > 0)
    {
      // We won't put any extension in the header
      bData[5] = 0;
      // Now write the stuffing bytes
      for(u8 i = 0; i < bData[4]; i++)
        bData[6+i] = 0xFF;
    }
  }

  // Check that we didn't made any error
  ASSERT(4+1+bData[4] + iPayloadLen == TS_PACKET_LEN);
  
  return TS_PACKET_LEN - iPayloadLen;
}


//******************************************************************************
// C_PsiSection class
//******************************************************************************
// In the MPEG2 system norm, a section can be split into several TS packets, but
// as this is really unuseful, our implementation don't allow sections biggest
// than the size of the available payload of a TS packet.
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_PsiSection::C_PsiSection()
{
  // Init the CRC encoder
  BuildCrc32Table();

  ZERO(m_iPsiStart);
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
/*C_PsiSection::~C_PsiSection()
{
}*/


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
u8 C_PsiSection::BuildHeader(u16 iPid, u8 iCounter, u8 iTableId, u16 iPsiId,
                             u8 iVersion, bool bCurrentNext, u8 iCurrentSection,
                             u8 iLastSection)
{
  // For a PID is 13 bits max
  ASSERT((iPid & 0xE000) == 0);
  // For the version is 5 bits max
  ASSERT((iVersion & 0xE0) == 0);

  // First build the TS header: the unit_start indicator will be set to 1
  // indicating that a PSI section begins in the packet
  m_iPsiStart = C_TsPacket::BuildHeader(iPid, true, iCounter);

  // Now add the pointer field immediatly after the header: as we don't know
  // its size yet, the payload will began immediatly after the pointer field
  // and its end will be marked by 0xFF stuffing bytes
  bData[m_iPsiStart] = 0;
  m_iPsiStart++;

  // Table id
  bData[m_iPsiStart] = iTableId;

  // Set section length to the minimum (id est no payload)
  SET_U16_TO(bData[m_iPsiStart+1], 9 & 0xFFF);

  // Section syntax set to one
  bData[m_iPsiStart+1] |= 0x80;

  // Id 
  SET_U16_TO(bData[m_iPsiStart+3], iPsiId);

  // Versioning
  bData[m_iPsiStart+5] = (iVersion) << 1;
  bData[m_iPsiStart+5] |= (bCurrentNext & 0x01);

  // Section management
  bData[m_iPsiStart+6] = iCurrentSection;
  bData[m_iPsiStart+7] = iLastSection;

  // 
  m_bIsModified = true;
  
  return m_iPsiStart + 8;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
byte* C_PsiSection::AppendData(u8 iDataLen)
{
  byte* pDataPos = NULL;
  
  u8 iPsiLen = (U16_AT(bData[m_iPsiStart+1]) & 0xFFF) + 3;
  if(iPsiLen + m_iPsiStart + iDataLen <= TS_PACKET_LEN)
  {
    // 
    pDataPos = bData+m_iPsiStart+iPsiLen-4;
    // New length
    SET_U16_TO(bData[m_iPsiStart+1], (iDataLen+iPsiLen-3 & 0xFFF));
    // Section syntax indicator must be kept to 1
    bData[m_iPsiStart+1] |= 0x80;

    // 
    m_bIsModified = true;
  }
  
  return pDataPos;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
byte* C_PsiSection::GetDataForUpdate(u8 iDataPos)
{
  ASSERT(iDataPos < m_iPsiStart + (U16_AT(bData[m_iPsiStart+1]) & 0xFFF) + 3);

  m_bIsModified = true;
  
  return bData + m_iPsiStart + iDataPos;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const byte* C_PsiSection::GetDataForRead(u8 iDataPos) const
{
  ASSERT(iDataPos < m_iPsiStart + (U16_AT(bData[m_iPsiStart+1]) & 0xFFF) + 3);
  return bData + m_iPsiStart + iDataPos;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_PsiSection::Finalize()
{
  if(m_bIsModified)
  {
    UpdateVersion();
    ComputeCrc32();
    AddFinalStuffing();

    m_bIsModified = false;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_PsiSection::UpdateVersion()
{
  // The version number is left-shifted of 1 bit, and is coded on 5 bits  
  u8 iVersion = bData[m_iPsiStart+5] & 0x3E;
  iVersion = (iVersion + 2) & 0x3E;
  u8 iCurrentNext = bData[m_iPsiStart+5] & 0x1;
  
  bData[m_iPsiStart+5] = iVersion | iCurrentNext;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_PsiSection::BuildCrc32Table()
{
  u32 i, j, k;
  for( i = 0 ; i < 256 ; i++ )
  {
    k = 0;
    for (j = (i << 24) | 0x800000 ; j != 0x80000000 ; j <<= 1)
      k = (k << 1) ^ (((k ^ j) & 0x80000000) ? 0x04c11db7 : 0);
    iCrc32Table[i] = k;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_PsiSection::ComputeCrc32()
{
  u32 iCrc = 0xffffffff;
  u8 iPsiLen = (U16_AT(bData[m_iPsiStart+1]) & 0xFFF) + 3 - 4;
  for(u8 i = m_iPsiStart; i < m_iPsiStart+iPsiLen; i++)
    iCrc = (iCrc << 8) ^ iCrc32Table[(iCrc >> 24) ^ bData[i]];

  SET_U32_TO(bData[m_iPsiStart+iPsiLen], iCrc);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_PsiSection::AddFinalStuffing()
{
  u8 iStuffStart = (U16_AT(bData[m_iPsiStart+1])&0xFFF)+m_iPsiStart+3;
  memset(bData+iStuffStart, 0xFF, TS_PACKET_LEN - iStuffStart);
}



//******************************************************************************
// C_Pat class
//******************************************************************************
// A PAT can contain the description of several program, but as we just want to
// build PAT for one program, this class has been restricted
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Pat::BuildHeader(u16 iStreamId, u8 iVersion, bool bCurrentNext)
{
  C_PsiSection::BuildHeader(0, 0, 0, iStreamId, iVersion, bCurrentNext, 0, 0);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int C_Pat::AddPgrm(u16 iPgrmNumber, u16 iPmtPid)
{
  int iRc = NO_ERR;
  
  byte* pPgrmDescr = AppendData(4);
  
  if(pPgrmDescr)
  {
    SET_U16_TO(pPgrmDescr[0], iPgrmNumber);
    SET_U16_TO(pPgrmDescr[2], iPmtPid);
  }
  else
  {
    // There was not enougn sace left on the packet
    iRc = GEN_ERR;
  }
  
  return iRc;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Pat::Write(C_TsPacket* pPacket)
{
  ASSERT(pPacket);

  // 
  Finalize();
  
  // Copy the PAT in the TS packet
  memcpy((byte*)(*pPacket), bData, TS_PACKET_LEN);

  // Increment the continuity counter
  IncrementCounter();
}






//******************************************************************************
// C_Pmt class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Pmt::BuildHeader(u16 iPid, u16 iPgrmNumber, u16 iPcrPid, u8 iVersion,
                        bool bCurrentNext/* = true*/)
{
  ASSERT((iPcrPid & 0xE000) == 0);

  C_PsiSection::BuildHeader(iPid, 0, 2, iPgrmNumber, iVersion,
                            bCurrentNext, 0, 0);

  // Set the PCR pid and the no pgrm info (->Pgrm_info_length is 0)
  byte* pAdditionalFields = AppendData(4);
  ASSERT(pAdditionalFields);
  SET_U16_TO(pAdditionalFields[0], iPcrPid);
  SET_U16_TO(pAdditionalFields[2], 0);
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
u16 C_Pmt::GetPcrPid() const
{
  const byte* pPcrPid = GetDataForRead(8);
  return U16_AT(pPcrPid[0]);
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Pmt::UpdatePcrPid(u16 iNewPid)
{
  ASSERT((iNewPid & 0xE000) == 0);
  
  byte* pPcrPid = GetDataForUpdate(8);
  SET_U16_TO(pPcrPid[0], iNewPid);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int C_Pmt::AddEs(u8 iType, u16 iPid/*, const C_Vector<C_Descriptor*>& cDescriptors*/)
{
  ASSERT((iPid & 0xE000) == 0);

  int iRc = NO_ERR;
  
  byte* pEsDescr = AppendData(5);
  if(pEsDescr)
  {
    pEsDescr[0] = iType;
    SET_U16_TO(pEsDescr[1], iPid);
    SET_U16_TO(pEsDescr[3], 0);
  }
  else
  {
    // Not enough space left on TS packet
    iRc = GEN_ERR;
  }

  return iRc;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Pmt::Write(C_TsPacket* pPacket)
{
  ASSERT(pPacket);

  //
  Finalize();
  
  // Copy the PAT in the TS packet
  memcpy((byte*)(*pPacket), bData, TS_PACKET_LEN);

  // Increment the continuity counter
  IncrementCounter();
}
