/*******************************************************************************
* streamdescr.h: MPEG stream description class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: streamdescr.h,v 1.2 2002/05/14 22:10:08 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _STREAMDESCR_H_
#define _STREAMDESCR_H_


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_ElementDescriptor
{
 public:
  C_ElementDescriptor(u8 iId, dvbpsi_pmt_es_t *pDvbPsiEs)
  {
    ASSERT(pDvbPsiEs);

    m_iId = iId;
    m_pDvbPsiEs = pDvbPsiEs;
    m_iCounter = 0;
  }
  
  void IncreaseCounter()
  { m_iCounter = (m_iCounter+1)&0x0F; }
  
  u8 GetCounter() const
  { return m_iCounter; }

  u8 GetId() const
  { return m_iId; }

  u16 GetPid() const
  { return m_pDvbPsiEs->i_pid; }

  void AddLanguageCode(const char *pszCode);

 protected:
  u8 m_iId;
  dvbpsi_pmt_es_t *m_pDvbPsiEs;
  u8 m_iCounter;
};



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_ProgramDescriptor
{
 public:
  // 
  C_ProgramDescriptor(unsigned int iMpegVersion);

  // Create a descriptor for the given stream
  C_ElementDescriptor* AddES(u8 iId, u8 iSubId = 0);

  // Get the descriptor for the given stream
  C_ElementDescriptor* GetDescriptor(u8 iId, u8 iSubId = 0);

  u16 GetPcrPid()
  {
    return m_sPmt.i_pcr_pid;
  }

  dvbpsi_pat_t m_sPat;
  dvbpsi_pmt_t m_sPmt;

  bool m_bNewPmt;

 private:
  u16 GetNewPid()
  { return m_iNextPid++; }

  u16 m_iNextPid;

  C_ElementDescriptor* m_apElementaryStreams[0xFF];
  C_ElementDescriptor* m_apP1ElementaryStreams[0xFF];

  unsigned int m_iMpegVersion;
};


#else
#error "Multiple inclusions of streamdescr.h"
#endif

