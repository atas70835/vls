/*******************************************************************************
* ps2ts.cpp: MPEG1 and MPEG2 PS to MPEG2 TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Andrew de Quincey <adq@lidskialf.net>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* TO DO: Fonction de lecture d'un TS -> Si erreur, alors le flag bEror est mis
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at vector.h for further explanation



//------------------------------------------------------------------------------
// Local definitions
//------------------------------------------------------------------------------

// Status
#define SKIPPED_DATA            -97
#define UNKNOWN_DATA            -98
#define END_OF_STREAM           -99

#define SYNC_MAX_LENGTH         (1024*1024)

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
C_Ps2Ts<Reader, TsProvider>::C_Ps2Ts(Reader* pReader, TsProvider* pTsProvider,
                                      unsigned int iMaxBufferedTs,
                                      unsigned int iMpegVersion,
                                      int iLoop,
                                      handle hLog) :
                            m_cPgrmDescriptor(iMpegVersion),
                            m_cPat(0, 0, 0), m_cPmt(0, 0, 0, 0, 0x50),
                            m_cPendingTS(NO)
{
  ASSERT(pReader);
  ASSERT(pTsProvider);
  ASSERT(iMaxBufferedTs > 1);
  ASSERT(iMpegVersion == 1 || iMpegVersion == 2);

  m_pReader = pReader;
  m_pTsProvider = pTsProvider;
  m_iMaxBufferedTs = iMaxBufferedTs;

  m_iDataType = UNKNOWN_DATA;
  m_iPrivateId = 0;
  m_iStatus = NO_ERR;
  m_bDiscontinuity = false;

  m_bSendPSI = m_bGenPat = m_bGenPmt = true;
  m_iTSCounter = 0;
  m_bNeedToSendPCR = true;
  m_iNextPCR = 0;
  m_iPrevPCR = 0;
  m_iLoop = iLoop;
  m_bInMiddleOfPES = false;
  m_bJustSynched = false;
  m_hLog = hLog;

  if(iMpegVersion == 1)
    m_iPackHeaderLen = MPEG1_PACK_HEADER_LEN - START_CODE_LEN;
  else
    m_iPackHeaderLen = MPEG2_PACK_HEADER_LEN - START_CODE_LEN;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
C_Ps2Ts<Reader, TsProvider>::~C_Ps2Ts()
{
  // The packets belong to the netlist, so don't delete them twice
  while(m_cPendingTS.Size() > 0) {
    C_TsPacket* pPacket = m_cPendingTS.Remove(0);
    if (pPacket) {
      m_pTsProvider->ReleasePacket(pPacket);
    }
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The data in the buffer can be anything: only rely on DataType
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::Synch()
{
  int iRc = NO_ERR;
  u8 headerCode[4];
  u64 startPos;

  // prefill the headercode
  startPos = m_pReader->GetPos();
  if ((iRc = ReadData(headerCode, 3)) != 0) {
    m_iStatus = iRc;
    return m_iStatus;
  }

  // loop, trying to find a PES_H_PACK_HEADER
  while(true) {
    // read the last byte of the headercode
    if ((iRc = ReadData(headerCode+3, 1)) != 0) {
      m_iStatus = iRc;
      break;
    }

    // found it?
    if (U32_AT(headerCode) == PES_H_PACK_HEADER) {
      m_bInMiddleOfPES = false;
      m_bJustSynched = true;
      m_bNeedToSendPCR = false;
      m_iNextPCR = 0;
      m_iPrevPCR = 0;
      break;
    }

    // OK, move the topmost 3 bytes into position
    memcpy(headerCode, headerCode+1, 3);

    // just give up if we haven't found one within a reasonable distance
    if ((m_pReader->GetPos() - startPos) > SYNC_MAX_LENGTH) {
      Log(m_hLog, LOG_ERROR, "Failed to find start of MPEG file");
      m_iStatus = GEN_ERR;
      break;
    }
  }

  return m_iStatus;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  C_TsPacket* C_Ps2Ts<Reader, TsProvider>::GetPacket(bool bPreparsing)
{
  int iRc = NO_ERR;
   
  // if there's nothing left in the buffer. we need to fill it
  if (m_cPendingTS.Size() == 0) {

    // keep looping until the buffer is full
    while(m_cPendingTS.Size() < m_iMaxBufferedTs) {

      // If we're not already sending PSI, check if we need to send it again
      if ((!m_bSendPSI) && (!bPreparsing)) {
        // check if resend interval is up
        if (m_bSendPSI = (m_iTSCounter % 500 <= m_iMaxBufferedTs)) {
          m_cPat.TsReset();
          m_cPmt.TsReset();
        }

        // check if a new PMT has appeared
        if (m_cPgrmDescriptor.m_bNewPmt) {
          m_bSendPSI = true;
          m_bGenPmt = true;
          m_cPgrmDescriptor.m_bNewPmt = false;
        }
      }

      // Send PSI packets if necessary
      if ((m_bSendPSI) && (!bPreparsing)) {
        // first of all regenerate any of the PSI information necessary
        if(m_bGenPat) {
          *m_cPat.GetLowLevelPat() = m_cPgrmDescriptor.m_sPat;
          m_cPat.Generate();
          m_cPat.GetLowLevelPat()->p_first_program = NULL;
          m_bGenPat = false;
          m_cPat.TsReset();
        }
        if(m_bGenPmt) {
          m_cPgrmDescriptor.m_sPmt.i_version =
            (m_cPgrmDescriptor.m_sPmt.i_version + 1) & 0x1f;
          *m_cPmt.GetLowLevelPmt() = m_cPgrmDescriptor.m_sPmt;
          m_cPmt.Generate();
          m_cPmt.GetLowLevelPmt()->p_first_es = NULL;
          m_cPmt.GetLowLevelPmt()->p_first_descriptor = NULL;
          m_bGenPmt = false;
          m_cPmt.TsReset();
        }

        // output PSI information as necessary
        if (m_cPat.TsHasNext()) {
          C_TsPacket* pPacket = m_pTsProvider->GetPacket();
          if (pPacket == NULL) {
            m_iStatus = GEN_ERR;
            break;
          }

          m_cPat.TsWrite(pPacket);
          m_cPendingTS.PushEnd(pPacket);
          m_iTSCounter++;
        } else if (m_cPmt.TsHasNext()) {
          C_TsPacket* pPacket = m_pTsProvider->GetPacket();
          if (pPacket == NULL) {
            m_iStatus = GEN_ERR;
            break;
          }

          m_cPmt.TsWrite(pPacket);
          m_cPendingTS.PushEnd(pPacket);
          m_iTSCounter++;
        } else {
          // we've finished sending PSI for this time
          m_bSendPSI = false;
        }
      }

      // not sending PSI => grab some REAL data packets
      if ((!m_bSendPSI) || (bPreparsing)) {
        // fetch some packets
        if ((iRc = FetchPackets(bPreparsing)) != 0) {
          // special handling for some return codes
          switch(iRc) {
          case UNKNOWN_DATA:
            iRc = Synch();
            break;

          case SKIPPED_DATA:
            iRc = 0;
            break;

          case END_OF_STREAM:
            if (m_iLoop) {
              // try and reset the stream
              m_pReader->ResetEndOfStream();

              // if that failed, the reader must not support
              // resetting the end of stream. We have to stop.
              if (m_pReader->EndOfStream()) {
                break;
              }

              // OK, synch with the stream again. Loop will continue sending packets
              iRc = Synch();
              m_bSendPSI = true;
              m_bGenPat = true;
              m_bGenPmt = true;
              m_iLoop--;
            }
            break;
          }

          // OK, was there really a problem?
          if (iRc) {
            m_iStatus = iRc;
            break;
          }
        }

        // if we're preparsing, exit as soon as we have one packet
        if (bPreparsing && m_cPendingTS.Size()) {
          break;
        }
      }
    }
  }
   
  // Return the first packet
  if(m_cPendingTS.Size() > 0)
    return m_cPendingTS.Remove(0);
  else
    return NULL;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::FetchPackets(bool bPreparsing)
{
  int iRc = NO_ERR;

  // if we're in the middle of parsing a PES packet,
  // just do that immediately with no further ado
  if (m_bInMiddleOfPES) {
    return ParsePES(bPreparsing);
  }

  // attempt to read in the start code/ data type
  if (!m_bJustSynched) {
    if ((iRc = ReadData(m_bBuff, START_CODE_LEN)) != 0) {
      return iRc;
    }
    m_iDataType = U32_AT(m_bBuff);
  } else {
    // if we've just synched we _know_ we're just after a
    // PACK_HEADER start code.
    m_bJustSynched = false;
    return ParsePackHeader(bPreparsing);
  }

  // OK, process the packet
  switch(m_iDataType) {
  case PES_H_PACK_HEADER:
    return ParsePackHeader(bPreparsing);

  case PES_H_SYSTEM_HEADER:
  case PES_H_PADDING:
  case PES_H_PGRM_MAP:
  case PES_H_PRIVATE_2:
  case PES_H_PGRM_DIR:
    return SkipPES();

  case PES_H_END_OF_PS:
    return END_OF_STREAM;

  default:
    if (IsDataPesHeader(m_iDataType)) {
      return ParsePES(bPreparsing);
    } else {
      return UNKNOWN_DATA;
    }
  }

  // should never get here
  return GEN_ERR;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::ParsePackHeader(bool bPreparsing)
{
  int iRc = NO_ERR;

  // try to read in the data for the pack header
  if ((iRc = ReadData(m_bBuff, m_iPackHeaderLen)) != 0) {
    return iRc;
  }

  // don't bother doing anything more if we're preparsing
  if (bPreparsing) {
    return iRc;
  }

  if(m_iPackHeaderLen == MPEG1_PACK_HEADER_LEN)
  {
    // Parse the SCR (MPEG1 format)
    u64 iHighBits = m_bBuff[0] & 0x0E;
    u64 iMiddleBits = U16_AT(m_bBuff[1]) & 0xFFFE;
    u64 iLowBits = U16_AT(m_bBuff[3]) & 0xFFFE;
    ASSERT((m_bBuff[0] & 0x01));
    ASSERT((m_bBuff[2] & 0x01));
    ASSERT((m_bBuff[4] & 0x01));

    // store it for when we need to output it
    m_iNextPCR = iHighBits << 29 | iMiddleBits << 14 | iLowBits >> 1;
    m_bNeedToSendPCR = true;
  }
  else
  {
    // Parse the SCR (MPEG2 format)
    u64 iHighBits = m_bBuff[0] & 0x38;
    u64 iMiddleBits = U32_AT(m_bBuff[0]) & 0x03FFF800;
    u64 iLowBits = U32_AT(m_bBuff[2]) & 0x03FFF800;
    ASSERT((m_bBuff[0] & 0x4));
    ASSERT((m_bBuff[2] & 0x4));
    ASSERT((m_bBuff[4] & 0x4));

    // store it for when we need to output it
    m_iNextPCR = iHighBits << 27 | iMiddleBits << 4 | iLowBits >> 11;
    m_bNeedToSendPCR = true;

    // Read additional stuffing bytes if any
    int iStuffLen = m_bBuff[m_iPackHeaderLen-1] & 0x7;
    if (iStuffLen) {
      iRc = SkipData(iStuffLen);
    }
  }

  // check for discontinuity
  if (m_iPrevPCR >= m_iNextPCR) {
    m_bDiscontinuity = true;
  }
  m_iPrevPCR = m_iNextPCR;

  // finished
  return iRc;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::ParsePES(bool bPreparsing)
{
  int iPos = 0;
  int iPesPos = 0;
  int iRc = NO_ERR;

  // read size of the PES packet
  if (!m_bInMiddleOfPES) {
    if ((iRc = ReadDataLength(&m_iDataLen)) != 0) {
      return iRc;
    }
  }

  // Extract the current TS descriptor.
  C_ElementDescriptor* curDesc;
  if ((m_iDataType & 0xff) != PES_ID_PRIVATE_1) {
    curDesc = m_cPgrmDescriptor.GetDescriptor(m_iDataType & 0xFF);
  } else {
    // Is an encapsulated private stream.. special handling needed
    // if we're at the start of a PES packet, read (and cache) the
    // header and find the private stream ID
    if (!m_bInMiddleOfPES) {
      // read in the start of the PES header
      if ((iRc = ReadData(m_bCache, 3)) != 0) {
        return iRc;
      }
      m_iCacheLen = 3;

      // read in the rest of the PES header
      if ((iRc = ReadData(m_bCache + m_iCacheLen, m_bCache[2])) != 0) {
        return iRc;
      }
      m_iCacheLen += m_bCache[2];

      // read in the start of the ES header
      u8* esHeader = m_bCache + m_iCacheLen;
      if ((iRc = ReadData(m_bCache + m_iCacheLen, 1)) != 0) {
        return iRc;
      }
      m_iCacheLen += 1;

      // finally, grab the private id
      m_iPrivateId = esHeader[0];
    }

    // get the descriptor taking into account the private stream id
    curDesc = m_cPgrmDescriptor.GetDescriptor(m_iDataType & 0xFF, m_iPrivateId);
  }

  // process the PES data
  while ((m_iDataLen > 0) && (m_cPendingTS.Size() < m_iMaxBufferedTs)) {
    // check if a new PMT has appeared. If so, just exit this loop.
    // The caller will take care of generating it and possibly calling us
    // again to add in more PES data
    if ((!bPreparsing) && (m_cPgrmDescriptor.m_bNewPmt)) {
      break;
    }

    // Grab a new TS packet to use
    C_TsPacket* pPacket = m_pTsProvider->GetPacket();

    // build the start of the TS packet
    if (!m_bInMiddleOfPES) { // we're at the start of the PES packet
      // build the header
      u16 iPid =  curDesc->GetPid();
      u8 iCounter = curDesc->GetCounter();
      iPos = pPacket->BuildHeader(iPid, true, iCounter);

      // if we need to send the PCR, and we're sending a packet for the stream
      // which contains the PCR, do it!
      if ((m_bNeedToSendPCR) && (iPid == m_cPgrmDescriptor.GetPcrPid())) {
        // build the adaptation field
        iPos = pPacket->BuildAdaptionField(m_iNextPCR);
        m_bNeedToSendPCR = false;

        // handle any discontinuity
        if (m_bDiscontinuity) {
          pPacket->SetDiscontinuityFlag();
          m_bDiscontinuity = false;
        }
      }

      // write the beginning of the PES packet (since we've already read it)
      byte* pTsPayload = (byte*)(*pPacket);
      SET_U32_TO(pTsPayload[iPos], m_iDataType);
      SET_U16_TO(pTsPayload[iPos+START_CODE_LEN], m_iDataLen);
      iPesPos = START_CODE_LEN + PES_SIZE_LEN;
      iPos += iPesPos;
    } else {
      // somewhere in the middle of a PES packet
      u16 iPid = curDesc->GetPid();
      u8 iCounter = curDesc->GetCounter();
      iPesPos = 0;
      iPos = pPacket->BuildHeader(iPid, false, iCounter);
    }

    // calculate how much data we can send
    int readSize = TS_PACKET_LEN - iPos;
    
    // If there is not enough data to completely fill the TS packet,
    // add stuffing bytes
    if(m_iDataLen < readSize) {
      // add enough stuffing bytes
      iPos = pPacket->AddStuffingBytes(m_iDataLen+iPesPos);
      readSize = m_iDataLen;

      // if we're at the start of a PES packet,
      // we'll need to rewrite the start of it again as it has moved
      if (!m_bInMiddleOfPES) {
        byte* pTsPayload = (byte*)(*pPacket);
        SET_U32_TO(pTsPayload[iPos], m_iDataType);
        SET_U16_TO(pTsPayload[iPos+START_CODE_LEN], m_iDataLen);
        iPos += START_CODE_LEN + PES_SIZE_LEN;
      }
    }

    // first of all copy any data we've cached across
    if (m_iCacheLen) {
      // work out how much to read from the cache
      int cacheReadSize = m_iCacheLen;
      if (cacheReadSize > readSize) {
        cacheReadSize = readSize;
      }

      // copy it across and update
      memcpy(((byte*)(*pPacket)+iPos), m_bCache, cacheReadSize);
      readSize -= cacheReadSize;
      iPos += cacheReadSize;
      m_iDataLen -= cacheReadSize;
      m_iCacheLen -= cacheReadSize;
    }

    // OK, read the data from the stream into the TS packet
    if (readSize) {
      if ((iRc = ReadData(((byte*)(*pPacket)+iPos), readSize)) != 0) {
        if (iRc == END_OF_STREAM) {
          // generate a TS packet with the error flag set to warn
          // the decoder
          memset(((byte*)(*pPacket)+iPos), 0, readSize);
          pPacket->SetErrorFlag();
        } else {
          // some other more fatal error.. just exit right away
          m_pTsProvider->ReleasePacket(pPacket);
          return iRc;
        }
      }
      m_iDataLen -= readSize;
    }

    // Put the TS packet in the list of pending TS
    m_cPendingTS.PushEnd(pPacket);

    // Update TS counters
    if (!bPreparsing) {
      curDesc->IncreaseCounter();
      m_iTSCounter++;
    }

    // Update the flag to show we're in the middle of a PES packet
    m_bInMiddleOfPES = true;

    // if we're preparsing, we only ever deal with one packet at a time
    if (bPreparsing) break;

    // if the stream ended abruptly, abort the loop
    if (iRc == END_OF_STREAM) break;
  }

  // if we're NOT still in the middle of a PES, clear the flag
  if (!m_iDataLen) {
    m_bInMiddleOfPES = false;
  }

  // OK, return the status. This will be either NO_ERR or END_OF_STREAM
  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::SkipPES()
{
  int iRc;

  // read size of the PS packet
  if ((iRc = ReadDataLength(&m_iDataLen)) != 0) {
    return iRc;
  }

  // skip it
  return SkipData(m_iDataLen);
}


template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::ReadData(u8* buf, int length) 
{
  int iRc;

  // try to read the data
  iRc = m_pReader->Read(buf, length);
  if (iRc == FILE_EOF) return END_OF_STREAM;
  if (iRc < 0) return iRc;
  if (m_pReader->EndOfStream()) return END_OF_STREAM;

  // success
  return 0;
}

template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::SkipData(int length) 
{
  int iRc;
  byte buf[128];

  // try to skip
  while(length) {
    // calculate and update lengths
    int skipSize = sizeof(buf);
    if (skipSize > length) skipSize = length;
    length -= skipSize;

    // read a bit
    iRc = m_pReader->Read(buf, skipSize);
    if (iRc == FILE_EOF) return END_OF_STREAM;
    if (iRc < 0) return iRc;
    if (m_pReader->EndOfStream()) return END_OF_STREAM;
  }

  // success
  return 0;
}

template <class Reader, class TsProvider>
  int C_Ps2Ts<Reader, TsProvider>::ReadDataLength(u16* lengthDest)
{
  int iRc;
  u8 buf[2];

  // try to read it
  iRc = m_pReader->Read(buf, 2);
  if (iRc == FILE_EOF) return END_OF_STREAM;
  if (iRc < 0) return iRc;
  if (m_pReader->EndOfStream()) return END_OF_STREAM;
   
  // OK, extract it
  *lengthDest = U16_AT(buf);

  // success
  return 0;
}
